# Sc_perception_launch

## Usage

You can use the command as follows at shell script to launch `*.launch.xml` in `launch` directory.
Make sure that you have sourced the setup file for both streedrone dynamic workspace and this workspace and the Gazebo simulation environment is running.

```bash
ros2 run tf2_ros static_transform_publisher -6.3 -2.4 0 -0.09 0 0 map map_pcd
ros2 launch sc_perception_launch content_awareness.xml 
ros2 launch sc_perception_launch situation_awareness.xml vehicle_model:=sd_vehicle
ros2 launch sc_perception_launch self_awareness.xml vehicle_model:=sd_vehicle sensor_model:=sample_sensor_kit 
ros2 launch velodyne_driver velodyne_top_driver_node-VLP16-composed-launch.py 
```
