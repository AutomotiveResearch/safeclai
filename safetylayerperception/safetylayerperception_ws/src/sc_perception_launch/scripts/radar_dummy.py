#!/usr/bin/env python3


# Copyright (c) 2024 HAN university of applied science

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import numpy as np
import rclpy
from rclpy.node import Node
from tf2_ros.static_transform_broadcaster import StaticTransformBroadcaster
from geometry_msgs.msg import TransformStamped, Vector3, PoseStamped
from tf2_ros import TransformBroadcaster
import math
from sensor_msgs.msg import NavSatFix ,LaserScan,PointCloud2,Imu
from rclpy.qos import QoSProfile, QoSReliabilityPolicy, QoSHistoryPolicy, QoSDurabilityPolicy
from scipy import interpolate
from nav_msgs.msg import Odometry,Path
from geometry_msgs.msg import Twist
from OSMHandler import OSMHandler
from autoware_auto_perception_msgs.msg import PredictedObjects,PredictedObject,ObjectClassification,DetectedObject,DetectedObjects
from rclpy.parameter import Parameter
from geometry_msgs.msg import Twist, Point
from tf2_ros import TransformException
from tf2_ros.buffer import Buffer
from tf2_ros.transform_listener import TransformListener
from builtin_interfaces.msg import Time
from sc_interfaces.msg import ObservedObjects,Object
import tf2_geometry_msgs, tf2_msgs
from geometry_msgs.msg import TransformStamped, Vector3, PoseStamped,Point32,PolygonStamped, Polygon
import copy
FIXED_FRAME = "front_link_link"
MAX_NUM_OBJECTS = 50

class radar_object(Node):
    def __init__(self):
        super().__init__('radar_object')
        simtime =Parameter("use_sim_time", Parameter.Type.BOOL, True)
        self.set_parameters([simtime]) 
        self.pub_ObservedObjects= self.create_publisher(
            DetectedObjects,"/sensing/radar/detected_objects",10)    
   
        
        # self.create_subscription( 
        #     PredictedObjects,
        #     'perception_safety_layer/object_recognition/objects',
        #     self.object_callback,
        #     10)
        
        self.timer = self.create_timer(0.1, self.timer_callback)


        self.object_publisher = []
      
        self.sbr_ = StaticTransformBroadcaster(self)
        self.br_ = TransformBroadcaster(self)

        buffer_time = Time()
        buffer_time.sec = 0
        self.tf_buffer = Buffer(buffer_time)
        self.tf_listener = TransformListener(self.tf_buffer, self)
        self.objects_id = [] 
        self.len = 0

    def timer_callback(self):
        msg = DetectedObjects()
        msg.header.frame_id = "base_link"
        msg.header.stamp = self.get_clock().now().to_msg()
        msg.objects = []
        object_ = DetectedObject()
        oc_placeholder = ObjectClassification()
        oc_placeholder.probability = 1.0
        oc_placeholder.label = 0
        object_.classification.append(oc_placeholder)
        object_.existence_probability = 0.0
        object_.kinematics.pose_with_covariance.pose.position.x = 18.6
        object_.kinematics.pose_with_covariance.pose.position.y = -0.61
        object_.kinematics.pose_with_covariance.pose.position.z = 0.7

        object_.kinematics.pose_with_covariance.pose.orientation.x = 0.0
        object_.kinematics.pose_with_covariance.pose.orientation.y = 0.0
        object_.kinematics.pose_with_covariance.pose.orientation.z = 0.0
        object_.kinematics.pose_with_covariance.pose.orientation.w = 1.0

        object_.kinematics.twist_with_covariance.twist.linear.x = -0.0
        object_.kinematics.twist_with_covariance.twist.linear.y = -0.0

        object_.kinematics.has_position_covariance = False
        object_.kinematics.has_twist_covariance = False
        object_.kinematics.has_twist = True
        object_.kinematics.orientation_availability = 0

        object_.shape.dimensions.x = 0.5
        object_.shape.dimensions.y = 0.5
        object_.shape.dimensions.z = 0.5
        object_.shape.type = 0


        msg.objects.append(object_)
        self.pub_ObservedObjects.publish(msg)
                
  


        # self.br_.sendTransform(object_list)

def main(args=None):
    rclpy.init(args=args)
    radar_= radar_object()
    rclpy.spin(radar_)
    #optitrack_tf_state.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()