#!/usr/bin/env python3


# Copyright (c) 2024 HAN university of applied science

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import numpy as np
import rclpy
from rclpy.node import Node
from tf2_ros.static_transform_broadcaster import StaticTransformBroadcaster
from geometry_msgs.msg import TransformStamped, Vector3, PoseStamped,Point32,PolygonStamped, Polygon
from tf2_ros import TransformBroadcaster
import math
from sensor_msgs.msg import NavSatFix ,LaserScan,PointCloud2,Imu
from rclpy.qos import QoSProfile, QoSReliabilityPolicy, QoSHistoryPolicy, QoSDurabilityPolicy
from scipy import interpolate
from nav_msgs.msg import Odometry,Path
from geometry_msgs.msg import Twist
from OSMHandler import OSMHandler
import tf2_geometry_msgs, tf2_msgs
from tf2_ros import TransformException
from tf2_ros.buffer import Buffer
from tf2_ros.transform_listener import TransformListener
from builtin_interfaces.msg import Time
from rclpy.parameter import Parameter
from sc_interfaces.msg import PointArray
from launch_ros.substitutions import FindPackageShare
import os


FIXED_FRAME = "front_link_link"
SCENARIO = "han_campus"
# SCENARIO = "han_ca"
def calc_compass_bearing(pointA, pointB):

    lat1 = math.radians(pointA[0])
    lat2 = math.radians(pointB[0])

    diffLong = math.radians(pointB[1] - pointA[1])

    x = math.sin(diffLong) * math.cos(lat2)
    y = math.cos(lat1) * math.sin(lat2) - (math.sin(lat1)
                                           * math.cos(lat2) * math.cos(diffLong))

    initial_bearing = math.atan2(x, y)

    initial_bearing = math.degrees(initial_bearing)
    compass_bearing = (initial_bearing + 360) % 360

    return compass_bearing

    

def calc_dist(lat1, lon1, lat2, lon2):

    R = 6378.137; # Radius of earth in KM
    dLat = math.radians(lat2) - math.radians(lat1)
    dLon = math.radians(lon2) - math.radians(lon1)
    a = math.sin(dLat/2) * math.sin(dLat/2) + math.cos(math.radians(lat1)) * math.cos(math.radians(lat2)) * math.sin(dLon/2) * math.sin(dLon/2)
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
    d = R * c
    return d * 1000 # meters

def angle_sub(angle1, angle2 ):
    result = angle1 - angle2 

    if result < 0:
        result +=360
    if result >= 360:
        result -=360    

    return result

## transform from ned to enu
def compass_bearing_to_regular(compass_bearing):
    regular_bearing =0
    if compass_bearing >=0 and compass_bearing <= 180:
        regular_bearing = 90 - compass_bearing
    else :
        regular_bearing = 90 - compass_bearing
        if regular_bearing < 180:
            regular_bearing+=360  
        if regular_bearing > 180:
            regular_bearing-=360  
    # convert from 0 to -180 to 0 to 360 
    if regular_bearing < 0:
        regular_bearing +=360     
    return regular_bearing

def quaternion_to_euler(x, y, z, w):

    t0 = +2.0 * (w * x + y * z)
    t1 = +1.0 - 2.0 * (x * x + y * y)
    roll_x = math.atan2(t0, t1)
    t2 = +2.0 * (w * y - z * x)
    t2 = +1.0 if t2 > +1.0 else t2
    t2 = -1.0 if t2 < -1.0 else t2
    pitch_y = math.asin(t2)
    t3 = +2.0 * (w * z + x * y)
    t4 = +1.0 - 2.0 * (y * y + z * z)
    yaw_z = math.atan2(t3, t4)
    return roll_x, pitch_y, yaw_z # in radians
def read_osm_file(osm_file):
       

    handler = OSMHandler()
    handler.ways = {}  # Initialize a dictionary to store ways
    handler.data = {"nodes": {}}

    handler.apply_file(osm_file)

    return handler.relations

class safety_envelop_calculation(Node):
    def __init__(self):
        super().__init__('safety_envelop_calculation')

        # simtime =Parameter("use_sim_time", Parameter.Type.BOOL, True)
        # self.set_parameters([simtime])  
        self.declare_parameter('origin',[  51.98940382497835 , 5.949848804210092, 2.5])
        self.origin = self.get_parameter('origin').value  
        qos_profile = QoSProfile(
            reliability=QoSReliabilityPolicy.BEST_EFFORT,
            durability=QoSDurabilityPolicy.VOLATILE,
            history=QoSHistoryPolicy.KEEP_LAST,
            depth=1
        ) 
        self.pub_left_map_base_link = self.create_publisher(
            Path,"context_data_2",10)    
        self.pub_right_map_base_link = self.create_publisher(
            Path,"context_data_1",10)    

        self.poly_pub = self.create_publisher(
            PolygonStamped,"safty_envelope",10)
        self.poly_pub_margin = self.create_publisher(
            PolygonStamped,"safty_margin",10)
        
        self.create_subscription(
            PolygonStamped,
            'sc_topic_safety_margin',
            self.safety_margin_1_callback,
            10)
               
        self.create_subscription(
            Odometry,
            'odom',
            self.odom_callback,
            10)

        # self.points_pub_1 = self.create_publisher(
        #     PointArray,"sc_safety_envelope",10)
    
        ##origin as base_link


        my_dir = FindPackageShare(package='sc_perception_launch').find('sc_perception_launch') 
        map_full_path = os.path.join(my_dir, 'maps') + "/han_parking_deck_real_noa/lanelet2_map.osm"
        
        self.map_data = read_osm_file(map_full_path)
        self.transform = TransformStamped()
        self.main_deck_map =[self.map_data[7121][0],self.map_data[7121][1]]
        if SCENARIO == "han_campus":
            ##overwrite the map data
            self.main_deck_map =[[(22.9314, 1.8545), (31.7134, 6.1613), (77.5204, 7.8559)], [(23.133, -0.135), (31.0778, 1.4844), (77.2052, 3.3228)]]
            self.main_deck_map =[[(-8.449, 0.7824), (35.7049, 2.3072)], [(-8.3108, -3.2153), (35.8432, -1.6904)]]

        self.msg_safety_margin = PolygonStamped()
        self.msg_safety_margin.polygon.points = [Point32()]*30
        buffer_time = Time()
        buffer_time.sec = 0
        self.tf_buffer = Buffer(buffer_time)
        self.tf_listener = TransformListener(self.tf_buffer, self)
        self.timer = self.create_timer(1/10, self.on_frame_listener)
        print("map",self.main_deck_map)

    def safety_margin_1_callback(self,message:PolygonStamped):
        self.msg_safety_margin = message
        self.msg_safety_margin.header.frame_id = FIXED_FRAME
        self.msg_safety_margin.header.stamp = self.get_clock().now().to_msg()
 
        self.poly_pub_margin.publish(self.msg_safety_margin)

    # def safety_margin_2_callback(self,message:Polygon):
        
    #     for i in range(15):
    #         self.msg_safety_margin.polygon.points[i+15] = message.points[i]

    def on_frame_listener(self):
        
        try:
            polygon_msg = PolygonStamped()
            polygon_msg.header.frame_id = FIXED_FRAME
            polygon_msg.header.stamp = self.get_clock().now().to_msg()

            polygon_msg.polygon.points = []
            # pass for now
            path_msg_1 = Path()
            path_msg_2 = Path()

            path_msg_1.header.frame_id = FIXED_FRAME
            path_msg_2.header.frame_id = FIXED_FRAME

            path_msg_1.header.stamp = self.get_clock().now().to_msg()
            path_msg_2.header.stamp = self.get_clock().now().to_msg()

            path_msg_1.poses = []
            path_msg_2.poses = []
            
            for gps_point in self.main_deck_map[0]:
                pose = PoseStamped()
                pose.header.stamp = self.get_clock().now().to_msg()
                pose.header.frame_id = "map"
                x, y = self.calc_map_base_link(self.origin,[gps_point[0],gps_point[1],0.0])
                if SCENARIO == "han_campus":
                    ## over write the x and y
                    x, y = (gps_point[0],gps_point[1]) 
                pose.pose.position.x = x
                pose.pose.position.y = y
                
                pose.pose.position.z = 0.0
                transform = self.tf_buffer.lookup_transform(
                    FIXED_FRAME,
                    "map",
                    Time()
                )
                pose_transformed = tf2_geometry_msgs.do_transform_pose(pose.pose, transform)
                


                pose.pose = pose_transformed
                path_msg_1.poses.append(pose)



            path_msg_1 = self.interpolate_map(path_msg_1)
            


            for gps_point in self.main_deck_map[1]:
                pose = PoseStamped()
                pose.header.stamp = self.get_clock().now().to_msg()
                pose.header.frame_id = "map"
                x, y = self.calc_map_base_link(self.origin,[gps_point[0],gps_point[1],0.0])
                if SCENARIO == "han_campus":
                    # over write the x and y
                    x, y = (gps_point[0],gps_point[1])

                pose.pose.position.x = x
                pose.pose.position.y = y
                pose.pose.position.z = 0.0
                transform = self.tf_buffer.lookup_transform(
                    FIXED_FRAME,
                    "map",
                    Time()
                )
                pose_transformed = tf2_geometry_msgs.do_transform_pose(pose.pose, transform)
                
                pose.pose = pose_transformed
                path_msg_2.poses.append(pose)

          


            path_msg_2 = self.interpolate_map(path_msg_2)
            ##find the vertex of the polygon


            
            for pose in path_msg_2.poses:
                point = Point32()
                point.x = pose.pose.position.x
                point.y = pose.pose.position.y
                point.z = pose.pose.position.z
                polygon_msg.polygon.points.append(point)
            temp_path = path_msg_1.poses[::-1]
            for pose in temp_path:
                point = Point32()
                point.x = pose.pose.position.x
                point.y = pose.pose.position.y
                point.z = pose.pose.position.z
                polygon_msg.polygon.points.append(point)
            


            polygon_msg_ = self.interpolate_polygon(polygon_msg, n=50)
            print(len(polygon_msg_.polygon.points))
            self.poly_pub.publish(polygon_msg_)
            
            
        except BaseException as e:
           print(e)
    
    def odom_callback(self,message:Odometry):
        self.current_linear_velocity = math.sqrt(message.twist.twist.linear.x **2 + message.twist.twist.linear.y**2)
        r,p,yaw = quaternion_to_euler(message.pose.pose.orientation.x,message.pose.pose.orientation.y,message.pose.pose.orientation.z,message.pose.pose.orientation.w)
        
       

        #self.calc_map_base_link(self.gps_data,self.main_deck_map)        
    def interpolate_polygon(self,polygon:PolygonStamped, n=100):


        # Convert the points list into a numpy array
        points = []

        for point in polygon.polygon.points:
            points.append([point.x, point.y])
       
        points = np.array(points)

         # Calculate the total length of the polygon
        total_length = np.sum(np.linalg.norm(np.diff(points, axis=0), axis=1))
        
        # Calculate the number of points to generate
       
        points_per_unit_length = n / total_length
        
        # Calculate the length between each pair of points
        lengths = np.linalg.norm(np.diff(points, axis=0), axis=1)
        
        # Calculate the cumulative lengths
        cumulative_lengths = np.concatenate(([0], np.cumsum(lengths)))
        
        # Generate points along the polygon edges
        interpolated_points = []
        for i in range(len(points) - 1):
            segment_length = cumulative_lengths[i + 1] - cumulative_lengths[i]
            num_segment_points = int(np.round(segment_length * points_per_unit_length))
            
            if num_segment_points == 0:
                continue
            
            segment_points = np.linspace(0, 1, num_segment_points + 1)[:-1]
            
            for ratio in segment_points:
                interpolated_point = (1 - ratio) * points[i] + ratio * points[i + 1]
                interpolated_points.append(interpolated_point)
        
        # Ensure the final number of points is 100
        while len(interpolated_points) < n:
            last_point = points[-1]
            interpolated_points.append(last_point)
        
        # Trim extra points if generated more than 100
        interpolated_points = interpolated_points[:n]
        # Convert the interpolated points back to a Polygon message
        interpolated_polygon = PolygonStamped()
        interpolated_polygon.header = polygon.header
        interpolated_polygon.polygon.points = []
        for point in interpolated_points:
            interpolated_point = Point32()
            interpolated_point.x = point[0]
            interpolated_point.y = point[1]
            interpolated_point.z = 0.0
            interpolated_polygon.polygon.points.append(interpolated_point)
        return interpolated_polygon
    
    def interpolate_map(self,map_data:Path):
        points = []
        for point in map_data.poses:
            points.append([point.pose.position.x,point.pose.position.y])
        interpolated_points = []
        
        for i in range(len(points) - 1):
            x1, y1 = points[i]
            x2, y2 = points[i + 1]
           
            # Calculate the direction and length of the line segment
            dx = x2 - x1
            dy = y2 - y1
            length = np.sqrt(dx**2 + dy**2)
            
            # Calculate the number of half-unit intervals
            num_intervals = int(length/4)
            
            if num_intervals > 0:
                # Calculate the half-unit interval step
                step_x = dx / (2 * num_intervals)
                step_y = dy / (2 * num_intervals)
                
                # Add points at half-unit intervals along the line segment
                for j in range(2 * num_intervals):
                    interpolated_x = x1 + j * step_x
                    interpolated_y = y1 + j * step_y
                    interpolated_points.append([interpolated_x, interpolated_y])
        

        # Add the last point from the original list
        interpolated_points.append(points[-1])

        ## remove the point where the x > 10 and x < -10
        
        ##interpolated_points = [point for point in interpolated_points if point[0] < 20.0 and point[0] > -2.0]
        interpolated_points_filted = []
        ## remove the point where the x > 10 and x < -10
        # print('----------------===============---------------------')
        for point in interpolated_points:
            dis = math.sqrt(point[0]**2 + point[1]**2)
           
            if dis <= (45.0 ):
                #print(dis)
                interpolated_points_filted.append(point)
        #print('----------------===============---------------------')
        map_data.poses.clear()

        for point in interpolated_points_filted:
            pose = PoseStamped()
            pose.pose.position.x = point[0]
            pose.pose.position.y = point[1]
            pose.pose.position.z = 0.0
            map_data.poses.append(pose)

        return map_data
    
    def calc_map_base_link(self,origin, map_data_point):
        bearing = calc_compass_bearing(origin , map_data_point)
        dist = calc_dist(
                origin [0], origin [1], map_data_point[0], map_data_point[1])
        bearing_rad = math.radians(bearing)
         ## transform gps_link to base_link
        x = dist * math.sin(bearing_rad) 
        y = dist * math.cos(bearing_rad) 

        # Define the rotation angle in radians
        theta = np.radians(origin[2])  # Rotate by 45 degrees (you can change this angle)

        # Define the rotation matrix
        r_m = np.array([[np.cos(theta), -np.sin(theta)],
                    [np.sin(theta), np.cos(theta)]])
        rotated_point = np.dot(r_m,[x,y])
        
        return rotated_point[0],rotated_point[1]
    




     



def main(args=None):
    rclpy.init(args=args)
    optitrack_tf_state= safety_envelop_calculation()
    rclpy.spin(optitrack_tf_state)
    #optitrack_tf_state.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()