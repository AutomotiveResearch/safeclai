#!/usr/bin/env python3

# Copyright (c) 2024 HAN university of applied science

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import numpy as np
import rclpy
from rclpy.node import Node
from tf2_ros.static_transform_broadcaster import StaticTransformBroadcaster
from geometry_msgs.msg import TransformStamped, Vector3, PoseStamped
from tf2_ros import TransformBroadcaster
import math
from sensor_msgs.msg import NavSatFix ,LaserScan,PointCloud2,Imu
from rclpy.qos import QoSProfile, QoSReliabilityPolicy, QoSHistoryPolicy, QoSDurabilityPolicy
from scipy import interpolate
from nav_msgs.msg import Odometry,Path
from geometry_msgs.msg import Twist
from OSMHandler import OSMHandler

def calc_compass_bearing(pointA, pointB):

    lat1 = math.radians(pointA[0])
    lat2 = math.radians(pointB[0])

    diffLong = math.radians(pointB[1] - pointA[1])

    x = math.sin(diffLong) * math.cos(lat2)
    y = math.cos(lat1) * math.sin(lat2) - (math.sin(lat1)
                                           * math.cos(lat2) * math.cos(diffLong))

    initial_bearing = math.atan2(x, y)

    initial_bearing = math.degrees(initial_bearing)
    compass_bearing = (initial_bearing + 360) % 360

    return compass_bearing

    

def calc_dist(lat1, lon1, lat2, lon2):

    R = 6378.137; # Radius of earth in KM
    dLat = math.radians(lat2) - math.radians(lat1)
    dLon = math.radians(lon2) - math.radians(lon1)
    a = math.sin(dLat/2) * math.sin(dLat/2) + math.cos(math.radians(lat1)) * math.cos(math.radians(lat2)) * math.sin(dLon/2) * math.sin(dLon/2)
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
    d = R * c
    return d * 1000 # meters

def angle_sub(angle1, angle2 ):
    result = angle1 - angle2 

    if result < 0:
        result +=360
    if result >= 360:
        result -=360    

    return result

## transform from ned to enu
def compass_bearing_to_regular(compass_bearing):
    regular_bearing =0
    if compass_bearing >=0 and compass_bearing <= 180:
        regular_bearing = 90 - compass_bearing
    else :
        regular_bearing = 90 - compass_bearing
        if regular_bearing < 180:
            regular_bearing+=360  
        if regular_bearing > 180:
            regular_bearing-=360  
    # convert from 0 to -180 to 0 to 360 
    if regular_bearing < 0:
        regular_bearing +=360     
    return regular_bearing

def euler_to_quaternion(roll, pitch, yaw):
    
        qx = math.sin(roll/2) * math.cos(pitch/2) * math.cos(yaw/2) - math.cos(roll/2) * math.sin(pitch/2) * math.sin(yaw/2)
        qy = math.cos(roll/2) * math.sin(pitch/2) * math.cos(yaw/2) + math.sin(roll/2) * math.cos(pitch/2) * math.sin(yaw/2)
        qz = math.cos(roll/2) * math.cos(pitch/2) * math.sin(yaw/2) - math.sin(roll/2) * math.sin(pitch/2) * math.cos(yaw/2)
        qw = math.cos(roll/2) * math.cos(pitch/2) * math.cos(yaw/2) + math.sin(roll/2) * math.sin(pitch/2) * math.sin(yaw/2)
    
        return qx, qy, qz, qw

def quaternion_to_euler(x, y, z, w):

    t0 = +2.0 * (w * x + y * z)
    t1 = +1.0 - 2.0 * (x * x + y * y)
    roll_x = math.atan2(t0, t1)
    t2 = +2.0 * (w * y - z * x)
    t2 = +1.0 if t2 > +1.0 else t2
    t2 = -1.0 if t2 < -1.0 else t2
    pitch_y = math.asin(t2)
    t3 = +2.0 * (w * z + x * y)
    t4 = +1.0 - 2.0 * (y * y + z * z)
    yaw_z = math.atan2(t3, t4)
    return roll_x, pitch_y, yaw_z # in radians

def read_osm_file(osm_file):
       

    handler = OSMHandler()
    handler.ways = {}  # Initialize a dictionary to store ways
    handler.data = {"nodes": {}}

    handler.apply_file(osm_file)

    return handler.relations

class lannet2_map_tf(Node):
    def __init__(self):
        super().__init__('lannet2_map_tf')
        self.declare_parameter('origin',[51.990226, 5.948833, 2.5])
        self.origin = self.get_parameter('origin').value  
        qos_profile = QoSProfile(
            reliability=QoSReliabilityPolicy.BEST_EFFORT,
            durability=QoSDurabilityPolicy.VOLATILE,
            history=QoSHistoryPolicy.KEEP_LAST,
            depth=1
        ) 
        self.pub_left_map_base_link = self.create_publisher(
            Path,"context_data_2",10)    
        self.pub_right_map_base_link = self.create_publisher(
            Path,"context_data_1",10)    
        
        self.odom_vel = self.create_subscription( 
            Odometry,
            'odom',
            self.set_odom,
            10)

        self.map_data = read_osm_file("/home/yxwork/SafeCLAI/safetylayerperception/safetylayerperception_ws/src/sc_perception_launch/scripts/lanelet2_map.osm")
        self.odom  = Odometry()
        ## TODO: add search lanelet based on GPS
        self.main_deck_map =[self.map_data[7121][0],self.map_data[7121][1]]
        print("map",self.main_deck_map)
             
       
        
        self.path_msg_1 = Path()
        self.path_msg_2 = Path()


    def calc_map_bond(self):
        self.path_msg_1.header.frame_id = "base_link"
        self.path_msg_2.header.frame_id = "base_link"

        # self.path_msg_1.header.stamp = message.header.stamp
        # self.path_msg_2.header.stamp = message.header.stamp

        self.path_msg_1.poses = []
        self.path_msg_2.poses = []
        r,p,yaw_odom = quaternion_to_euler(self.odom.pose.pose.orientation.x,self.odom.pose.pose.orientation.y,self.odom.pose.pose.orientation.z,self.odom.pose.pose.orientation.w)
        yaw_odom = -yaw_odom
        from_frame_rel = [self.odom.pose.pose.position.x, self.odom.pose.pose.position.y, self.odom.pose.pose.position.z,r,p,yaw_odom ]
        
        for gps_point in self.main_deck_map[0]:
            pose = PoseStamped()
            x, y = self.calc_map_base_link(self.origin,[gps_point[0],gps_point[1],0.0])


            to_frame_rel = [x, y, 0.0, 0.0, 0.0, 0.0]


            x, y, z =  to_frame_rel[0] - from_frame_rel[0], to_frame_rel[1] - from_frame_rel[1], to_frame_rel[2] - from_frame_rel[2]

            
            # Define the rotation matrix
            r_m = np.array([[np.cos(yaw_odom), -np.sin(yaw_odom)],
                    [np.sin(yaw_odom), np.cos(yaw_odom)]])
            rotated_point = np.dot(r_m,[x,y])
            

            pose.pose.position.x = rotated_point[0]
            pose.pose.position.y = rotated_point[1]
            
            pose.pose.position.z = 0.0
            self.path_msg_1.poses.append(pose)

        
        for gps_point in self.main_deck_map[1]:
            pose = PoseStamped()
            x, y = self.calc_map_base_link(self.origin,[gps_point[0],gps_point[1],0.0])

            to_frame_rel = [x, y, 0.0, 0.0, 0.0, 0.0]

            x, y, z =  to_frame_rel[0] - from_frame_rel[0], to_frame_rel[1] - from_frame_rel[1], to_frame_rel[2] - from_frame_rel[2]
            

            # Define the rotation matrix
            r_m = np.array([[np.cos(yaw_odom), -np.sin(yaw_odom)],
                    [np.sin(yaw_odom), np.cos(yaw_odom)]])
            rotated_point = np.dot(r_m,[x,y])

            pose.pose.position.x = rotated_point[0]
            pose.pose.position.y = rotated_point[1]
            pose.pose.position.z = 0.0
            self.path_msg_2.poses.append(pose)

        

    def interpolate_map(self,map_data:Path):
        points = []
        for point in map_data.poses:
            points.append([point.pose.position.x,point.pose.position.y])
        interpolated_points = []
        
        for i in range(len(points) - 1):
            x1, y1 = points[i]
            x2, y2 = points[i + 1]
           
            # Calculate the direction and length of the line segment
            dx = x2 - x1
            dy = y2 - y1
            length = np.sqrt(dx**2 + dy**2)
            
            # Calculate the number of half-unit intervals
            num_intervals = int(length * 2)
            
            if num_intervals > 0:
                # Calculate the half-unit interval step
                step_x = dx / (2 * num_intervals)
                step_y = dy / (2 * num_intervals)
                
                # Add points at half-unit intervals along the line segment
                for j in range(2 * num_intervals):
                    interpolated_x = x1 + j * step_x
                    interpolated_y = y1 + j * step_y
                    interpolated_points.append([interpolated_x, interpolated_y])
        

        # Add the last point from the original list
        interpolated_points.append(points[-1])
        interpolated_points_filted = []
        ## remove the point where the x > 10 and x < -10
        # print('----------------===============---------------------')
        for point in interpolated_points:
            dis = math.sqrt(point[0]**2 + point[1]**2)
           
            if dis <= 20.0:
                #print(dis)
                interpolated_points_filted.append(point)
        #print('----------------===============---------------------')

        map_data.poses.clear()

        for point in interpolated_points_filted:
            pose = PoseStamped()
            pose.pose.position.x = point[0]
            pose.pose.position.y = point[1]
            pose.pose.position.z = 0.0
            map_data.poses.append(pose)

        return map_data
    
    def calc_map_base_link(self,origin, map_data_point):
        bearing = calc_compass_bearing(origin , map_data_point)
        dist = calc_dist(
                origin [0], origin [1], map_data_point[0], map_data_point[1])
        bearing_rad = math.radians(bearing)
         ## transform gps_link to base_link
        x = dist * math.sin(bearing_rad) 
        y = dist * math.cos(bearing_rad) 

        # Define the rotation angle in radians
        theta = np.radians(origin[2])  # Rotate by 45 degrees (you can change this angle)

        # Define the rotation matrix
        r_m = np.array([[np.cos(theta), -np.sin(theta)],
                    [np.sin(theta), np.cos(theta)]])
        rotated_point = np.dot(r_m,[x,y])
        
        return rotated_point[0],rotated_point[1]
    

    def set_odom(self,message:Odometry):
        self.odom  = message
        self.calc_map_bond()
        self.path_msg_1 = self.interpolate_map(self.path_msg_1)
        self.path_msg_2 = self.interpolate_map(self.path_msg_2)
        self.pub_left_map_base_link.publish(self.path_msg_1)
        self.pub_right_map_base_link.publish(self.path_msg_2)




def main(args=None):
    rclpy.init(args=args)
    optitrack_tf_state= lannet2_map_tf()
    rclpy.spin(optitrack_tf_state)
    #optitrack_tf_state.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()