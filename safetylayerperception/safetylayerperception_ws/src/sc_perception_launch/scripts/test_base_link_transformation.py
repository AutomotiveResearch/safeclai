#!/usr/bin/env python3

# Copyright (c) 2024 HAN university of applied science

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import numpy as np
import rclpy
from rclpy.node import Node
from tf2_ros.static_transform_broadcaster import StaticTransformBroadcaster
from geometry_msgs.msg import TransformStamped, Vector3, PoseStamped
from tf2_ros import TransformBroadcaster
import math
from sensor_msgs.msg import NavSatFix ,LaserScan,PointCloud2,Imu
from rclpy.qos import QoSProfile, QoSReliabilityPolicy, QoSHistoryPolicy, QoSDurabilityPolicy
from rclpy.parameter import Parameter
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Twist
from tf2_ros import TransformException
from tf2_ros.buffer import Buffer
from tf2_ros.transform_listener import TransformListener
from builtin_interfaces.msg import Time
class test_base_link_transformation(Node):
    def __init__(self):
        super().__init__('test_base_link_transformation_odom')
        # simtime =Parameter("use_sim_time", Parameter.Type.BOOL, False)
        # self.set_parameters([simtime])  
        # self.declare_parameter('odom_pos',[0,0,0,0,0,0])
        # self.declare_parameter('reset_odom',True)
        # self.reset_odom = self.get_parameter('reset_odom').value
        # self.odom_pos = self.get_parameter('odom_pos').value
        self.br = TransformBroadcaster(self)
        self.pose = Odometry()

        self.create_subscription(Odometry,'/odometry/global',self.pose_callback,10)
   

   
    
        
        
    def pose_callback(self, message:Odometry):
        self.pose = message
        Transform_msg = TransformStamped()
        Transform_msg.header.stamp= message.header.stamp
        Transform_msg.header.frame_id = "map"
        Transform_msg.child_frame_id = "base_link"
        Transform_msg.transform.translation.x = self.pose.pose.pose.position.x
        Transform_msg.transform.translation.y = self.pose.pose.pose.position.y
        Transform_msg.transform.translation.z = self.pose.pose.pose.position.z
        Transform_msg.transform.rotation.x = self.pose.pose.pose.orientation.x
        Transform_msg.transform.rotation.y = self.pose.pose.pose.orientation.y
        Transform_msg.transform.rotation.z = self.pose.pose.pose.orientation.z
        Transform_msg.transform.rotation.w = self.pose.pose.pose.orientation.w
        self.br.sendTransform(Transform_msg)
        
        

  

def main(args=None):
    rclpy.init(args=args)
    optitrack_tf_state= test_base_link_transformation()
    rclpy.spin(optitrack_tf_state)
    #optitrack_tf_state.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()