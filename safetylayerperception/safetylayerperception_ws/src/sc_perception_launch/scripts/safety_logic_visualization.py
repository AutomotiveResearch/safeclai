#!/usr/bin/env python3


# Copyright (c) 2024 HAN university of applied science

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import numpy as np
import rclpy
from rclpy.node import Node
from tf2_ros.static_transform_broadcaster import StaticTransformBroadcaster
from geometry_msgs.msg import TransformStamped, Vector3, PoseStamped
from tf2_ros import TransformBroadcaster
import math
from sensor_msgs.msg import NavSatFix ,LaserScan,PointCloud2,Imu
from rclpy.qos import QoSProfile, QoSReliabilityPolicy, QoSHistoryPolicy, QoSDurabilityPolicy
from sc_interfaces.msg import SafetyLogicState,SafetyEnvelopeTrigger
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Twist
from visualization_msgs.msg import MarkerArray,Marker
from rclpy.parameter import Parameter
from sc_interfaces.msg import ObservedObjects,Object


def quaternion_to_euler(x, y, z, w):
    import math
    t0 = +2.0 * (w * x + y * z)
    t1 = +1.0 - 2.0 * (x * x + y * y)
    roll_x = math.atan2(t0, t1)
    t2 = +2.0 * (w * y - z * x)
    t2 = +1.0 if t2 > +1.0 else t2
    t2 = -1.0 if t2 < -1.0 else t2
    pitch_y = math.asin(t2)
    t3 = +2.0 * (w * z + x * y)
    t4 = +1.0 - 2.0 * (y * y + z * z)
    yaw_z = math.atan2(t3, t4)
    return roll_x, pitch_y, yaw_z # in radians

class safety_logic_visual(Node):
    def __init__(self):
        super().__init__('safety_logic_visual')
        
        simtime =Parameter("use_sim_time", Parameter.Type.BOOL, True)
        self.set_parameters([simtime])  
        self.create_subscription(
            SafetyLogicState,"/sc_topic_safety_logic_state",self.sc_logic_callback,10)

        
        self.publisher_virual_wall = self.create_publisher(
            MarkerArray,"/sc_topic_virtual_wall",10)
        
        self.Markers = MarkerArray()
        self.marker_wall = Marker()
        self.marker_text = Marker()

    def sc_logic_callback(self,message:SafetyLogicState):
        self.Markers.markers.clear()
        self.marker_wall.header.stamp = self.get_clock().now().to_msg()
        self.marker_wall.header.frame_id = "front_link_link"
        self.marker_wall.ns = "stop_virtual_wall_sc"
        self.marker_wall.id = 0
        self.marker_wall.type = 1
        self.marker_wall.action = 0
        self.marker_wall.pose.position.x = 1.0
        self.marker_wall.pose.position.y = 0.0
        self.marker_wall.pose.position.z = 1.0
        self.marker_wall.pose.orientation.x = 0.0
        self.marker_wall.pose.orientation.y = 0.0
        self.marker_wall.pose.orientation.z = 0.0
        self.marker_wall.pose.orientation.w = 1.0
        self.marker_wall.scale.x = 0.1
        self.marker_wall.scale.y = 5.0
        self.marker_wall.scale.z = 2.0
        self.marker_wall.color.a = 0.5
        self.marker_wall.color.r = 0.0
        self.marker_wall.color.g = 1.0
        self.marker_wall.color.b = 0.0
        self.marker_wall.lifetime.sec = 0
        self.marker_wall.lifetime.nanosec = 500000000
        self.marker_wall.frame_locked = True

        self.marker_text.header.stamp = self.get_clock().now().to_msg()
        self.marker_text.header.frame_id = "front_link_link"
        self.marker_text.ns = "stop_factor_text_sc"
        self.marker_text.id = 0
        self.marker_text.type = 9
        self.marker_text.action = 0
        self.marker_text.text = "Safety State " + str(message.state_major) + "." + str(message.state_minor)
        self.marker_text.pose.position.x = 1.0
        self.marker_text.pose.position.y = 0.0
        self.marker_text.pose.position.z = 1.0
        self.marker_text.pose.orientation.x = 0.0
        self.marker_text.pose.orientation.y = 0.0
        self.marker_text.pose.orientation.z = 0.0
        self.marker_text.pose.orientation.w = 1.0
        self.marker_text.scale.x = 0.0
        self.marker_text.scale.y = 0.0
        self.marker_text.scale.z = 1.0
        self.marker_text.color.a = 1.0
        self.marker_text.color.r = 1.0
        self.marker_text.color.g = 1.0
        self.marker_text.color.b = 1.0
        self.marker_text.lifetime.sec = 0
        self.marker_text.lifetime.nanosec = 500000000
        self.marker_text.frame_locked = True

        


        if message.state_major == 2:
            self.marker_wall.color.r = 1.0
            self.marker_wall.color.g = 0.0
            self.marker_wall.color.b = 0.0
            self.marker_text.lifetime.nanosec = 500000000
            self.marker_text.frame_locked = True
            self.marker_wall.lifetime.nanosec = 500000000
            self.marker_wall.frame_locked = True

            self.Markers.markers.append(self.marker_wall)
        self.Markers.markers.append(self.marker_text)
        self.publisher_virual_wall.publish(self.Markers)


def main(args=None):
    rclpy.init(args=args)
    optitrack_tf_state= safety_logic_visual()
    rclpy.spin(optitrack_tf_state)
    #optitrack_tf_state.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()