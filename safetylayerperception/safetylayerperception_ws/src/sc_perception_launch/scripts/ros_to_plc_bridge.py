#!/usr/bin/env python3


# Copyright (c) 2024 HAN university of applied science

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import numpy as np
import socket
import struct
import time
# from sc_interfaces.msg import ObservedObjects,Object,SystemState, IsVechicleMoving ,SystemQuality, IgnitionSignal
# from nav_msgs.msg import Odometry
class point2d:
    x = 0.0
    y = 0.0
    bytes_message = None

    ## message encoder function
    def encode_message(self):
        #convert every variable to bytes
        x_bytes = bytearray(struct.pack("f",self.x))
        y_bytes = bytearray(struct.pack("f",self.y))
        ## concatenate all the bytes
        self.bytes_message = x_bytes + y_bytes

class object_information:
    ## position and orientation of the object in the vehicle frame
    pos_x = 0.0 
    pos_y = 0.0
    heading_rad = 0.0
    ## object velocity 
    velocity_x = 0.0
    velocity_y = 0.0
    ## object size info
    object_size_x = 0.0
    object_size_y = 0.0
    bytes_message = None

    ## message encoder function
    def encode_message(self):
        #convert every variable to bytes
        pos_x_bytes = bytearray(struct.pack("f",self.pos_x))
        pos_y_bytes = bytearray(struct.pack("f",self.pos_y))
        heading_rad_bytes = bytearray(struct.pack("f",self.heading_rad))
        velocity_x_bytes = bytearray(struct.pack("f",self.velocity_x))
        velocity_y_bytes = bytearray(struct.pack("f",self.velocity_y))
        object_size_x_bytes = bytearray(struct.pack("f",self.object_size_x))
        object_size_y_bytes = bytearray(struct.pack("f",self.object_size_y))
        ## concatenate all the bytes
        self.bytes_message = pos_x_bytes + pos_y_bytes + heading_rad_bytes + velocity_x_bytes + velocity_y_bytes + object_size_x_bytes + object_size_y_bytes

## ------------------------message class from ROS2 to PLC------------------------ ## 
class vehicle_information:
    ## position and orientation of the vehicle in the map frame
    map_pos_x = 0.0 
    map_pos_y = 0.0
    map_heading_rad = 0.0
    ## ego velocity 
    ego_velocity_x = 0.0
    ego_velocity_y = 0.0
    ## ego steering angle in radians (ground wheel angle)
    ego_steering_angle_rad = 0.0
    message_counter = 0
    ## ego flags
    ego_ignition = False
    ego_system_quality = False

    ## message to be sent to the PLC
    bytes_message = None

    ## message encoder function
    def encode_message(self,type:int):
        if self.message_counter > 255:
            self.message_counter = 0
        #convert every variable to bytes
        type_bytes = type.to_bytes(1,byteorder='big')
        message_counter_bytes = self.message_counter.to_bytes(1,byteorder='big')
        #pack float info into 4 bytes
        map_pos_x_bytes = bytearray(struct.pack("f",self.map_pos_x))
        map_pos_y_bytes = bytearray(struct.pack("f",self.map_pos_y))
        map_heading_rad_bytes = bytearray(struct.pack("f",self.map_heading_rad))
        ego_velocity_x_bytes = bytearray(struct.pack("f",self.ego_velocity_x))
        ego_velocity_y_bytes = bytearray(struct.pack("f",self.ego_velocity_y))
        ego_steering_angle_rad_bytes = bytearray(struct.pack("f",self.ego_steering_angle_rad))
        ## convert bool to bytes
        ego_ignition_bytes = self.ego_ignition.to_bytes(1,byteorder='big')
        ego_system_quality_bytes = self.ego_system_quality.to_bytes(1,byteorder='big')
        ## concatenate all the bytes
        self.bytes_message = type_bytes + message_counter_bytes + map_pos_x_bytes + map_pos_y_bytes + map_heading_rad_bytes + ego_velocity_x_bytes + ego_velocity_y_bytes + ego_steering_angle_rad_bytes + ego_ignition_bytes + ego_system_quality_bytes
        
        
        
    # def load_from_ros2(self,ros2_vehicle_information:Odometry,ignition_signal:IgnitionSignal,system_quality:SystemQuality,system_state:SystemState,is_vehicle_moving:IsVechicleMoving):
    #     self.map_pos_x = ros2_vehicle_information.pose.pose.position.x
    #     self.map_pos_y = ros2_vehicle_information.pose.pose.position.y
    #     self.map_pos_z = ros2_vehicle_information.pose.pose.position.z
    #     self.map_orientation_x = ros2_vehicle_information.pose.pose.orientation.x
    #     self.map_orientation_y = ros2_vehicle_information.pose.pose.orientation.y
    #     self.map_orientation_z = ros2_vehicle_information.pose.pose.orientation.z
    #     self.map_orientation_w = ros2_vehicle_information.pose.pose.orientation.w
    #     self.ego_velocity_x = ros2_vehicle_information.twist.twist.linear.x
    #     self.ego_velocity_y = ros2_vehicle_information.twist.twist.linear.y
    #     self.ego_velocity_z = ros2_vehicle_information.twist.twist.linear.z
    #     self.ego_steering_angle_rad = system_state.steering_angle
    #     self.ego_ignition = ignition_signal.data
    #     self.ego_system_quality = system_quality.quality
    #     self.message_counter = self.message_counter + 1

    
class object_information_list:

    ## message to be sent to the PLC
    bytes_message = None
    message_counter = 0
    def __init__(self,length:int):
        ## fixed length of the list predefined in the PLC
        self.length = length
        ## list of objects
        self.object_list =  [object_information() for _ in range(length)]

    ## message encoder function
    def encode_message(self,type:int,size:int):
        if self.message_counter > 255:
            self.message_counter = 0        
        # convert every variable to bytes
        type_bytes = type.to_bytes(1,byteorder='big')
        message_counter_bytes = self.message_counter.to_bytes(1,byteorder='big')
        size_bytes = size.to_bytes(1,byteorder='big')
        ## concatenate all the bytes
        self.bytes_message = type_bytes + message_counter_bytes + size_bytes
        for i in range(self.length):
            self.object_list[i].encode_message()
            self.bytes_message += self.object_list[i].bytes_message

    
    # def load_from_ros2(self,ros2_object_list:ObservedObjects):
    #     pass

class map_information:
    bytes_message = None
    message_counter = 0
    def __init__(self,length:int):
        ## fixed length of the list predefined in the PLC
        self.length = length
        ## list of objects
        self.map =  [point2d() for _ in range(length)]

    ## message encoder function
    def encode_message(self,type:int,size:int):
        if self.message_counter > 255:
            self.message_counter = 0
        # convert every variable to bytes
        type_bytes = type.to_bytes(1,byteorder='big')
        message_counter_bytes = self.message_counter.to_bytes(1,byteorder='big')
        size_bytes = size.to_bytes(1,byteorder='big')
        ## concatenate all the bytes
        self.bytes_message = type_bytes + message_counter_bytes + size_bytes
        for i in range(self.length):
            self.map[i].encode_message()
            self.bytes_message += self.map[i].bytes_message



class nominal_input:
    bytes_message = None
    steering_angle = 0.0
    acceleration = 0.0
    message_counter = 0

    ## message encoder function
    def encode_message(self,type:int):
        if self.message_counter > 255:
            self.message_counter = 0
        #convert every variable to bytes
        type_bytes = type.to_bytes(1,byteorder='big')
        message_counter_bytes = self.message_counter.to_bytes(1,byteorder='big')
        #pack float info into 4 bytes
        steering_angle_bytes = bytearray(struct.pack("f",self.steering_angle))
        acceleration_bytes = bytearray(struct.pack("f",self.acceleration))
        ## concatenate all the bytes
        self.bytes_message = type_bytes + message_counter_bytes + acceleration_bytes + steering_angle_bytes


## ------------------------message class from PLC to ROS2------------------------ ## 
class control_signal:
    bytes_message = None
    steering_angle = 0.0
    acceleration = 0.0

    ## message decoder function
    def decode_message(self):
        if self.bytes_message is not None:
            pass

##------plc interface class------##
class plc_interface_client:
    ## PLC IP address
    PLC_IP = ""
    ## PLC port
    PLC_PORT = 5000
    ## PLC socket
    PLC_SOCKET = None
    
    protocol = "TCP"

    def __init__(self,ip:str,port:int,protocol:str):
        self.PLC_IP = ip
        self.PLC_PORT = port
        self.protocol = protocol
        if protocol == "TCP":
            self.PLC_SOCKET = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.PLC_SOCKET.connect((self.PLC_IP, self.PLC_PORT))
            print("connected to the PLC")
        elif protocol == "UDP":
            self.PLC_SOCKET = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            print("connected to the PLC")

    def send_message(self,message:bytes):
         if self.PLC_SOCKET is not None:
            if self.protocol == "TCP":
                    self.PLC_SOCKET.sendall(message)
                    print("message sent to the PLC")
            elif self.protocol == "UDP":
                    self.PLC_SOCKET.sendto(message,(self.PLC_IP, self.PLC_PORT))
                    print("message sent to the PLC")

    def receive_message(self,buffer):
            
         if self.PLC_SOCKET is not None:
            if self.protocol == "TCP":
                    data = self.PLC_SOCKET.recv(buffer)
                    print("message received from the PLC")
                    return data
            elif self.protocol == "UDP":
                    data, address = self.PLC_SOCKET.recvfrom(buffer)
                    print("message received from the PLC")
                    return data
            

if __name__ == "__main__":
    ## test the message encoder
    ego_info = vehicle_information()
    ego_info.map_pos_x = 1.0
    ego_info.map_pos_y = 2.0
    ego_info.map_heading_rad = 3.0
    ego_info.ego_velocity_x = 8.0
    ego_info.ego_velocity_y = 9.0
    ego_info.ego_steering_angle_rad = 11.0
    ego_info.ego_ignition = True
    ego_info.ego_system_quality = True
    ego_info.message_counter = 12

    ego_info.encode_message(3)

    print("The ego_message_info:")
    ##print all the bytes in hex format without next line
    print(','.join(format(x, '02x') for x in ego_info.bytes_message))
    print(len(ego_info.bytes_message))

    ## test the message encoder
    nominal_input_info = nominal_input()
    nominal_input_info.steering_angle = 1.0
    nominal_input_info.acceleration = 2.0
    nominal_input_info.message_counter = 3
    nominal_input_info.encode_message(4)

    print("The nominal_input:")
    ##print all the bytes in hex format without next line
    print(','.join(format(x, '02x') for x in nominal_input_info.bytes_message))
    print(len(nominal_input_info.bytes_message))


    ## test the message encoder
    object_info = object_information()
    object_info.pos_x = 1.0
    object_info.pos_y = 2.0
    object_info.heading_rad = 3.0
    object_info.velocity_x = 3.0
    object_info.velocity_y = 4.0
    object_info.object_size_x = 5.0
    object_info.object_size_y = 6.0

    object_list_ = object_information_list(100)
    object_list_.object_list[0] = object_info
    object_list_.object_list[1] = object_info
    object_list_.object_list[2] = object_info
    object_list_.object_list[3] = object_info
    object_list_.message_counter = 1
    size_of_the_objects = 4
    object_list_.encode_message(2,size_of_the_objects)

    print("The object_list:")
    ##print all the bytes in hex format without next line
    print(','.join(format(x, '02x') for x in object_list_.bytes_message))
    print(len(object_list_.bytes_message))

    ## test the message encoder
    point2d_info = point2d()
    point2d_info.x = 1.0
    point2d_info.y = 2.0

    map_info = map_information(50)
    map_info.map[0] = point2d_info
    map_info.map[1] = point2d_info
    map_info.map[2] = point2d_info
    map_info.map[3] = point2d_info
    map_info.map[4].x = 3.0
    map_info.message_counter = 1
    size_of_the_map = 4
    map_info.encode_message(1,size_of_the_map)

    print("The map_info:")

    ##print all the bytes in hex format without next line
    print(','.join(format(x, '02x') for x in map_info.bytes_message))
    print(len(map_info.bytes_message))


    #send the message to the PLC
    ## Decision:
    ## for each port there is a different msg? 
    ## otherwise might be wise to add the first 2 bytes as the id of the message
    ## such that from the reverse side we can read the message length and then read the message using the length

    port_ego_info = plc_interface_client("127.0.0.1",5000,"UDP")
    port_nominal_input_info = plc_interface_client("127.0.0.1",5001,"UDP")
    port_object_list_ = plc_interface_client("127.0.0.1",5002,"UDP")
    port_map_info = plc_interface_client("127.0.0.1",5003,"UDP")
    
    while True:
        ego_info.encode_message(3)
        object_list_.encode_message(2,size_of_the_objects)
        map_info.encode_message(1,size_of_the_map)
        nominal_input_info.encode_message(4)

        port_ego_info.send_message(ego_info.bytes_message)
        ego_info.message_counter += 1
        time.sleep(0.1)

        port_nominal_input_info.send_message(nominal_input_info.bytes_message)
        nominal_input_info.message_counter += 1
        time.sleep(0.1)
        
        port_object_list_.send_message(object_list_.bytes_message)
        object_list_.message_counter += 1
        time.sleep(0.1)

        port_map_info.send_message(map_info.bytes_message)
        map_info.message_counter += 1
        time.sleep(0.1)