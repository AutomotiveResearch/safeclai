#!/usr/bin/env python3

# Copyright (c) 2024 HAN university of applied science

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import numpy as np
import rclpy
from rclpy.node import Node
from sensor_msgs.msg import Joy
from geometry_msgs.msg import Twist, TransformStamped
from sc_interfaces.msg import NominalPowerSupplyStatus, NominalAccelerationRequest, NominalSteeringRequest
class twist_to_nominal(Node):
    def __init__(self):
        super().__init__('twist_to_nominal')
        self.declare_parameter('machine_id', "base_link")
        self.machine_id = self.get_parameter('machine_id').value
        self.declare_parameter('initial_pose', [0, 0, 0, 0])
        self.initial_pose = self.get_parameter('initial_pose').value

        self.power_pub = self.create_publisher(NominalPowerSupplyStatus, 'sc_topic_nominal_power_supply_status', 10)
        self.accel_pub = self.create_publisher(NominalAccelerationRequest, 'sc_topic_nominal_acceleration_request', 10)
        self.steer_pub = self.create_publisher(NominalSteeringRequest, 'sc_topic_nominal_steering_request', 10)

        self.create_subscription(
            Twist,
            '/cmd_vel_autoware',
            self.pub_cmd,
            10)
     
        self.cmd_vel_msg = Twist()


    def pub_cmd(self, message: Twist):
        
        ## the linear x will be the sum of the l2 and R2 remapped to -10 to 10

        #
        try:
            # for simulation
            self.cmd_vel_msg = message
            power_msg = NominalPowerSupplyStatus()
            power_msg.header.stamp = self.get_clock().now().to_msg()
            power_msg.header.frame_id = self.machine_id
            power_msg.data = True
            self.power_pub.publish(power_msg)

            accel_msg = NominalAccelerationRequest()
            accel_msg.header.stamp = self.get_clock().now().to_msg()
            accel_msg.header.frame_id = self.machine_id
            accel_msg.request = self.cmd_vel_msg.linear.x
            self.accel_pub.publish(accel_msg)

            steer_msg = NominalSteeringRequest()
            steer_msg.header.stamp = self.get_clock().now().to_msg()
            steer_msg.header.frame_id = self.machine_id
            steer_msg.request = self.cmd_vel_msg.angular.z
            self.steer_pub.publish(steer_msg)
        except:
            pass



def main(args=None):
    rclpy.init(args=args)
    optitrack_tf_state = twist_to_nominal()
    rclpy.spin(optitrack_tf_state)
    # optitrack_tf_state.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
