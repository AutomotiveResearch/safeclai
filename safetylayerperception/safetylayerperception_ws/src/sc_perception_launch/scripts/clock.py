#!/usr/bin/env python3


# Copyright (c) 2024 HAN university of applied science

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import numpy as np
import rclpy
from rclpy.node import Node
from tf2_ros.static_transform_broadcaster import StaticTransformBroadcaster
from geometry_msgs.msg import TransformStamped, Vector3, PoseStamped
from tf2_ros import TransformBroadcaster
import math
from sensor_msgs.msg import NavSatFix ,LaserScan,PointCloud2,Imu
from rclpy.qos import QoSProfile, QoSReliabilityPolicy, QoSHistoryPolicy, QoSDurabilityPolicy
from rclpy.parameter import Parameter
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Twist
from tf2_ros import TransformException
from tf2_ros.buffer import Buffer
from tf2_ros.transform_listener import TransformListener
from builtin_interfaces.msg import Time
from rosgraph_msgs.msg import Clock
class clock(Node):
    def __init__(self):
        super().__init__('clock_odom')
        simtime =Parameter("use_sim_time", Parameter.Type.BOOL, False)
        self.set_parameters([simtime])  
        self.declare_parameter('odom_pos',[0,0,0,0,0,0])
        self.declare_parameter('reset_odom',True)
        self.reset_odom = self.get_parameter('reset_odom').value
        self.odom_pos = self.get_parameter('odom_pos').value

        print("odom_pos = ",self.odom_pos)
        qos_profile = QoSProfile(
            reliability=QoSReliabilityPolicy.BEST_EFFORT,
            durability=QoSDurabilityPolicy.VOLATILE,
            history=QoSHistoryPolicy.KEEP_LAST,
            depth=1
        ) 
        self.pub_odom_tf = self.create_publisher(
            Clock,"clock",10)
        self.create_subscription(
            PointCloud2,
            "/sensing/lidar/top/velodyne_points_localization",
            self.odom_callback,
            10)
   
        self.pre_time = 0
    def odom_callback(self, message):
        msg = Clock()
        msg.clock.sec = message.header.stamp.sec    
        msg.clock.nanosec = message.header.stamp.nanosec
        time = message.header.stamp.sec + message.header.stamp.nanosec*1e-9
        if time == self.pre_time or time < self.pre_time :
            print("skip ============================================== ")
            self.pre_time = time
            return
        self.pub_odom_tf.publish(msg)
        print("clock = ",msg.clock.sec,msg.clock.nanosec)
        
        self.pre_time = time
        
        

  

def main(args=None):
    rclpy.init(args=args)
    optitrack_tf_state= clock()
    rclpy.spin(optitrack_tf_state)
    #optitrack_tf_state.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()