#!/usr/bin/env python3


# Copyright (c) 2024 HAN university of applied science

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import numpy as np
import rclpy
from rclpy.node import Node
from tf2_ros.static_transform_broadcaster import StaticTransformBroadcaster
from geometry_msgs.msg import TransformStamped, Vector3, PoseStamped
from tf2_ros import TransformBroadcaster
import math
from sensor_msgs.msg import NavSatFix ,LaserScan,PointCloud2,Imu
from rclpy.qos import QoSProfile, QoSReliabilityPolicy, QoSHistoryPolicy, QoSDurabilityPolicy
from scipy import interpolate
from nav_msgs.msg import Odometry,Path
from geometry_msgs.msg import Twist
from OSMHandler import OSMHandler
from autoware_auto_perception_msgs.msg import PredictedObjects,PredictedObject,ObjectClassification, DetectedObject, DetectedObjects
from rclpy.parameter import Parameter
from geometry_msgs.msg import Twist, Point
from tf2_ros import TransformException
from tf2_ros.buffer import Buffer
from tf2_ros.transform_listener import TransformListener
from builtin_interfaces.msg import Time
from sc_interfaces.msg import ObservedObjects,Object
import tf2_geometry_msgs, tf2_msgs
from geometry_msgs.msg import TransformStamped, Vector3, PoseStamped,Point32,PolygonStamped, Polygon
import copy
FIXED_FRAME = "front_link_link"
MAX_NUM_OBJECTS = 50 

def uint8arrary_to_hex_string(array):
    array_as_hex_string = ''.join(f'{x:02x}' for x in array)
    return array_as_hex_string

def hex_string_to_uint8array(hex_string):
    byte_array = bytes.fromhex(hex_string)
    # Convert the byte array to a NumPy array
    new_array = np.frombuffer(byte_array, dtype=np.uint8)
    return new_array
def quaternion_to_euler(quaternion):
    x, y, z, w = quaternion
    t0 = +2.0 * (w * x + y * z)
    t1 = +1.0 - 2.0 * (x * x + y * y)
    roll_x = math.atan2(t0, t1)
    t2 = +2.0 * (w * y - z * x)
    t2 = +1.0 if t2 > +1.0 else t2
    t2 = -1.0 if t2 < -1.0 else t2
    pitch_y = math.asin(t2)
    t3 = +2.0 * (w * z + x * y)
    t4 = +1.0 - 2.0 * (y * y + z * z)
    yaw_z = math.atan2(t3, t4)
    return roll_x, pitch_y, yaw_z # in radians
def vector_transform(vector, quaternion, translation):
    # Extracting components
    x, y = vector
    qx, qy, qz, qw = quaternion
    tx, ty, tz = translation
    roll, pitch, yaw = quaternion_to_euler(quaternion)

    #only yaw rotation

    new_X = x*math.cos(yaw) - y*math.sin(yaw)
    new_Y = x*math.sin(yaw) + y*math.cos(yaw)

    return [new_X,new_Y]

def compare_objects(detected_object:Object,predicted_objects:ObservedObjects):
    #compare the detected object with the predicted object
    is_overlapped = False
    for i in range(len(predicted_objects.obejcts_position)):
        predicted_object = predicted_objects.obejcts_position[i]
        dis = math.sqrt((detected_object.pos_x- predicted_object.pos_x)**2 + (detected_object.pos_y - predicted_object.pos_y)**2)
        
        if dis < 1.0:
            is_overlapped = True
            break
    return is_overlapped

class object_frame_publisher(Node):
    def __init__(self):
        super().__init__('object_frame_publisher')
        # simtime =Parameter("use_sim_time", Parameter.Type.BOOL, True)
        # self.set_parameters([simtime]) 
        self.merge_objects = True




        qos_profile = QoSProfile(
            reliability=QoSReliabilityPolicy.BEST_EFFORT,
            durability=QoSDurabilityPolicy.VOLATILE,
            history=QoSHistoryPolicy.KEEP_LAST,
            depth=1
        ) 
        self.pub_ObservedObjects= self.create_publisher(
            ObservedObjects,"sc_topic_perception_objects",10)    
   
        
        self.create_subscription( 
            PredictedObjects,
            'perception/object_recognition/objects',
            self.traced_object_callback,
            qos_profile)
        
        self.create_subscription(
            DetectedObjects,
            "/perception/object_recognition/cost_map/clusters"
            ,self.object_callback,qos_profile)

        

        self.create_subscription(
            PolygonStamped,"/sc_topic_other_safety_margin_0",self.sc_other_safety_margins_callback,qos_profile)
        self.object_publisher = []
        for i in range(0,50):
            temp =  self.create_publisher(
                PolygonStamped,"/sc_topic_other_safety_margins_"+str(i),10)

            self.object_publisher.append(temp)         
        self.sbr_ = StaticTransformBroadcaster(self)
        self.br_ = TransformBroadcaster(self)

        buffer_time = Time()
        buffer_time.sec = 0
        self.tf_buffer = Buffer(buffer_time)
        self.tf_listener = TransformListener(self.tf_buffer, self)
        self.objects_id = [] 
        self.len = 0
        self.len_object_no_twist = 0
        self.object_no_twist = []
        self.total_length = 0

    def sc_other_safety_margins_callback(self,message:PolygonStamped):
            try:
                cur_polygon = message
                loop_time = np.clip(self.total_length,0,50)
                for i in range(loop_time):
                    PolygonStamped_temp = PolygonStamped()
                    PolygonStamped_temp.header.frame_id = FIXED_FRAME
                    PolygonStamped_temp.header.stamp = self.get_clock().now().to_msg()
                    PolygonStamped_temp.polygon.points = []
                    for j in range(31):
                        temp_point = Point32()
                        temp_point.x = cur_polygon.polygon.points[(i*31)+j].x
                        temp_point.y = cur_polygon.polygon.points[(i*31)+j].y
                        temp_point.z = 0.0
                        PolygonStamped_temp.polygon.points.append(temp_point)
                    self.object_publisher[i].publish(PolygonStamped_temp)

                for i in range(self.total_length,MAX_NUM_OBJECTS):
                    PolygonStamped_temp = PolygonStamped()
                    PolygonStamped_temp.header.frame_id = FIXED_FRAME
                    PolygonStamped_temp.header.stamp = self.get_clock().now().to_msg()
                    self.object_publisher[i].publish(PolygonStamped_temp)
            except BaseException as e:
                print(e)  
                


          



    def object_callback(self,message:DetectedObjects):

        try:
            frame = message.header.frame_id
            self.len_object_no_twist = len(message.objects)
            if self.len_object_no_twist > 50:
                self.len_object_no_twist = 50
            self.object_no_twist.clear()
            for i in range(self.len_object_no_twist):

        
                pose = PoseStamped()
                pose.header.stamp = self.get_clock().now().to_msg()
                pose.header.frame_id = frame
                current_object = message.objects[i]
                # current_object = PredictedObject()
                pose.pose.position.x = message.objects[i].kinematics.pose_with_covariance.pose.position.x
                pose.pose.position.y = message.objects[i].kinematics.pose_with_covariance.pose.position.y
                pose.pose.position.z = message.objects[i].kinematics.pose_with_covariance.pose.position.z
                pose.pose.orientation.x = message.objects[i].kinematics.pose_with_covariance.pose.orientation.x
                pose.pose.orientation.y = message.objects[i].kinematics.pose_with_covariance.pose.orientation.y
                pose.pose.orientation.z = message.objects[i].kinematics.pose_with_covariance.pose.orientation.z
                pose.pose.orientation.w = message.objects[i].kinematics.pose_with_covariance.pose.orientation.w


                
                #apply the transformation to the object
                transform = self.tf_buffer.lookup_transform(
                        FIXED_FRAME,
                        frame,
                        Time()
                    )


                pose_transformed = tf2_geometry_msgs.do_transform_pose(pose.pose, transform)
                object_tmp = Object()
                object_tmp.pos_x = pose_transformed.position.x
                object_tmp.pos_y = pose_transformed.position.y
                object_tmp.pos_z = pose_transformed.position.z

                object_tmp.v_x = 0.0
                object_tmp.v_y = 0.0
                object_tmp.v_z = 0.0
                object_tmp.hitbox_x = 2.0
                object_tmp.hitbox_y = 2.0
                self.object_no_twist.append(object_tmp)
        except BaseException as e:
            print(e)


    def traced_object_callback(self,message:PredictedObjects):

        try:
            frame = message.header.frame_id
            
            length = len(message.objects)
            self.len = length
            sc_objects = ObservedObjects()
            sc_objects.header = message.header
            sc_objects.header.frame_id = FIXED_FRAME
            sc_objects.len = length
            sc_objects.obejcts_position
            object_list = []
            self.objects_id = []
            for i in range(length):
          
                string_id = uint8arrary_to_hex_string(message.objects[i].object_id.uuid)
                self.objects_id.append(string_id)
        
                pose = PoseStamped()
                pose.header.stamp = self.get_clock().now().to_msg()
                pose.header.frame_id = frame
                current_object = message.objects[i]
                # current_object = PredictedObject()
                pose.pose.position.x = message.objects[i].kinematics.initial_pose_with_covariance.pose.position.x
                pose.pose.position.y = message.objects[i].kinematics.initial_pose_with_covariance.pose.position.y
                pose.pose.position.z = message.objects[i].kinematics.initial_pose_with_covariance.pose.position.z
                pose.pose.orientation.x = message.objects[i].kinematics.initial_pose_with_covariance.pose.orientation.x
                pose.pose.orientation.y = message.objects[i].kinematics.initial_pose_with_covariance.pose.orientation.y
                pose.pose.orientation.z = message.objects[i].kinematics.initial_pose_with_covariance.pose.orientation.z
                pose.pose.orientation.w = message.objects[i].kinematics.initial_pose_with_covariance.pose.orientation.w


                
                #apply the transformation to the object
                transform = self.tf_buffer.lookup_transform(
                        FIXED_FRAME,
                        frame,
                        Time()
                    )


                pose_transformed = tf2_geometry_msgs.do_transform_pose(pose.pose, transform)
                object_tmp = Object()
                object_tmp.pos_x = pose_transformed.position.x
                object_tmp.pos_y = pose_transformed.position.y
                object_tmp.pos_z = pose_transformed.position.z

                ve_vetor = vector_transform([current_object.kinematics.initial_twist_with_covariance.twist.linear.x,current_object.kinematics.initial_twist_with_covariance.twist.linear.y],[pose_transformed.orientation.x,pose_transformed.orientation.y,pose_transformed.orientation.z,pose_transformed.orientation.w],[pose_transformed.position.x,pose_transformed.position.y,pose_transformed.position.z])
                object_tmp.v_x = ve_vetor[0]
                object_tmp.v_y = ve_vetor[1]
                object_tmp.v_z = 0.0
                object_tmp.hitbox_x = 2.0
                object_tmp.hitbox_y = 2.0
                sc_objects.obejcts_position[i] = object_tmp
            #print("object_list",self.objects_id)
            ## merge the object without twist and the object with twist
            if self.merge_objects:
                counter = 0
                sc_object_compare = copy.deepcopy(sc_objects)
                for i in range(self.len_object_no_twist):
                        is_overlapped = compare_objects(self.object_no_twist[i],sc_object_compare)
                        
                        if is_overlapped == False:
                            if self.len + counter < MAX_NUM_OBJECTS:
                                sc_objects.obejcts_position[self.len + counter] = self.object_no_twist[i]
                                counter = counter + 1
                       
                self.total_length = self.len + counter

                    
                sc_objects.len = self.total_length
                if sc_objects.len > 50:
                    sc_objects.len = 50

            else:
                self.total_length = self.len    
            # else:
            #     for i in range(self.len_object_no_twist):
            #         sc_objects.obejcts_position[i] = self.object_no_twist[i]
            #     sc_objects.len = self.len_object_no_twist

            self.pub_ObservedObjects.publish(sc_objects)
        except BaseException as e:
            print(e)
                
  


        # self.br_.sendTransform(object_list)

def main(args=None):
    rclpy.init(args=args)
    optitrack_tf_state= object_frame_publisher()
    rclpy.spin(optitrack_tf_state)
    #optitrack_tf_state.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()