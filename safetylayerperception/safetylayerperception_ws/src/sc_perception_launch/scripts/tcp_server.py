import socket

# Create a TCP/IP socket
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Bind the socket to the address and port
server_address = ('127.0.0.1', 5000)
server_socket.bind(server_address)

# Listen for incoming connections
server_socket.listen(5)  # Maximum 5 connections in the queue

print("Server is waiting for connections...")

while True:
    # Wait for a connection
    connection, client_address = server_socket.accept()

    try:
        print(f"Connection from {client_address}")

        # Receive data from the client
        while True:
            data = connection.recv(4096)  # Buffer size is 1024 bytes
            if not data:
                break
               
            print(','.join(format(x, '02x') for x in data))
            print(len(data))

            # Send data back to the client
            connection.sendall(data)

    finally:
        # Close the connection
        connection.close()
