#!/usr/bin/env python3

# Copyright (c) 2024 HAN university of applied science

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import numpy as np
import rclpy
from rclpy.node import Node
from sensor_msgs.msg import Joy
from geometry_msgs.msg import TwistStamped, TransformStamped
import os
class joy_twist(Node):
    def __init__(self):
        super().__init__('joy_twist')
        self.declare_parameter('machine_id', "Robot1")
        self.machine_id = self.get_parameter('machine_id').value
        self.declare_parameter('initial_pose', [0, 0, 0, 0])
        self.initial_pose = self.get_parameter('initial_pose').value

        self.cmd_vel_pub = self.create_publisher(TwistStamped, 'twist_cmd', 10)

        self.create_subscription(
            Joy,
            'joy',
            self.pub_cmd,
            10)
 
        self.first_hold = False
    def pub_cmd(self, message: Joy):
        
        ## the linear x will be the sum of the l2 and R2 remapped to -10 to 10

        #
        try:
            ##if the button 9 is pressed for 3 seconds, the initial pose will be reset
            if message.buttons[9] == 1:
                if self.first_hold == False:
                    self.first_hold = True
                    self.hold_time = self.get_clock().now().to_msg().sec + self.get_clock().now().to_msg().nanosec*1e-9
                else:
                    if self.get_clock().now().to_msg().sec + self.get_clock().now().to_msg().nanosec*1e-9 - self.hold_time > 2:
                        
                        os.system('ros2 service call /sc_service_hmi_activation_request sc_interfaces/srv/HMIActivationRequest "{data: False}"')
                        os.system('ros2 service call /sc_service_hmi_activation_request sc_interfaces/srv/HMIActivationRequest "{data: True}"')
                        self.get_logger().info('Reset and set the safety state')
                        self.first_hold = False
            else:
                self.first_hold = False
                
        except:
            pass

def main(args=None):
    rclpy.init(args=args)
    optitrack_tf_state = joy_twist()
    rclpy.spin(optitrack_tf_state)
    # optitrack_tf_state.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
