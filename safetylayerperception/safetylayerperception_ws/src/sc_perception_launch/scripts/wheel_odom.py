#!/usr/bin/env python3


# Copyright (c) 2024 HAN university of applied science

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import numpy as np
import rclpy
from rclpy.node import Node
from tf2_ros.static_transform_broadcaster import StaticTransformBroadcaster
from geometry_msgs.msg import TransformStamped, Vector3, PoseStamped
from tf2_ros import TransformBroadcaster
import math
from sensor_msgs.msg import NavSatFix ,LaserScan,PointCloud2,Imu
from rclpy.qos import QoSProfile, QoSReliabilityPolicy, QoSHistoryPolicy, QoSDurabilityPolicy
from rclpy.parameter import Parameter
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Twist
from tf2_ros import TransformException
from tf2_ros.buffer import Buffer
from tf2_ros.transform_listener import TransformListener
from builtin_interfaces.msg import Time

rear_tyre_radius = 0.281
rear_wheel_mass = 10.08
rear_tyre_width = 0.145
front_tyre_radius = 0.265
front_wheel_mass = 9.14
front_tyre_width = 0.125
friction_dry_mu = 0.95
friction_wet_mu = 0.4
wheelbase = 1.686

def quaternion_to_euler(x, y, z, w):
  
    t0 = +2.0 * (w * x + y * z)
    t1 = +1.0 - 2.0 * (x * x + y * y)
    roll_x = math.atan2(t0, t1)
    t2 = +2.0 * (w * y - z * x)
    t2 = +1.0 if t2 > +1.0 else t2
    t2 = -1.0 if t2 < -1.0 else t2
    pitch_y = math.asin(t2)
    t3 = +2.0 * (w * z + x * y)
    t4 = +1.0 - 2.0 * (y * y + z * z)
    yaw_z = math.atan2(t3, t4)
    return [roll_x , pitch_y, yaw_z] # in radians

def angle_subtract(angle1, angle2):
  
    # Calculate the absolute difference between the angles
    abs_diff = (angle1 - angle2)
    # If the angle is greater than pi radians, subtract 2pi radians.
    if abs_diff > math.pi:
        abs_diff -= 2 * math.pi
    # If the angle is less than -pi radians, add 2pi radians.
    elif abs_diff < -math.pi:
        abs_diff += 2 * math.pi

    return abs_diff
class wheel(Node):
    def __init__(self):
        super().__init__('wheel_odom')
        simtime =Parameter("use_sim_time", Parameter.Type.BOOL, True)
        self.set_parameters([simtime])  
        self.declare_parameter('odom_pos',[0,0,0,0,0,0])
        self.declare_parameter('reset_odom',True)
        self.reset_odom = self.get_parameter('reset_odom').value
        self.odom_pos = self.get_parameter('odom_pos').value

        print("odom_pos = ",self.odom_pos)
        qos_profile = QoSProfile(
            reliability=QoSReliabilityPolicy.BEST_EFFORT,
            durability=QoSDurabilityPolicy.VOLATILE,
            history=QoSHistoryPolicy.KEEP_LAST,
            depth=1
        ) 
        self.pub_odom_tf = self.create_publisher(
            Odometry,"wheel_odom",10)
        self.create_subscription(
            Odometry,
            "odom",
            self.odom_callback,
            qos_profile)
        
        self.create_subscription(
            Odometry,
            "odom/gps",
            self.odom_callback_1,
            qos_profile)
        self.imu_raw = self.create_subscription(
            Imu,
            '/sensing/imu/imu_data',
            self.record_imu,
            qos_profile)
        
        buffer_time = Time()
        buffer_time.sec = 1
        self.tf_buffer = Buffer(buffer_time)
        self.tf_listener = TransformListener(self.tf_buffer, self)
        self.timer = self.create_timer(0.01, self.on_frame_listener)
        self.steer_rad = 0.0
        self.pre_time = 0.0
        self.pre_front_left_wheel_frame_rotation = [0.0,0.0,0.0]
        self.pre_front_right_wheel_frame_rotation =[0.0,0.0,0.0]
        self.pre_rear_left_wheel_frame_rotation = [0.0,0.0,0.0]
        self.pre_rear_right_wheel_frame_rotation = [0.0,0.0,0.0]
        self.pre_linear_velocity = 0.0
        self.current_linear_velocity = 0.0
        self.odom_ground_truth = [0.0,0.0,0.0,0.0,0.0,0.0]
        self.imu_linear_v_x = 0.0
        self.pre_imu_stamp = 0.0
        self.yaw = 0.0
        self.steer_rad_pre  = 0.0 
    def record_imu(self,message:Imu):

        r,p,yaw = quaternion_to_euler(message.orientation.x,message.orientation.y,message.orientation.z,message.orientation.w)
        # self.odom_pos[5] = yaw
        self.yaw = yaw
        current_stamp = message.header.stamp.sec + message.header.stamp.nanosec*1e-9
        current_time = self.get_clock().now().to_msg().sec + self.get_clock().now().to_msg().nanosec*1e-9
        acc_x = message.linear_acceleration.x + 0.23
        if self.pre_imu_stamp == 0.0 or self.pre_time == 0.0 :
            self.pre_imu_stamp = current_stamp
            self.pre_time = current_time
            return
        ## integrating the acceleration to get the velocity
        self.imu_linear_v_x += acc_x * (current_stamp - self.pre_imu_stamp)
        print("g_t_yaw ", (self.yaw) )
        self.pre_imu_stamp = current_stamp



        
        dt = current_time - self.pre_time
      
        odom_msg = Odometry()
        odom_msg.header.stamp = self.get_clock().now().to_msg()
        odom_msg.header.frame_id = "odom"
        odom_msg.child_frame_id = "base_link"


        vehicle_linear_velocity = self.current_linear_velocity
        # if self.pre_linear_velocity < 0.0:
        #     self.pre_linear_velocity = vehicle_linear_velocity
        # a = 0.1
        # vehicle_linear_velocity = vehicle_linear_velocity*a + self.pre_linear_velocity*(1-a)
      
                
        angular_velocity = vehicle_linear_velocity * math.tan(self.steer_rad) / wheelbase
                
        odom_msg.twist.twist.linear.x = vehicle_linear_velocity
        odom_msg.twist.twist.angular.z = angular_velocity

        self.odom_pos[5] += angular_velocity * dt     

        
        if self.odom_pos[5] > math.pi:
            self.odom_pos[5] -=  2*math.pi
        # If the angle is less than -pi radians, add 2pi radians.
        elif self.odom_pos[5] < -math.pi:
            self.odom_pos[5] +=  2*math.pi    
        x_dot = vehicle_linear_velocity * np.cos(self.odom_pos[5])
        y_dot = vehicle_linear_velocity * np.sin(self.odom_pos[5])
        self.odom_pos[0] += x_dot * dt
        self.odom_pos[1] += y_dot * dt
         
        
        print(angular_velocity * dt)



        odom_msg.pose.pose.position.x = self.odom_pos[0]
        odom_msg.pose.pose.position.y = self.odom_pos[1]
        odom_msg.pose.pose.orientation.x = 0.0
        odom_msg.pose.pose.orientation.y = 0.0
        odom_msg.pose.pose.orientation.z = np.sin(self.odom_pos[5]/2.0)
        odom_msg.pose.pose.orientation.w = np.cos(self.odom_pos[5]/2.0)

               
        odom_msg.pose.covariance[0]  = 0.2 
        odom_msg.pose.covariance[7]  = 0.2 
        odom_msg.pose.covariance[35] = 0.4 

        print("current_pos", self.odom_pos)
       # print("current_pos_gound_truth", self.odom_ground_truth)
        self.pub_odom_tf.publish(odom_msg)

        self.pre_linear_velocity = vehicle_linear_velocity


        self.pre_time = current_time

    def odom_callback(self, message:Odometry):
        self.current_linear_velocity = math.sqrt(message.twist.twist.linear.x **2 + message.twist.twist.linear.y**2)
        
        
        

    def odom_callback_1(self, message:Odometry):
        
        
        r,p,y = quaternion_to_euler(message.pose.pose.orientation.x,message.pose.pose.orientation.y,message.pose.pose.orientation.z,message.pose.pose.orientation.w)
        self.odom_ground_truth = [message.pose.pose.position.x,message.pose.pose.position.y,message.pose.pose.position.z,r,p,self.yaw]
        self.reset_odom = self.get_parameter('reset_odom').value
        if self.reset_odom == True:
            self.odom_pos = self.odom_ground_truth
            self.reset_odom = False
            self.set_parameters([Parameter('reset_odom', Parameter.Type.BOOL, False)])
            self.set_parameters([Parameter('odom_pos', Parameter.Type.DOUBLE_ARRAY, self.odom_pos)])
    def on_frame_listener(self):
        try:
            current_time = self.get_clock().now().to_msg().sec + self.get_clock().now().to_msg().nanosec*1e-9
            dt = current_time - self.pre_time
            time_tf = Time()
            from_frame_rel = "base_link"
            to_frame_rel_left = "front_left_wheel"
            to_frame_rel_right = "front_right_wheel"
            to_frame_rear_left = "rear_left_wheel"
            to_frame_rear_right = "rear_right_wheel"
            front_left_wheel_frame = self.tf_buffer.lookup_transform(
                to_frame_rel_left,
                from_frame_rel,
                time_tf
            )
            front_right_wheel_frame = self.tf_buffer.lookup_transform(
                to_frame_rel_right,
                from_frame_rel,
                time_tf
            )
            rear_left_wheel_frame = self.tf_buffer.lookup_transform(
                to_frame_rear_left,
                from_frame_rel,
                time_tf
            )
            rear_right_wheel_frame = self.tf_buffer.lookup_transform(
                to_frame_rear_right,
                from_frame_rel,
                time_tf
            )

            
         
            front_left_wheel_frame_rotation = quaternion_to_euler(front_left_wheel_frame.transform.rotation.x, front_left_wheel_frame.transform.rotation.y, front_left_wheel_frame.transform.rotation.z, front_left_wheel_frame.transform.rotation.w) 
            front_right_wheel_frame_rotation = quaternion_to_euler(front_right_wheel_frame.transform.rotation.x, front_right_wheel_frame.transform.rotation.y, front_right_wheel_frame.transform.rotation.z, front_right_wheel_frame.transform.rotation.w)

            rear_left_wheel_frame_rotation = quaternion_to_euler(rear_left_wheel_frame.transform.rotation.x, rear_left_wheel_frame.transform.rotation.y, rear_left_wheel_frame.transform.rotation.z, rear_left_wheel_frame.transform.rotation.w)
            rear_right_wheel_frame_rotation = quaternion_to_euler(rear_right_wheel_frame.transform.rotation.x, rear_right_wheel_frame.transform.rotation.y, rear_right_wheel_frame.transform.rotation.z, rear_right_wheel_frame.transform.rotation.w)
            self.steer_rad = (front_left_wheel_frame_rotation[1] + front_right_wheel_frame_rotation[1])/2
            a = 0.2
            if self.steer_rad_pre == 0.0:
                self.steer_rad_pre = self.steer_rad
            self.steer_rad = self.steer_rad*a + self.steer_rad_pre*(1-a)
            self.steer_rad_pre = self.steer_rad

         
            # print("front_right_wheel_frame_rotation",front_right_wheel_frame_rotation)
            # print("rear_left_wheel_frame_rotation",rear_left_wheel_frame_rotation)
            # print("rear_right_wheel_frame_rotation",rear_right_wheel_frame_rotation)
            # if 1:

                # odom_msg = Odometry()
                # odom_msg.header.stamp = self.get_clock().now().to_msg()
                # odom_msg.header.frame_id = "odom"
                # odom_msg.child_frame_id = "base_link"

                # ## using dt to calculate individual wheel frame's angular velocity rad/s
                # front_left_wheel_frame_angular_velocity = [angle_subtract(front_left_wheel_frame_rotation[0], self.pre_front_left_wheel_frame_rotation[0])/dt, angle_subtract(front_left_wheel_frame_rotation[1], self.pre_front_left_wheel_frame_rotation[1])/dt, angle_subtract(front_left_wheel_frame_rotation[2], self.pre_front_left_wheel_frame_rotation[2])/dt]
                # front_right_wheel_frame_angular_velocity = [angle_subtract(front_right_wheel_frame_rotation[0], self.pre_front_right_wheel_frame_rotation[0])/dt, -angle_subtract(front_right_wheel_frame_rotation[1], self.pre_front_right_wheel_frame_rotation[1])/dt, angle_subtract(front_right_wheel_frame_rotation[2], self.pre_front_right_wheel_frame_rotation[2])/dt]
                # rear_left_wheel_frame_angular_velocity = [angle_subtract(rear_left_wheel_frame_rotation[0], self.pre_rear_left_wheel_frame_rotation[0])/dt, -angle_subtract(rear_left_wheel_frame_rotation[1], self.pre_rear_left_wheel_frame_rotation[1])/dt, angle_subtract(rear_left_wheel_frame_rotation[2],self.pre_rear_left_wheel_frame_rotation[2])/dt]
                # rear_right_wheel_frame_angular_velocity = [angle_subtract(rear_right_wheel_frame_rotation[0], self.pre_rear_right_wheel_frame_rotation[0])/dt, angle_subtract(rear_right_wheel_frame_rotation[1], self.pre_rear_right_wheel_frame_rotation[1])/dt, angle_subtract(rear_right_wheel_frame_rotation[2], self.pre_rear_right_wheel_frame_rotation[2])/dt]

                
                # print("front_left_wheel_frame_rotation",np.degrees(front_left_wheel_frame_rotation[2]))
                # # print("delta theta ",angle_subtract(front_left_wheel_frame_rotation[2], self.pre_front_left_wheel_frame_rotation[2]))
                # ## use angular velocity to calculate individual wheel frame's linear velocity m/s
                # front_left_wheel_frame_linear_velocity = [front_left_wheel_frame_angular_velocity[0]*front_tyre_radius, front_left_wheel_frame_angular_velocity[1]*front_tyre_radius, front_left_wheel_frame_angular_velocity[2]*front_tyre_radius]
                # front_right_wheel_frame_linear_velocity = [front_right_wheel_frame_angular_velocity[0]*front_tyre_radius, front_right_wheel_frame_angular_velocity[1]*front_tyre_radius, front_right_wheel_frame_angular_velocity[2]*front_tyre_radius]
                # rear_left_wheel_frame_linear_velocity = [rear_left_wheel_frame_angular_velocity[0]*rear_tyre_radius, rear_left_wheel_frame_angular_velocity[1]*rear_tyre_radius, rear_left_wheel_frame_angular_velocity[2]*rear_tyre_radius]
                # rear_right_wheel_frame_linear_velocity = [rear_right_wheel_frame_angular_velocity[0]*rear_tyre_radius, rear_right_wheel_frame_angular_velocity[1]*rear_tyre_radius, rear_right_wheel_frame_angular_velocity[2]*rear_tyre_radius]

                # ## average the linear velocity of the left and right wheels to get the linear velocity of the vehicle
                # vehicle_linear_velocity = - (front_left_wheel_frame_linear_velocity[2] + front_right_wheel_frame_linear_velocity[2] )/2
                
                # ## apply the exponential filter to the linear velocity of the vehicle
                # a = 0.8
                # vehicle_linear_velocity = vehicle_linear_velocity*a + self.pre_linear_velocity*(1-a)
                # vehicle_linear_velocity = self.imu_linear_v_x

                ## calculate the steering angle of the vehicle
              
                # angular_velocity = vehicle_linear_velocity * math.tan(self.steer_rad) / wheelbase
                
                # odom_msg.twist.twist.linear.x = vehicle_linear_velocity
                # odom_msg.twist.twist.angular.z = angular_velocity

                
                # x_dot = vehicle_linear_velocity * np.cos(self.odom_pos[5])
                # y_dot = vehicle_linear_velocity * np.sin(self.odom_pos[5])
                # self.odom_pos[0] += x_dot * dt
                # self.odom_pos[1] += y_dot * dt
               

                # odom_msg.pose.pose.position.x = self.odom_pos[0]
                # odom_msg.pose.pose.position.y = self.odom_pos[1]
                # odom_msg.pose.pose.orientation.x = 0.0
                # odom_msg.pose.pose.orientation.y = 0.0
                # odom_msg.pose.pose.orientation.z = np.sin(self.odom_pos[5]/2.0)
                # odom_msg.pose.pose.orientation.w = np.cos(self.odom_pos[5]/2.0)

               
                # odom_msg.pose.covariance[0]  = 0.2 
                # odom_msg.pose.covariance[7]  = 0.2 
                # odom_msg.pose.covariance[35] = 0.4 

                # print("current_pos", self.odom_pos)
                # print("current_pos_gound_truth", self.odom_ground_truth)
                # self.pub_odom_tf.publish(odom_msg)

                # self.pre_linear_velocity = vehicle_linear_velocity
                # self.pre_front_left_wheel_frame_rotation = front_left_wheel_frame_rotation
                # self.pre_front_right_wheel_frame_rotation = front_right_wheel_frame_rotation
                # self.pre_rear_left_wheel_frame_rotation = rear_left_wheel_frame_rotation
                # self.pre_rear_right_wheel_frame_rotation = rear_right_wheel_frame_rotation

                # self.pre_time = current_time
        except TransformException as ex:
            self.get_logger().warning(
                f'Could not transform front wheel to {from_frame_rel}, please use sim angle for wheel feedback: {ex}')
            
            return    
  

def main(args=None):
    rclpy.init(args=args)
    optitrack_tf_state= wheel()
    rclpy.spin(optitrack_tf_state)
    #optitrack_tf_state.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()