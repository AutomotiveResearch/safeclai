#!/usr/bin/env python3

# Copyright (c) 2024 HAN university of applied science

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import numpy as np
import rclpy
from rclpy.node import Node
from tf2_ros.static_transform_broadcaster import StaticTransformBroadcaster
from geometry_msgs.msg import TransformStamped, Vector3, PoseStamped
from tf2_ros import TransformBroadcaster
import math
from sensor_msgs.msg import NavSatFix ,LaserScan,PointCloud2,Imu
from rclpy.qos import QoSProfile, QoSReliabilityPolicy, QoSHistoryPolicy, QoSDurabilityPolicy
from rclpy.parameter import Parameter
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Twist
from tf2_ros import TransformException
from tf2_ros.buffer import Buffer
from tf2_ros.transform_listener import TransformListener
from builtin_interfaces.msg import Time
from sc_interfaces.msg import SystemState, IsVechicleMoving ,SystemQuality, IgnitionSignal
from autoware_auto_vehicle_msgs.msg import VelocityReport

rear_tyre_radius = 0.281
rear_wheel_mass = 10.08
rear_tyre_width = 0.145
front_tyre_radius = 0.265
front_wheel_mass = 9.14
front_tyre_width = 0.125
friction_dry_mu = 0.95
friction_wet_mu = 0.4
wheelbase = 1.686

def quaternion_to_euler(x, y, z, w):
  
    t0 = +2.0 * (w * x + y * z)
    t1 = +1.0 - 2.0 * (x * x + y * y)
    roll_x = math.atan2(t0, t1)
    t2 = +2.0 * (w * y - z * x)
    t2 = +1.0 if t2 > +1.0 else t2
    t2 = -1.0 if t2 < -1.0 else t2
    pitch_y = math.asin(t2)
    t3 = +2.0 * (w * z + x * y)
    t4 = +1.0 - 2.0 * (y * y + z * z)
    yaw_z = math.atan2(t3, t4)
    return [roll_x , pitch_y, yaw_z] # in radians

def angle_subtract(angle1, angle2):
  
    # Calculate the absolute difference between the angles
    abs_diff = (angle1 - angle2)
    # If the angle is greater than pi radians, subtract 2pi radians.
    if abs_diff > math.pi:
        abs_diff -= 2 * math.pi
    # If the angle is less than -pi radians, add 2pi radians.
    elif abs_diff < -math.pi:
        abs_diff += 2 * math.pi

    return abs_diff
class fts_interface(Node):
    def __init__(self):
        super().__init__('fts_interface')
        # simtime =Parameter("use_sim_time", Parameter.Type.BOOL, True)
        # self.set_parameters([simtime])  
        self.declare_parameter('odom_pos',[0,0,0,0,0,0])
        self.declare_parameter('reset_odom',True)
        self.reset_odom = self.get_parameter('reset_odom').value
        self.odom_pos = self.get_parameter('odom_pos').value
        self.is_sim = self.get_parameter('use_sim_time').value
        self.get_logger().info("self.is_sim = " + self.is_sim.__str__())
        print("odom_pos = ",self.odom_pos)
        qos_profile = QoSProfile(
            reliability=QoSReliabilityPolicy.BEST_EFFORT,
            durability=QoSDurabilityPolicy.VOLATILE,
            history=QoSHistoryPolicy.KEEP_LAST,
            depth=1
        ) 

        
        ## major odom source 
        self.create_subscription(
            Odometry,
            "/localization/kinematic_state",
            self.odom_callback,
            qos_profile)
        
                
        self.create_subscription(
             VelocityReport, "/vehicle/status/velocity_status",self.tuth_vel_callback, 10)
        
        self.state_pub = self.create_publisher(
            SystemState,"/sc/system_state", 10)
        
        self.ignition_signal_pub = self.create_publisher(
            IgnitionSignal,"/sc_topic_ignition_signal", 10)
        self.vehicle_moving_pub = self.create_publisher(
            IsVechicleMoving,"/sc_topic_vehicle_moving", 10)
        self.system_quality_pub = self.create_publisher(
            SystemQuality,"/sc_topic_system_quality", 10)
        
        buffer_time = Time()
        buffer_time.sec = 1
        self.tf_buffer = Buffer(buffer_time)
        self.tf_listener = TransformListener(self.tf_buffer, self)
        self.timer = self.create_timer(0.01, self.on_frame_listener)
        self.timer_fault_check = self.create_timer(0.01, self.on_timer)
        self.steer_rad = 0.0
        self.pre_time = 0.0
        self.pre_time_odom = 0.0
        self.pre_front_left_wheel_frame_rotation = [0.0,0.0,0.0]
        self.pre_front_right_wheel_frame_rotation =[0.0,0.0,0.0]
        self.pre_rear_left_wheel_frame_rotation = [0.0,0.0,0.0]
        self.pre_rear_right_wheel_frame_rotation = [0.0,0.0,0.0]
        self.pre_linear_velocity = 0.0
        self.current_linear_velocity = 0.0
        self.odom_ground_truth = [0.0,0.0,0.0,0.0,0.0,0.0]
        self.imu_linear_v_x = 0.0
        self.pre_imu_stamp = 0.0
        self.yaw = 0.0
        self.steer_rad_pre  = 0.0 
        
        self.pre_odom = [0.0, 0.0]
        self.velocity_pos = 0.0
        self.velocity_power_train = 0.0
        self.error_counter = 0 
        
    def tuth_vel_callback(self, message:VelocityReport):
        self.velocity_power_train = math.sqrt(message.longitudinal_velocity**2 + message.lateral_velocity**2)

    def odom_callback(self, message:Odometry):
        
        current_stamp_time =message.header.stamp.sec + message.header.stamp.nanosec*1e-9

        dt = current_stamp_time - self.pre_time_odom

        if dt == 0.0:
            return
        
        current_odom_pos = [message.pose.pose.position.x,message.pose.pose.position.y]
        self.velocity_pos = math.sqrt((current_odom_pos[0] - self.pre_odom[0])**2 + (current_odom_pos[1] - self.pre_odom[1])**2)/dt


        self.current_linear_velocity = math.sqrt(message.twist.twist.linear.x **2 + message.twist.twist.linear.y**2)
        ## if the twist x is negative, the velocity is negative
        if message.twist.twist.linear.x < 0:
            self.current_linear_velocity = -self.current_linear_velocity
        r,p,yaw = quaternion_to_euler(message.pose.pose.orientation.x,message.pose.pose.orientation.y,message.pose.pose.orientation.z,message.pose.pose.orientation.w)
        ##
        
        a = 0.1
        if self.pre_linear_velocity == 0.0:
            self.pre_linear_velocity = self.current_linear_velocity
        self.current_linear_velocity = self.current_linear_velocity*a + self.pre_linear_velocity*(1-a)
        self.pre_linear_velocity = self.current_linear_velocity
        
        systemstate_msg  = SystemState()
        systemstate_msg.header.stamp = self.get_clock().now().to_msg()
        systemstate_msg.header.frame_id = "base_link"
        systemstate_msg.velocity =  message.twist.twist.linear.x

        systemstate_msg.steering_angle = -self.steer_rad
        if self.is_sim:
            systemstate_msg.steering_angle = -self.steer_rad

        systemstate_msg.heading = yaw
        systemstate_msg.safety_sensor_performance = True
        systemstate_msg.safety_sensor_quality = True
        systemstate_msg.actuator_performance = True
        systemstate_msg.actuator_quality = True

        self.state_pub.publish(systemstate_msg)

        ignition_msg = IgnitionSignal()
        ignition_msg.header.stamp = self.get_clock().now().to_msg()
        ignition_msg.header.frame_id = "base_link"
        ignition_msg.data = True
        self.ignition_signal_pub.publish(ignition_msg)

        vehicle_moving_msg = IsVechicleMoving()
        vehicle_moving_msg.header.stamp = self.get_clock().now().to_msg()
        vehicle_moving_msg.header.frame_id = "base_link"
        if message.twist.twist.linear.x <= 0.1:
            vehicle_moving_msg.data = False
        else:
            vehicle_moving_msg.data = True
        self.vehicle_moving_pub.publish(vehicle_moving_msg)



        self.pre_time_odom = current_stamp_time
        self.pre_odom = current_odom_pos
        
        


    def on_timer(self):
        

        system_quality_msg = SystemQuality()
        system_quality_msg.header.stamp = self.get_clock().now().to_msg()
        system_quality_msg.header.frame_id = "base_link"
        system_quality_msg.quality = True
    
        self.get_logger().info("self.velocity_pos = " + self.velocity_pos.__str__())
        self.get_logger().info("self.velocity_power_train = " + self.velocity_power_train.__str__())

        vel_threshhold = 0.5 # m/s
        if abs(self.velocity_pos- self.velocity_power_train) > vel_threshhold:
            self.error_counter += 1
        else:
            self.error_counter = 0
        if self.error_counter > 10:
            self.get_logger().error("huge difference between velocity_pos and velocity_power_train")
            # system_quality_msg.quality = False

        self.system_quality_pub.publish(system_quality_msg)

    def on_frame_listener(self):
        try:

            time_tf = Time()
            from_frame_rel = "base_link"
            to_frame_rel_left = "front_left_wheel"
            to_frame_rel_right = "front_right_wheel"

            front_left_wheel_frame = self.tf_buffer.lookup_transform(
                to_frame_rel_left,
                from_frame_rel,
                time_tf
            )
            front_right_wheel_frame = self.tf_buffer.lookup_transform(
                to_frame_rel_right,
                from_frame_rel,
                time_tf
            )

            
         
            front_left_wheel_frame_rotation = quaternion_to_euler(front_left_wheel_frame.transform.rotation.x, front_left_wheel_frame.transform.rotation.y, front_left_wheel_frame.transform.rotation.z, front_left_wheel_frame.transform.rotation.w) 
            front_right_wheel_frame_rotation = quaternion_to_euler(front_right_wheel_frame.transform.rotation.x, front_right_wheel_frame.transform.rotation.y, front_right_wheel_frame.transform.rotation.z, front_right_wheel_frame.transform.rotation.w)

            self.steer_rad = (front_left_wheel_frame_rotation[1] + front_right_wheel_frame_rotation[1])/2
            a = 0.4
            if self.steer_rad_pre == 0.0:
                self.steer_rad_pre = self.steer_rad
            self.steer_rad = self.steer_rad*a + self.steer_rad_pre*(1-a)
            self.steer_rad_pre = self.steer_rad

         
        except TransformException as ex:
            self.get_logger().warning(
                f'Could not transform front wheel to {from_frame_rel}, please use sim angle for wheel feedback: {ex}')
            
            return    
  

def main(args=None):
    rclpy.init(args=args)
    optitrack_tf_state= fts_interface()
    rclpy.spin(optitrack_tf_state)
    #optitrack_tf_state.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()