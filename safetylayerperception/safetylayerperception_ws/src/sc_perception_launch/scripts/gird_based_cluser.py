#!/usr/bin/env python3


# Copyright (c) 2024 HAN university of applied science

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import numpy as np
import rclpy
from rclpy.node import Node
from tf2_ros.static_transform_broadcaster import StaticTransformBroadcaster
from geometry_msgs.msg import TransformStamped, Vector3, PoseStamped
from tf2_ros import TransformBroadcaster
import math
from sensor_msgs.msg import NavSatFix ,LaserScan,PointCloud2,Imu
from rclpy.qos import QoSProfile, QoSReliabilityPolicy, QoSHistoryPolicy, QoSDurabilityPolicy
from nav_msgs.msg import Odometry,Path,OccupancyGrid
from autoware_auto_perception_msgs.msg import PredictedObjects,PredictedObject,ObjectClassification,DetectedObject,DetectedObjects
from rclpy.parameter import Parameter
from geometry_msgs.msg import Twist, Point
from tf2_ros import TransformException
from tf2_ros.buffer import Buffer
from tf2_ros.transform_listener import TransformListener
from builtin_interfaces.msg import Time
from sc_interfaces.msg import ObservedObjects,Object
import tf2_geometry_msgs, tf2_msgs
from geometry_msgs.msg import TransformStamped, Vector3, PoseStamped,Point32,PolygonStamped, Polygon
import copy
import time
MAX_NUM_OBJECTS = 50


def find_neighbors(x, y, binary_map):
    offsets = [(-1, -1), (-1, 0), (-1, 1),
               (0, -1),           (0, 1),
               (1, -1),  (1, 0),  (1, 1)]

    neighbors = []
    for dx, dy in offsets:
        nx, ny = x + dx, y + dy
        if (0 <= nx < len(binary_map)) and (0 <= ny < len(binary_map[0])):
            if binary_map[nx][ny] == 255:
                neighbors.append((nx, ny))
    return neighbors

def cluster_points(binary_map):
    clusters = []
    visited = set()

    for i in range(len(binary_map)):
        for j in range(len(binary_map[0])):
            if binary_map[i][j] == 255 and (i, j) not in visited:
                cluster = []
                stack = [(i, j)]
                while stack:
                    x, y = stack.pop()
                    if (x, y) not in visited:
                        cluster.append((x, y))
                        visited.add((x, y))
                        neighbors = find_neighbors(x, y, binary_map)
                        stack.extend(neighbors)
                clusters.append(cluster)
    return clusters

def calculate_centroid(cluster):
    sum_x = sum(point[0] for point in cluster)
    sum_y = sum(point[1] for point in cluster)
    centroid_x = sum_x / len(cluster)
    centroid_y = sum_y / len(cluster)
    return centroid_x, centroid_y

class grid_based_clustering(Node):
    def __init__(self):
        super().__init__('grid_based_clustering')
        simtime =Parameter("use_sim_time", Parameter.Type.BOOL, True)
        self.set_parameters([simtime]) 
        self.pub_ObservedObjects= self.create_publisher(
            DetectedObjects,"/perception/object_recognition/cost_map/clusters", 10)    
   
        
        self.create_subscription( 
            OccupancyGrid,
            "/perception/occupancy_grid_map/map",
            self.map_callback,
            10)
        


        self.object_publisher = []
      
        self.sbr_ = StaticTransformBroadcaster(self)
        self.br_ = TransformBroadcaster(self)

        buffer_time = Time()
        buffer_time.sec = 0
        self.tf_buffer = Buffer(buffer_time)
        self.tf_listener = TransformListener(self.tf_buffer, self)
        self.objects_id = [] 
        self.len = 0
    def map_callback(self, mapdata: OccupancyGrid):
        # binary_map = np.array(mapdata.data).reshape(300, 300)
        # filtered_map = np.where(binary_map < 98, 0, 255)
        # start_time = time.time()
        
        filtered_map = []
        for i in range(mapdata.info.height):
            row = []
            for j in range(mapdata.info.width):
                if mapdata.data[i*mapdata.info.width+j] < 98:
                    row.append(0)
                else:
                    row.append(255)
            filtered_map.append(row)


        clusters = cluster_points(filtered_map)
        # print("Number of clusters: ", len(clusters))
        msg = DetectedObjects()

        msg.header.frame_id = "map"
        msg.header.stamp = self.get_clock().now().to_msg()
        msg.objects = []
        for cluster in clusters:
            centroid = calculate_centroid(cluster)
            #if the centroid is with in the center of the map
            centroid_y, centroid_x = centroid
            is_in_center = (centroid_x > 0.4*mapdata.info.width) and (centroid_x < 0.6*mapdata.info.width) and (centroid_y > 0.4*mapdata.info.height) and (centroid_y < 0.6*mapdata.info.height)

            if len(cluster) < 20 and is_in_center:
                    
                pos_x = centroid_x*mapdata.info.resolution+mapdata.info.origin.position.x
                pos_y = centroid_y*mapdata.info.resolution+mapdata.info.origin.position.y



                object_ = DetectedObject()
                oc_placeholder = ObjectClassification()
                oc_placeholder.probability = 1.0
                oc_placeholder.label = 0
                object_.classification.append(oc_placeholder)
                object_.existence_probability = 0.0
                object_.kinematics.pose_with_covariance.pose.position.x = pos_x
                object_.kinematics.pose_with_covariance.pose.position.y = pos_y
                object_.kinematics.pose_with_covariance.pose.position.z = 0.2

                object_.kinematics.pose_with_covariance.pose.orientation.x = 0.0
                object_.kinematics.pose_with_covariance.pose.orientation.y = 0.0
                object_.kinematics.pose_with_covariance.pose.orientation.z = 0.0
                object_.kinematics.pose_with_covariance.pose.orientation.w = 1.0

                object_.kinematics.twist_with_covariance.twist.linear.x = -0.0
                object_.kinematics.twist_with_covariance.twist.linear.y = -0.0

                object_.kinematics.has_position_covariance = False
                object_.kinematics.has_twist_covariance = False
                object_.kinematics.has_twist = True
                object_.kinematics.orientation_availability = 0

                object_.shape.dimensions.x = 1.0
                object_.shape.dimensions.y = 1.0
                object_.shape.dimensions.z = 1.0
                object_.shape.type = 0


                msg.objects.append(object_)
        self.pub_ObservedObjects.publish(msg)
        # end_time = time.time()
        # print("Time taken for clustering: ", (end_time-start_time)*1000, "ms")


  


        # self.br_.sendTransform(object_list)

def main(args=None):
    rclpy.init(args=args)
    grid_based_clustering_= grid_based_clustering()
    rclpy.spin(grid_based_clustering_)
    #optitrack_tf_state.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()