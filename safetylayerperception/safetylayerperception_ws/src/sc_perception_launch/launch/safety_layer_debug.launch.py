# Copyright (c) 2024 HAN university of applied science

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import os
from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument, IncludeLaunchDescription, ExecuteProcess
from launch.conditions import IfCondition, UnlessCondition
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import Command, LaunchConfiguration, PythonExpression, FindExecutable, PathJoinSubstitution
from launch_ros.actions import Node
from launch_ros.substitutions import FindPackageShare
import threading
from launch_ros.substitutions import FindPackageShare
from launch_ros.parameter_descriptions import ParameterValue
from ament_index_python.packages import get_package_share_directory


def generate_launch_description():
    # thread_set_origin = threading.Thread(target=set_origin)
    # thread_set_origin.start()
    # Create the launch description and populate
    ld = LaunchDescription()
    use_sim_time = LaunchConfiguration('use_sim_time')

    declare_use_sim_time_cmd = DeclareLaunchArgument(
                                name='use_sim_time',
                                default_value='true',
                                description='Use simulation (Gazebo) clock if true')

    # Set the path to different files and folders.
    # Get URDF via xacro
    robot_description_content = Command(
        [
            PathJoinSubstitution([FindExecutable(name="xacro")]),
            " ",
            PathJoinSubstitution(
                [FindPackageShare("twizy_drone"), "urdf",
                 "sd_twizy.urdf.xacro"]
            ),
            " ",
            "name:=sd_twizy"
        ]
    )
    my_dir = FindPackageShare(package='twizy_drone').find('twizy_drone') 
    lanuch_dir_ = os.path.join(my_dir, 'launch')
    mapdir = os.path.join(my_dir, 'maps')
    
    robot_description = {"robot_description": ParameterValue(
        robot_description_content, value_type=str)}

    node_robot_state_publisher = Node(
        package="robot_state_publisher",
        executable="robot_state_publisher",
        output="screen",
        parameters=[{'use_sim_time': False}, robot_description],
    )

    safety_envelop_calculation = Node(
        package='sc_perception_launch',
        executable='safety_envelop_calculation.py',
        output='screen',
        parameters=[{'use_sim_time': use_sim_time}],
    )

    twist_to_nominal = Node(
        package='sc_perception_launch',
        executable='twist_to_nominal.py',
        output='screen',
        parameters=[{'use_sim_time': use_sim_time}],
    )

    safety_logic_visualization = Node(
        package='sc_perception_launch',
        executable='safety_logic_visualization.py',
        output='screen',
        parameters=[{'use_sim_time': use_sim_time}],
    )

    fts_interface = Node(
        package='sc_perception_launch',
        executable='fts_interface.py',
        output='screen',
        parameters=[{'use_sim_time': use_sim_time}],
    )
    object_frame_publisher = Node(
        package='sc_perception_launch',
        executable='object_frame_publisher.py',
        output='screen',
        parameters=[{'use_sim_time': use_sim_time}],
    )
    grid_based_2d_cluster = Node(
        package='sc_perception_launch',
        executable='grid_based_2d_cluster',
        output='screen',
        parameters=[{'use_sim_time': use_sim_time}],
    )
    matlab_node = Node(
        package='safeclai',
        executable='SafeCLAI',
        output='screen',
        parameters=[{'use_sim_time': use_sim_time}],
    )


    dz_logic = Node(
        package='sc_dezyne_logic',
        executable='logic',
        output='log',
        parameters=[{'use_sim_time': use_sim_time}],
    )
    dz_arbiter = Node(
        package='sc_dezyne_logic',
        executable='arbiter',
        output='log',
        parameters=[{'use_sim_time': use_sim_time}],
    )
    dz_longnotlat = Node(
        package='sc_dezyne_logic',
        executable='longnotlat',
        output='log',
        parameters=[{'use_sim_time': use_sim_time}],
    )

    # Create the launch description and populate
    # ld.add_action(node_robot_state_publisher)
    ld.add_action(declare_use_sim_time_cmd)
    ld.add_action(safety_envelop_calculation)
    ld.add_action(twist_to_nominal)
    ld.add_action(safety_logic_visualization)
    ld.add_action(fts_interface)
    ld.add_action(object_frame_publisher)
    ld.add_action(grid_based_2d_cluster)

    ld.add_action(matlab_node)
    ld.add_action(dz_logic)
    ld.add_action(dz_arbiter)
    ld.add_action(dz_longnotlat)

    return ld

# ros2 service call /datum robot_localization/srv/SetDatum '{geo_pose: {position: {latitude: 51.41588358903864, longitude: 5.876291768287732 , altitude: 13.0}, orientation: {x: 0.0, y: 0.0, z: 0.0, w: 1.0}}}'
