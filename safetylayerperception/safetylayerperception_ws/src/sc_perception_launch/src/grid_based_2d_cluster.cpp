// # Copyright (c) 2024 HAN university of applied science

// # Permission is hereby granted, free of charge, to any person obtaining
// # a copy of this software and associated documentation files (the
// # "Software"), to deal in the Software without restriction, including
// # without limitation the rights to use, copy, modify, merge, publish,
// # distribute, sublicense, and/or sell copies of the Software, and to
// # permit persons to whom the Software is furnished to do so, subject to
// # the following conditions:

// # The above copyright notice and this permission notice shall be
// # included in all copies or substantial portions of the Software.

// # THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// # EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// # MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// # NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// # LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// # OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// # WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


#include <chrono>
#include <functional>
#include <memory>
#include <string>
#include <vector>
#include <chrono> // Include chrono for time measurement
#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"
#include "sensor_msgs/msg/point_cloud2.hpp"
#include "sensor_msgs/msg/imu.hpp"
#include "nav_msgs/msg/occupancy_grid.hpp"
#include <autoware_auto_perception_msgs/msg/detected_objects.hpp>
#include <autoware_auto_perception_msgs/msg/detected_object.hpp>
#include <autoware_auto_perception_msgs/msg/object_classification.hpp>
using namespace std::chrono_literals;

using namespace std;
using namespace std::chrono; // Add chrono namespace for time measurement
vector<pair<int, int>> find_neighbors(int x, int y, vector<vector<int>>& binary_map) {
    vector<pair<int, int>> neighbors;
    vector<pair<int, int>> offsets = {{-1, -1}, {-1, 0}, {-1, 1},
                                      {0, -1},           {0, 1},
                                      {1, -1},  {1, 0},   {1, 1}};

    for (const auto& offset : offsets) {
        int nx = x + offset.first;
        int ny = y + offset.second;
        if (nx >= 0 && nx < binary_map.size() && ny >= 0 && ny < binary_map[0].size()) {
            if (binary_map[nx][ny] == 255) {
                neighbors.push_back({nx, ny});
            }
        }
    }
    return neighbors;
}

vector<vector<pair<int, int>>> cluster_points(vector<vector<int>>& binary_map) {
    vector<vector<pair<int, int>>> clusters;
    vector<pair<int, int>> visited;

    for (int i = 0; i < binary_map.size(); ++i) {
        for (int j = 0; j < binary_map[0].size(); ++j) {
            if (binary_map[i][j] == 255 && find(visited.begin(), visited.end(), make_pair(i, j)) == visited.end()) {
                vector<pair<int, int>> cluster;
                vector<pair<int, int>> stack = {{i, j}};
                while (!stack.empty()) {
                    auto [x, y] = stack.back();
                    stack.pop_back();
                    if (find(visited.begin(), visited.end(), make_pair(x, y)) == visited.end()) {
                        cluster.push_back({x, y});
                        visited.push_back({x, y});
                        auto neighbors = find_neighbors(x, y, binary_map);
                        stack.insert(stack.end(), neighbors.begin(), neighbors.end());
                    }
                }
                clusters.push_back(cluster);
            }
        }
    }
    return clusters;
}

pair<double, double> calculate_centroid(vector<pair<int, int>>& cluster) {
    double sum_x = 0, sum_y = 0;
    for (const auto& point : cluster) {
        sum_x += point.first;
        sum_y += point.second;
    }
    double centroid_x = sum_x / cluster.size();
    double centroid_y = sum_y / cluster.size();
    return {centroid_x, centroid_y};
}

class grid_based_2d_cluster : public rclcpp::Node
{
public:
  grid_based_2d_cluster()
  : Node("grid_based_2d_cluster"), count_(0)
  { 
    
    pub_objects = this->create_publisher<autoware_auto_perception_msgs::msg::DetectedObjects>("/perception/object_recognition/cost_map/clusters",  10);
    sub_grid = this->create_subscription<nav_msgs::msg::OccupancyGrid>(
      "/perception/occupancy_grid_map/map", rclcpp::SensorDataQoS(), std::bind(&grid_based_2d_cluster::pcd_callback, this, std::placeholders::_1));

  }

private:
  void pcd_callback(const nav_msgs::msg::OccupancyGrid::ConstSharedPtr msg) const
  {
    auto start = high_resolution_clock::now(); // Record the starting time

    std::vector<std::vector<int>> filtered_map;
    std::vector<int> row;
    for (int i = 0; i < msg->info.height; i++)
    {
      row.clear();
      for (int j = 0; j < msg->info.width; j++)
      {
        if (msg->data[i*msg->info.width + j] < 99)
        {
          row.push_back(0);
        }
        else
        {
          row.push_back(255);
        }
      }
      filtered_map.push_back(row);
    }
    //print the filtered map
    auto clusters = cluster_points(filtered_map);
    auto msg_to_send = autoware_auto_perception_msgs::msg::DetectedObjects();
    msg_to_send.header.frame_id = "map";
    msg_to_send.header.stamp = this->now();
    msg_to_send.objects.clear();
    for (int i = 0; i < clusters.size(); ++i) {
        auto [centroid_y, centroid_x] = calculate_centroid(clusters[i]);
        bool is_in_center = centroid_x > 0.4 * msg->info.width && centroid_x < 0.6 * msg->info.width && centroid_y > 0.4 * msg->info.height && centroid_y < 0.6 * msg->info.height;

        if (is_in_center && clusters[i].size() <20)
        {   
            auto pos_x = centroid_x * msg->info.resolution + msg->info.origin.position.x;
            auto pos_y = centroid_y * msg->info.resolution + msg->info.origin.position.y;
            autoware_auto_perception_msgs::msg::DetectedObject object;
            autoware_auto_perception_msgs::msg::ObjectClassification oc_placeholder;
            oc_placeholder.probability = 1.0;
            oc_placeholder.label = 0;
            object.classification.push_back(oc_placeholder);
            object.existence_probability = 0.0;
            object.kinematics.pose_with_covariance.pose.position.x = pos_x;
            object.kinematics.pose_with_covariance.pose.position.y = pos_y;
            object.kinematics.pose_with_covariance.pose.position.z = 0.2;

            object.kinematics.pose_with_covariance.pose.orientation.w = 1.0;

            object.kinematics.has_position_covariance = false;
            object.kinematics.has_twist_covariance = false;

            object.shape.dimensions.x = 1.0;
            object.shape.dimensions.y = 1.0;
            object.shape.dimensions.z = 1.0;
            object.shape.type = 0;

            msg_to_send.objects.push_back(object);
        }
    }
    pub_objects->publish(msg_to_send);
    auto stop = high_resolution_clock::now(); // Record the ending time
    auto duration = duration_cast<milliseconds>(stop - start); // Calculate the duration

    cout << "Execution time: " << duration.count() << " milliseconds" << endl; // Print the duration
  }

  rclcpp::Subscription<nav_msgs::msg::OccupancyGrid>::SharedPtr sub_grid;
  rclcpp::Publisher<autoware_auto_perception_msgs::msg::DetectedObjects>::SharedPtr pub_objects;

  size_t count_;
};

int main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<grid_based_2d_cluster>());
  rclcpp::shutdown();
  return 0;
}
