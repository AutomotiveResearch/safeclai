# SafetyLayerPerception

This perception system is based on autoware.universe. The Lidar pipeline is only using euclidean clustering as its method. No deep learning method was applied. A few new nodes was added to be able to integrate with the SafeCLAI framework. 
For more information please refer to the [autoware.universe](https://autowarefoundation.github.io/autoware-documentation/main/design/autoware-architecture/perception/).

## Structure
![rosgraph](reference-implementaion-perception-diagram.drawio.svg)

## Build the package
```bash
source /opt/ros/humble/setup.bash
cd safetylayerperception_ws/
rosdep install -y --from-paths src --ignore-src --rosdistro $ROS_DISTRO
colcon build --symlink-install --cmake-args -DCMAKE_BUILD_TYPE=Release
```
## Usage
The entry point of this package is `sc_perception_launch` package.
You can use the command as follows at shell script to launch `*.launch.xml` in `launch` directory.
Make sure that you have sourced the setup file for both streedrone dynamic workspace and this workspace and the Gazebo simulation environment is running.

```bash
ros2 launch sc_perception_launch content_awareness_real.xml 
ros2 launch sc_perception_launch situation_awareness.xml vehicle_model:=sd_vehicle
ros2 launch sc_perception_launch self_awareness.xml vehicle_model:=sd_vehicle sensor_model:=sample_sensor_kit 
```
