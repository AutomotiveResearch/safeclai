# SafetyLayerIntegration

This repository is mainly holding the interface. 
Some useful shell scripts are also included.

## Getting started

Make sure you already installed ros2-humble in ubuntu 22.04(recommended). 
Build instruction. 
```
colcon build
```
Make sure you are at the safeclai_intergration folder.
## Test the interface 

Make sure you are at the safeclai_intergration folder, then source the install.

```
source install/setup.bash
```

You could use RQT to see the topic interaction. 

```
rqt
```
This will open the rqt GUI, providing a graphical interface for various ROS tools and plugins.

Selecting the Plugin: Once rqt is running, you will see a menu bar at the top of the window. To visualize topics and create publishers, you'll need to select the appropriate plugins. Click on the "Plugins" menu at the top.

### Adding Topic Visualization:

To add a topic visualization, hover over the "Visualization" option in the "Plugins" menu.
From the dropdown menu that appears, select the specific visualization tool you want to use. For example, you can choose "Plot" to visualize numeric data over time.
Once selected, a new window or tab will open, displaying the selected visualization tool.
### Adding a Publisher:

To add a publisher for sending messages to a specific topic, go to the "Plugins" menu again.
Hover over the "Publisher" option in the dropdown menu.
Select the type of message you want to publish. This will open a new window where you can configure and publish messages to a ROS topic.
Configure the message fields as needed and use the interface to send messages to the selected topic.
### Viewing Published Data:

If you've added a topic visualization, you can use it to monitor the data published to a specific topic in real-time.
Select the topic you want to visualize from the list provided in the visualization tool.
The tool will display data from the selected topic, making it easy to analyze and understand the information being published.
### Interacting with Visualization and Publisher:

Depending on the specific plugin you've chosen, you can often interact with the visualization (e.g., zoom in/out, pan) and use the publisher interface to send data or commands to your robot or ROS nodes.
Saving Configurations: rqt allows you to save your current workspace layout and configurations for future use. To save your setup, you can go to the "File" menu and select "Save Perspective" or "Save Configuration" as needed.

![Example](rqt_example.png)
