&nbsp;

![LogoSafeCLAInoWhiteStroke_SubTitle.svg](Documentation/LogoSafeCLAInoWhiteStroke_SubTitle.svg)

&nbsp;


> Copyright (c) 2024 HAN university of applied science
> 
> Permission is hereby granted, free of charge, to any person obtaining a copy
> of this software and associated documentation files (the "Software"), to deal
> in the Software without restriction, including without limitation the rights
> to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
> copies of the Software, and to permit persons to whom the Software is
> furnished to do so, subject to the following conditions:
> 
> The above copyright notice and this permission notice shall be included in all
> copies or substantial portions of the Software.
> 
> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
> IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
> FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
> AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
> LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
> OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
> SOFTWARE.
## Introduction
This documentation and related code is the result of the SafeCLAI project, a [SIA-granted](https://regieorgaan-sia.nl/) RAAK-mkb project aiming for a re-usable safety concept for low-speed autonomous vehicles. The resulting safety concept proposes a safety layer between the functional (or 'performance') control layer and an assumed safe low-level x-by-wire control layer controlling lateral (steering) and longitudinal (accelerating, braking) vehicle motion, based on setpoints communicated by the functional control layer.

More information can be found at the [wiki](https://gitlab.com/AutomotiveResearch/safeclai/-/wikis/home). 

