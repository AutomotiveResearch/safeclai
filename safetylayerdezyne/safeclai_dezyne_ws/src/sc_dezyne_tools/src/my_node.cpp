#include "rclcpp/rclcpp.hpp"

/*
#include "isafety_module/srv/activate.hpp"
#include "isafety_module/srv/deactivate.hpp"
#include "isafety_module/srv/disable.hpp"
#include "isafety_module/srv/enable.hpp"
#include "isafety_module/srv/moving.hpp"
#include "isafety_module/srv/stop.hpp"
#include "isafety_module/srv/stopped.hpp"
#include "isafety_module/srv/inside.hpp"
#include "isafety_module/srv/outside.hpp"
*/

#include "sc_interfaces/msg/brake_override.hpp"
#include "sc_interfaces/msg/ignition_signal.hpp"
#include "sc_interfaces/msg/is_vechicle_moving.hpp"
#include "sc_interfaces/msg/nominal_power_supply_status.hpp"
#include "sc_interfaces/msg/nominal_acceleration_request.hpp"
#include "sc_interfaces/msg/nominal_steering_request.hpp"
#include "sc_interfaces/msg/safety_trigger.hpp"
#include "sc_interfaces/msg/safety_envelope_trigger.hpp"
#include "sc_interfaces/msg/system_quality.hpp"
#include "sc_interfaces/msg/safety_logic_state.hpp"
#include "sc_interfaces/msg/safety_logic_state.hpp"
#include "sc_interfaces/msg/safety_deceleration_request.hpp"
#include "sc_interfaces/msg/safety_steering_request.hpp"

#include "sc_interfaces/srv/hmi_activation_request.hpp"
#include "sc_interfaces/srv/communication_activation_request.hpp"

#include <iomanip>

using namespace std::chrono_literals;

rclcpp::Publisher<sc_interfaces::msg::IgnitionSignal>::SharedPtr                publisher_is;
rclcpp::Publisher<sc_interfaces::msg::BrakeOverride>::SharedPtr                 publisher_bo;
rclcpp::Publisher<sc_interfaces::msg::SafetyLogicState>::SharedPtr              publisher_sls;
rclcpp::Publisher<sc_interfaces::msg::SafetyDecelerationRequest>::SharedPtr     publisher_sdr;
rclcpp::Publisher<sc_interfaces::msg::SafetySteeringRequest>::SharedPtr         publisher_ssr;
rclcpp::Publisher<sc_interfaces::msg::NominalAccelerationRequest>::SharedPtr    publisher_nar;
rclcpp::Publisher<sc_interfaces::msg::NominalSteeringRequest>::SharedPtr        publisher_nsr;
rclcpp::Publisher<sc_interfaces::msg::NominalPowerSupplyStatus>::SharedPtr      publisher_npss;
rclcpp::Publisher<sc_interfaces::msg::SystemQuality>::SharedPtr                 publisher_sq;
rclcpp::Publisher<sc_interfaces::msg::SafetyTrigger>::SharedPtr                 publisher_st;
rclcpp::Publisher<sc_interfaces::msg::SafetyEnvelopeTrigger>::SharedPtr         publisher_set;
rclcpp::Publisher<sc_interfaces::msg::IsVechicleMoving>::SharedPtr              publisher_vm;

rclcpp::Client<sc_interfaces::srv::HMIActivationRequest>::SharedPtr             client_har;
rclcpp::Client<sc_interfaces::srv::CommunicationActivationRequest>::SharedPtr   client_car;

void hmi_act_req(int flag)
{
    auto request = std::make_shared<sc_interfaces::srv::HMIActivationRequest::Request>();
    request->data = flag == 0 ? false : true;
    client_har->async_send_request(request);
}

void com_act_req(int flag)
{
    auto request = std::make_shared<sc_interfaces::srv::CommunicationActivationRequest::Request>();
    request->data = flag == 0 ? false : true;
    client_car->async_send_request(request);
}

void brake_ov(int override)
{
    auto message = sc_interfaces::msg::BrakeOverride();
    message.data = override == 0 ? false : true;
    std::cout <<  "bo: '" << message.data << "'\n";
    publisher_bo->publish(message);
}
 
void ign_sig(int flag)
{
    auto message = sc_interfaces::msg::IgnitionSignal();
    message.data = flag == 0 ? false : true;
    std::cout <<  "is: '" << message.data << "'\n";
    publisher_is->publish(message);
}
 
void sys_qua(int flag)
{
    auto message = sc_interfaces::msg::SystemQuality();
    message.quality = flag == 0 ? false : true;
    std::cout <<  "is: '" << message.quality << "'\n";
    publisher_sq->publish(message);
}
 
void saf_tri(int flag)
{
    auto message = sc_interfaces::msg::SafetyTrigger();
    message.trigger = flag == 0 ? false : true;
    std::cout <<  "is: '" << message.trigger << "'\n";
    publisher_st->publish(message);
}
 
void saf_env_tri(int flag)
{
    auto message = sc_interfaces::msg::SafetyEnvelopeTrigger();
    message.trigger = flag == 0 ? false : true;
    std::cout <<  "is: '" << message.trigger << "'\n";
    publisher_set->publish(message);
}
 
void veh_mov(int flag)
{
    auto message = sc_interfaces::msg::IsVechicleMoving();
    message.data = flag == 0 ? false : true;
    std::cout <<  "is: '" << message.data << "'\n";
    publisher_vm->publish(message);
}
 
void saf_log_sta(int major, int minor)
{
    auto message = sc_interfaces::msg::SafetyLogicState();
    message.state_major = major;
    message.state_minor = minor;
    std::cout <<  "sls: '" << message.state_major << "," << message.state_minor << "'\n";
    publisher_sls->publish(message);
}
 
void saf_dec_req(float request)
{
    auto message = sc_interfaces::msg::SafetyDecelerationRequest();
    message.request = request;
    std::cout <<  "sdr: '" << message.request << "'\n";
    publisher_sdr->publish(message);
}
 
void saf_ste_req(int frozen)
{
    auto message = sc_interfaces::msg::SafetySteeringRequest();
    message.frozen = frozen == 0 ? false : true;
    std::cout <<  "ssr: '" << message.frozen << "'\n";
    publisher_ssr->publish(message);
}
 
void nom_acc_req(float request)
{
    auto message = sc_interfaces::msg::NominalAccelerationRequest();
    message.request = request;
    std::cout <<  "nar: '" << message.request << "'\n";
    publisher_nar->publish(message);
}
 
void nom_ste_req(float request)
{
    auto message = sc_interfaces::msg::NominalSteeringRequest();
    message.request = request;
    std::cout <<  "nsr: '" << message.request << "'\n";
    publisher_nsr->publish(message);
}
 
void nom_pow_sup_sta(int data)
{
    auto message = sc_interfaces::msg::NominalPowerSupplyStatus();
    message.data = data == 0 ? false : true;
    std::cout <<  "npss: '" << message.data << "'\n";
    publisher_npss->publish(message);
}
 
int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);

    std::shared_ptr<rclcpp::Node> node = rclcpp::Node::make_shared("logictest");
    publisher_is  = node->create_publisher<sc_interfaces::msg::IgnitionSignal>("sc_topic_ignition_signal", 10);
    publisher_bo  = node->create_publisher<sc_interfaces::msg::BrakeOverride>("sc_topic_brake_override", 10);
    publisher_sls = node->create_publisher<sc_interfaces::msg::SafetyLogicState>("sc_topic_safety_logic_state", 10);
    publisher_sdr = node->create_publisher<sc_interfaces::msg::SafetyDecelerationRequest>("sc_topic_safety_deceleration_request", 10);
    publisher_ssr = node->create_publisher<sc_interfaces::msg::SafetySteeringRequest>("sc_topic_safety_steering_request", 10);
    publisher_nar = node->create_publisher<sc_interfaces::msg::NominalAccelerationRequest>("sc_topic_nominal_acceleration_request", 10);
    publisher_nsr = node->create_publisher<sc_interfaces::msg::NominalSteeringRequest>("sc_topic_nominal_steering_request", 10);
    publisher_npss = node->create_publisher<sc_interfaces::msg::NominalPowerSupplyStatus>("sc_topic_nominal_power_supply_status", 10);
    publisher_sq  = node->create_publisher<sc_interfaces::msg::SystemQuality>("sc_topic_system_quality", 10);
    publisher_st  = node->create_publisher<sc_interfaces::msg::SafetyTrigger>("sc_topic_safety_trigger", 10);
    publisher_set = node->create_publisher<sc_interfaces::msg::SafetyEnvelopeTrigger>("sc_topic_safety_envelope_trigger", 10);
    publisher_vm  = node->create_publisher<sc_interfaces::msg::IsVechicleMoving>("sc_topic_vehicle_moving", 10);
    
    client_har    = node->create_client<sc_interfaces::srv::HMIActivationRequest>("sc_service_hmi_activation_request");
    client_car    = node->create_client<sc_interfaces::srv::CommunicationActivationRequest>("sc_service_communication_activation_request");

    char command;
    float fl;
    int i, i0;
    char buf [81];
    std::cout << "Enter command: (? = help)" << std::endl;
    while (fgets(buf, 80, stdin) != NULL)
    {
        command = buf[0];
        //std::cout << "'" << buf << "'" << std::endl;
        switch (buf[0])
        {
            case 'h':
                sscanf(&buf[1], "%d", &i);
                hmi_act_req(i);
                break;
            case 'c':
                sscanf(&buf[1], "%d", &i);
                com_act_req(i);
                break;
            case 'i':
                sscanf(&buf[1], "%d", &i);
                ign_sig(i);
                break;
            case 's':
                sscanf(&buf[1], "%d", &i);
                sys_qua(i);
                break;
            case 'e':
                sscanf(&buf[1], "%d", &i);
                saf_env_tri(i);
                break;
            case 't':
                sscanf(&buf[1], "%d", &i);
                saf_tri(i);
                break;
            case 'v':
                sscanf(&buf[1], "%d", &i);
                veh_mov(i);
                break;
            case 'L':
                sscanf(&buf[1], "%d %d", &i, &i0);
                saf_log_sta(i, i0);
                break;
            case 'B':
                sscanf(&buf[1], "%d", &i);
                brake_ov(i);
                break;
            case 'D':
                sscanf(&buf[1], "%f", &fl);
                //std::cout << "D '" << fl << "'" << std::endl;
                saf_dec_req(fl);
                break;
            case 'S':
                sscanf(&buf[1], "%d", &i);
                //std::cout << "S '" << i << "'" << std::endl;
                saf_ste_req(i);
                break;
            case 'A':
                sscanf(&buf[1], "%f", &fl);
                //std::cout << "A '" << i << "'" << std::endl;
                nom_acc_req(fl);
                break;
            case 'N':
                sscanf(&buf[1], "%f", &fl);
                nom_ste_req(fl);
                break;
            case 'P':
                sscanf(&buf[1], "%d", &i);
                nom_pow_sup_sta(i);
                break;
            case '?':
                std::clog << "tester for logic:\n"
                          << "h 0|1          HMI activation request\n"
                          << "c 0|1          communication activation request\n"
                          << "i 0|1          ignition signal\n"
                          << "s 0|1          system quality\n"
                          << "e 0|1          safety envelope trigger\n"
                          << "t 0|1          safety trigger\n"
                          << "v 0|1          vehicle is moving\n"
                          << "\n"
                          << "L <int> <int>  safety logic state\n"
                          << "B 0|1          brake override\n"
                          << "D <float>      safety deceleration request\n"
                          << "S 0|1          safety steering request\n"
                          << "A <float>      nominal acceleration request\n"
                          << "N <float>      nominal steering request\n"
                          << "P 0|1          nominal power supply status\n"
                          << "?              this text\n" 
                          << "q              quit\n" 
                          << std::endl;
            case 'q':
                break;
            default:
                std::cout << "unknown command: '" << buf << "'\n";
        }
        if (command == 'q')
        {
            break;
        }
    }
    std::cout << "quit...'" << std::endl;
    rclcpp::shutdown();
    return 0;
  
  /*
  rclcpp::Client<isafety_module::srv::Activate>::SharedPtr activate_client =
    node->create_client<isafety_module::srv::Activate>("activate");
  rclcpp::Client<isafety_module::srv::Deactivate>::SharedPtr deactivate_client =
    node->create_client<isafety_module::srv::Deactivate>("deactivate");
  rclcpp::Client<isafety_module::srv::Moving>::SharedPtr moving_client =
    node->create_client<isafety_module::srv::Moving>("moving");
  rclcpp::Client<isafety_module::srv::Stopped>::SharedPtr stopped_client =
    node->create_client<isafety_module::srv::Stopped>("stopped");
  rclcpp::Client<isafety_module::srv::Inside>::SharedPtr inside_client =
    node->create_client<isafety_module::srv::Inside>("inside");
  rclcpp::Client<isafety_module::srv::Outside>::SharedPtr outside_client =
    node->create_client<isafety_module::srv::Outside>("outside");

  auto activate_request = std::make_shared<isafety_module::srv::Activate::Request>();
  auto deactivate_request = std::make_shared<isafety_module::srv::Deactivate::Request>();
  auto moving_request = std::make_shared<isafety_module::srv::Moving::Request>();
  auto stopped_request = std::make_shared<isafety_module::srv::Stopped::Request>();
  auto inside_request = std::make_shared<isafety_module::srv::Inside::Request>();
  auto outside_request = std::make_shared<isafety_module::srv::Outside::Request>();

  rclcpp::Service<isafety_module::srv::Disable>::SharedPtr disable_service =
    node->create_service<isafety_module::srv::Disable>("disable", &disable);
  rclcpp::Service<isafety_module::srv::Enable>::SharedPtr enable_service =
    node->create_service<isafety_module::srv::Enable>("enable", &enable);

  std::cin >> std::skipws;
  while(std::cin.get(command))
  {
    // process 10 incoming events
    for (size_t i = 0; i < 10; ++i) rclcpp::spin_some(node);

    switch(command)
    {
    case 'a':
      activate_client->async_send_request(activate_request);
      break;
    case 'd':
      deactivate_client->async_send_request(deactivate_request);
      break;
    case 'm':
      moving_client->async_send_request(moving_request);
      break;
    case 's':
      stopped_client->async_send_request(stopped_request);
      break;
    case 'i':
      inside_client->async_send_request(inside_request);
      break;
    case 'o':
      outside_client->async_send_request(outside_request);
      break;
    case 'h':
      std::clog << "help:\n"
                << "a => activate\n"
                << "d => deactivate\n"
                << "m => moving\n"
                << "s => stopped\n"
                << "i => inside\n"
                << "o => outside" << std::endl;
      break;
    default:
      break;
    }
  }

  rclcpp::shutdown();
  return 0;
  */
}
