namespace cc
{
    interface ibrake_override
    {
        in void brake_override_on();
        in void brake_override_off();
        behaviour
        {
            on brake_override_on, brake_override_off: {}
        }
    }
    interface isafety_logic_state
    {
        in void safety_logic_state_ss0();
        in void safety_logic_state_ss1_1();
        in void safety_logic_state_ss1_2();
        in void safety_logic_state_ss2_1();
        in void safety_logic_state_ss2_2();
        in void safety_logic_state_ss3_1();
        in void safety_logic_state_ss3_2();
        behaviour
        {
            on safety_logic_state_ss0, safety_logic_state_ss1_1, safety_logic_state_ss1_2, 
                safety_logic_state_ss2_1, safety_logic_state_ss2_2, 
                safety_logic_state_ss3_1, safety_logic_state_ss3_2:
                {}
        }
    }
    interface iexternal
    {
        in void notify_set();
        in void notify_remove();
        in void safety_deceleration_request();
        in void safety_deceleration_request_cancel();
        in void safety_steering_request();
        in void safety_steering_request_cancel();
        behaviour
        {
            on notify_set, notify_remove, 
                safety_deceleration_request, safety_deceleration_request_cancel, 
                safety_steering_request, safety_steering_request_cancel: {}
        }
    }

    component external_adapter
    {
        provides iexternal iext_in;
        requires iexternal iext_out;

        behaviour
        {
            bool notify = false;
            bool deceleration = false;
            bool steering = false;

            // flank detection
            on iext_in.notify_set(): 
            { 
                if (! notify)
                {
                    notify = true;
                    iext_out.notify_set(); 
                }
            }
            on iext_in.notify_remove():
            { 
                if (notify)
                {
                    notify = false;
                    iext_out.notify_remove(); 
                }
            }
            on iext_in.safety_deceleration_request(): 
            { 
                if (! deceleration)
                {
                    deceleration = true;
                    iext_out.safety_deceleration_request(); 
                }
            }
            on iext_in.safety_deceleration_request_cancel(): 
            { 
                if (deceleration)
                {
                    deceleration = false;
                    iext_out.safety_deceleration_request_cancel(); 
                }
            }
            on iext_in.safety_steering_request(): 
            { 
                if (! steering)
                {
                    steering = true;
                    iext_out.safety_steering_request(); 
                }
            }
            on iext_in.safety_steering_request_cancel(): 
            { 
                if (steering)
                {
                    steering = false;
                    iext_out.safety_steering_request_cancel(); 
                }
            }
        }
    }


    component longnotlat_control
    {
        provides ibrake_override bo;
        provides isafety_logic_state sls;
        requires iexternal ext;

        behaviour
        {
            bool override = false;
            enum SS {SS0, SS1_1, SS1_2, SS2_1, SS2_2, SS3_1, SS3_2};
            SS ss = SS.SS0;

            on bo.brake_override_on(): 
            { 
                override = true;
                ext.safety_deceleration_request();
            }
            on bo.brake_override_off(): 
            {
                override = false; 
                //if (override == true || maj == 2) 
                //else 
                if (ss == SS.SS0 || 
                    ss == SS.SS1_1 || ss == SS.SS1_2 || 
                    ss == SS.SS3_1 || ss == SS.SS3_2)
                { 
                    ext.safety_deceleration_request_cancel();
                }
            }
            on sls.safety_logic_state_ss0():
            { 
                ss = SS.SS0;
                if (! override)
                {
                    ext.safety_deceleration_request_cancel();
                }
                ext.safety_steering_request_cancel();
                ext.notify_remove();
            }
            on sls.safety_logic_state_ss1_1():
            { 
                ss = SS.SS1_1;
                if (! override)
                {
                    ext.safety_deceleration_request_cancel();
                }
                ext.safety_steering_request_cancel();
                ext.notify_set();
            }
            on sls.safety_logic_state_ss1_2():
            { 
                ss = SS.SS1_2;
                if (! override)
                {
                    ext.safety_deceleration_request_cancel();
                }
                ext.safety_steering_request_cancel();
                ext.notify_remove();
            }
            on sls.safety_logic_state_ss2_1():
            { 
                ss = SS.SS2_1;
                ext.safety_deceleration_request(); 
                ext.safety_steering_request_cancel();
                ext.notify_set();
            }
            on sls.safety_logic_state_ss2_2():
            { 
                ss = SS.SS2_2;
                ext.safety_deceleration_request(); 
                ext.safety_steering_request();
                ext.notify_set();
            }
            on sls.safety_logic_state_ss3_1():
            { 
                ss = SS.SS3_1;
                if (! override)
                {
                    ext.safety_deceleration_request_cancel();
                }
                ext.safety_steering_request_cancel();
                ext.notify_remove();
            }
            on sls.safety_logic_state_ss3_2():
            { 
                ss = SS.SS3_2;
                if (! override)
                {
                    ext.safety_deceleration_request_cancel();
                }
                ext.safety_steering_request_cancel();
                ext.notify_remove();
            }
                //if (override == true) 
                //    ext.safety_deceleration_request($100$); 
                //else 
                //    ext.safety_deceleration_request($0$);

                //ext.safety_steering_request($false$);
                
                //if (override == true || maj == 2) 
                //if ((maj == 1 && min == 1) ||
                //    (maj == 2 && min == 1) ||
                //    (maj == 2 && min == 1))
                //ext.notify_set();
                //ext.notify_remove();
//            }
        }
    }
    component external_comm
    {
        provides iexternal ext_out; 
    }
    component longnotlat
    {
        provides ibrake_override bo;
        provides isafety_logic_state sls;
        requires iexternal ext;

        system
        {
            longnotlat_control lnl;
            external_adapter ea;
            //external_comm ec;

            bo <=> lnl.bo;
            sls <=> lnl.sls;
            lnl.ext <=> ea.iext_in;
            ea.iext_out <=> ext;

        }
    }
}
