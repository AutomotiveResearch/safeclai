// Copyright 2016 Open Source Robotics Foundation, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <functional>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"
#include "sc_interfaces/msg/safety_deceleration_request.hpp"
#include "sc_interfaces/msg/safety_steering_request.hpp"
#include "sc_interfaces/msg/nominal_power_supply_status.hpp"
#include "sc_interfaces/msg/nominal_acceleration_request.hpp"
#include "sc_interfaces/msg/nominal_steering_request.hpp"
#include "geometry_msgs/msg/twist.hpp"
#include <iostream>
#include <string>


using std::placeholders::_1;

class ArbiterSubscriber : public rclcpp::Node
{
public:
    ArbiterSubscriber() : Node("arbiter")
    {
        subscription_sdr = this->create_subscription<sc_interfaces::msg::SafetyDecelerationRequest>(
            "sc_topic_safety_deceleration_request", 10, std::bind(&ArbiterSubscriber::topic_callback_sdr, this, _1));
        subscription_ssr = this->create_subscription<sc_interfaces::msg::SafetySteeringRequest>(
            "sc_topic_safety_steering_request", 10, std::bind(&ArbiterSubscriber::topic_callback_ssr, this, _1));
        subscription_nar = this->create_subscription<sc_interfaces::msg::NominalAccelerationRequest>(
            "sc_topic_nominal_acceleration_request", 10, std::bind(&ArbiterSubscriber::topic_callback_nar, this, _1));
        subscription_nsr = this->create_subscription<sc_interfaces::msg::NominalSteeringRequest>(
            "sc_topic_nominal_steering_request", 10, std::bind(&ArbiterSubscriber::topic_callback_nsr, this, _1));
        subscription_npss = this->create_subscription<sc_interfaces::msg::NominalPowerSupplyStatus>(
            "sc_topic_nominal_power_supply_status", 10, std::bind(&ArbiterSubscriber::topic_callback_npss, this, _1));
        publisher_twist = this->create_publisher<geometry_msgs::msg::Twist>("cmd_vel", 10);


    }

private:
    float   curr_sdr = 0;
    bool    curr_ssr = false;     
    float   curr_nar = 0;
    float   curr_nsr = 0;
    bool    curr_npss = false;
    
    void deceleration_request(float request) 
    {
        // TODO
        //auto message = sc_interfaces::msg::DecelerationRequest();
        //message.request = request;
        message_twist.linear.x = request;
        RCLCPP_INFO(this->get_logger(), "arbiter sends deceleration-request: '%f' (to be implemented)", request);
        publisher_twist->publish(message_twist);
        //publisher_dr->publish(message);
    }

    void steering_request(float request) 
    {
        // TODO
        //auto message = sc_interfaces::msg::SteeringRequest();
        message_twist.angular.z = request;
        RCLCPP_INFO(this->get_logger(), "arbiter sends steering-request: '%f' (to be implemented)", request);
        //publisher_sr->publish(message);
    }

    void deceleraton_logic(void) 
    {
        float request = 0.;
        if (curr_sdr > 0.0)
        {
            // // note the negative acceleration request as a deceleration request
            // if (curr_sdr > -curr_nar)
            // {
            //     request = curr_sdr;
            // }
            // else
            // {
                request = curr_nar - curr_sdr; // acceleration request minus deceleration request is the final deceleration request
            // }
            // if the deceleration request is negative, set it to zero == break to the speed controller
            if (request < 0)
            {
                request = 0;
            }
        }
        else
        {
            request = curr_nar;
        }
        deceleration_request(request);
    }
    
    void steering_logic(void) 
    {
        static float   curr_sr_frozen = 0;
        
        if (! curr_npss || curr_ssr == true)
        {
            steering_request(curr_sr_frozen);
        }
        else
        {
            curr_sr_frozen = curr_nsr;
            steering_request(curr_nsr);
            // store the nominal steering request, to be used when low-power, or safety-frozen
        }
    }

    void topic_callback_sdr(const sc_interfaces::msg::SafetyDecelerationRequest & msg) 
    {
        std::cout << std::endl;
        RCLCPP_INFO(this->get_logger(), "arbiter receives sdr: '%lf'", msg.request);
        curr_sdr = msg.request;
        deceleraton_logic();
    }

    void topic_callback_ssr(const sc_interfaces::msg::SafetySteeringRequest & msg) 
    {
        std::cout << std::endl;
        RCLCPP_INFO(this->get_logger(), "arbiter receives ssr: '%d'", msg.frozen);
        curr_ssr = msg.frozen;
        deceleraton_logic();
    }

    void topic_callback_nar(const sc_interfaces::msg::NominalAccelerationRequest & msg) 
    {
        std::cout << std::endl;
        RCLCPP_INFO(this->get_logger(), "arbiter receives nar: '%lf'", msg.request);
        curr_nar = msg.request;
        deceleraton_logic();
    }

    void topic_callback_nsr(const sc_interfaces::msg::NominalSteeringRequest & msg) 
    {
        std::cout << std::endl;
        RCLCPP_INFO(this->get_logger(), "arbiter receives nsr: '%lf'", msg.request);
        curr_nsr = msg.request;
        steering_logic();
    }

    void topic_callback_npss(const sc_interfaces::msg::NominalPowerSupplyStatus & msg) 
    {
        std::cout << std::endl;
        RCLCPP_INFO(this->get_logger(), "arbiter receives npss: '%d'", msg.data);
        curr_npss = msg.data;
        steering_logic();
    }
    
    rclcpp::Subscription<sc_interfaces::msg::SafetyDecelerationRequest>::SharedPtr  subscription_sdr;
    rclcpp::Subscription<sc_interfaces::msg::SafetySteeringRequest>::SharedPtr      subscription_ssr;
    rclcpp::Subscription<sc_interfaces::msg::NominalAccelerationRequest>::SharedPtr subscription_nar;
    rclcpp::Subscription<sc_interfaces::msg::NominalSteeringRequest>::SharedPtr     subscription_nsr;
    rclcpp::Subscription<sc_interfaces::msg::NominalPowerSupplyStatus>::SharedPtr   subscription_npss;
    rclcpp::Publisher<geometry_msgs::msg::Twist>::SharedPtr           publisher_twist;
    geometry_msgs::msg::Twist                                         message_twist;
};

int main(int argc, char * argv[])
{
    std::cout << "Arbiter" << std::endl;
    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<ArbiterSubscriber>());
    rclcpp::shutdown();
    return 0;
}
