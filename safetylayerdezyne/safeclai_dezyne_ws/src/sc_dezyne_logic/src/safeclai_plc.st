PROGRAM TEST
  VAR
    (* input *)
    hmi      AT %IX0 : BOOL; (* local and remote *)

    (* vehicle *)
    ignition AT %IX1 : BOOL;
    velocity AT %IX2 : BOOL;
    power    AT %IX3 : BOOL;

    (* safety *)
    quality  AT %IX4 : BOOL;
    trigger  AT %IX5 : BOOL;
    envelope AT %IX6 : BOOL;

    (* output *)
    state_major AT %QW0 : INT := 0;
    state_minor AT %QW1 : INT := 0;
  END_VAR


  INITIAL_STEP SS0:
    STEP_SS0(N);
  END_STEP

  STEP SS1_1:
    STEP_SS1_1(N);
  END_STEP

  STEP SS1_2:
    STEP_SS1_2(N);
  END_STEP

  STEP SS2_1:
    STEP_SS2_1(N);
  END_STEP

  STEP SS2_2:
    STEP_SS2_2(N);
  END_STEP

  STEP SS3_1:
    STEP_SS3_1(N);
  END_STEP

  STEP SS3_2:
    STEP_SS3_2(N);
  END_STEP


  TRANSITION FROM SS0 TO SS1_1 (* T1 *)
    := hmi AND ignition;
  END_TRANSITION

  TRANSITION FROM SS0 TO SS3_1 (* T14 *)
    := NOT hmi AND ignition;
  END_TRANSITION


  TRANSITION FROM SS1_1 TO SS0 (* T2 *)
    := NOT ignition;
  END_TRANSITION

  TRANSITION FROM SS1_1 TO SS1_2 (* T3 *)
    := velocity;
  END_TRANSITION

  TRANSITION FROM SS1_1 TO SS3_1 (* T9 *)
    := NOT hmi;
  END_TRANSITION


  TRANSITION FROM SS1_2 TO SS1_1 (* T4 *)
    := NOT velocity;
  END_TRANSITION

  TRANSITION FROM SS1_2 TO SS2_2 (* T5 *)
    := NOT power OR NOT quality OR trigger OR envelope;
  END_TRANSITION


  TRANSITION FROM SS2_2 TO SS2_1 (* T6 *)
    := NOT velocity;
  END_TRANSITION


  TRANSITION FROM SS2_1 TO SS1_1 (* T7 *)
    := hmi;
  END_TRANSITION

  TRANSITION FROM SS2_1 TO SS3_1 (* T8 *)
    := NOT hmi;
  END_TRANSITION


  TRANSITION FROM SS3_1 TO SS1_1 (* T10 *)
    := hmi;
  END_TRANSITION

  TRANSITION FROM SS3_1 TO SS3_2 (* T11 *)
    := velocity;
  END_TRANSITION

  TRANSITION FROM SS3_1 TO SS0 (* T13 *)
    := NOT ignition;
  END_TRANSITION


  TRANSITION FROM SS3_2 TO SS3_1 (* T12 *)
    := NOT velocity;
  END_TRANSITION


  ACTION STEP_SS0:
    state_major := 0;
    state_minor := 0;
  END_ACTION

  ACTION STEP_SS1_1:
    state_major := 1;
    state_minor := 1;
  END_ACTION

  ACTION STEP_SS1_2:
    state_major := 1;
    state_minor := 2;
  END_ACTION

  ACTION STEP_SS2_1:
    state_major := 2;
    state_minor := 1;
  END_ACTION

  ACTION STEP_SS2_2:
    state_major := 2;
    state_minor := 2;
  END_ACTION

  ACTION STEP_SS3_1:
    state_major := 3;
    state_minor := 1;
  END_ACTION

  ACTION STEP_SS3_2:
    state_major := 3;
    state_minor := 2;
  END_ACTION

END_PROGRAM

CONFIGURATION STD_CONF
  RESOURCE STD_RESOURCE ON FOO
    TASK STD_TASK(INTERVAL := t#100ms, PRIORITY := 0);
    PROGRAM MAIN_INSTANCE : TEST;
  END_RESOURCE
END_CONFIGURATION
