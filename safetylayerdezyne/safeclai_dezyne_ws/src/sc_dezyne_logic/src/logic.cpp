#include "safeclai.hh"

#include "rclcpp/rclcpp.hpp"

#include "sc_interfaces/msg/safety_logic_state.hpp"
#include "sc_interfaces/msg/ignition_signal.hpp"
#include "sc_interfaces/msg/is_vechicle_moving.hpp"
#include "sc_interfaces/msg/nominal_power_supply_status.hpp"
#include "sc_interfaces/msg/system_quality.hpp"
#include "sc_interfaces/msg/safety_trigger.hpp"
#include "sc_interfaces/msg/safety_envelope_trigger.hpp"

#include "sc_interfaces/srv/hmi_activation_request.hpp"
#include "sc_interfaces/srv/communication_activation_request.hpp"

#include <dzn/locator.hh>
#include <dzn/runtime.hh>

class SafetyLogic : public rclcpp::Node
{
  dzn::locator locator;
  dzn::runtime runtime;
  cc::safeclai safeclai;
public:
    SafetyLogic ()
      : Node ("logic")
      , locator ()
      , runtime ()
      , safeclai (locator.set (runtime))
    {
        // T* events are no-ops,
        // saf_log_sta will communicate the safety logic state,
        // but it must be called after the T* actions return,
        // only then the safety logic state has been updated
        safeclai.logic.in.T1 = []{};
        safeclai.logic.in.T2 = []{};
        safeclai.logic.in.T3 = []{};
        safeclai.logic.in.T4 = []{};
        safeclai.logic.in.T5 = []{};
        safeclai.logic.in.T6 = []{};
        safeclai.logic.in.T7 = []{};
        safeclai.logic.in.T8 = []{};
        safeclai.logic.in.T9 = []{};
        safeclai.logic.in.T10 = []{};
        safeclai.logic.in.T11 = []{};
        safeclai.logic.in.T12 = []{};
        safeclai.logic.in.T13 = []{};
        safeclai.logic.in.T14 = []{};

        using std::placeholders::_1;
        using std::placeholders::_2;

        subscription_is = this->create_subscription<sc_interfaces::msg::IgnitionSignal>(
            "sc_topic_ignition_signal", 10, std::bind(&SafetyLogic::callback_is, this, _1));
        subscription_vm = this->create_subscription<sc_interfaces::msg::IsVechicleMoving>(
            "sc_topic_vehicle_moving", 10, std::bind(&SafetyLogic::callback_vm, this, _1));
        subscription_npss = this->create_subscription<sc_interfaces::msg::NominalPowerSupplyStatus>(
            "sc_topic_nominal_power_supply_status", 10, std::bind(&SafetyLogic::callback_npss, this, _1));
        subscription_sq = this->create_subscription<sc_interfaces::msg::SystemQuality>(
            "sc_topic_system_quality", 10, std::bind(&SafetyLogic::callback_sq, this, _1));
        subscription_st = this->create_subscription<sc_interfaces::msg::SafetyTrigger>(
            "sc_topic_safety_trigger", 10, std::bind(&SafetyLogic::callback_st, this, _1));
        subscription_set = this->create_subscription<sc_interfaces::msg::SafetyEnvelopeTrigger>(
            "sc_topic_safety_envelope_trigger", 10, std::bind(&SafetyLogic::callback_set, this, _1));
            
        publisher_sls = this->create_publisher<sc_interfaces::msg::SafetyLogicState>("sc_topic_safety_logic_state", 10);

        service_har = this->create_service<sc_interfaces::srv::HMIActivationRequest>(
            "sc_service_hmi_activation_request", std::bind(&SafetyLogic::callback_har, this, _1, _2));
        service_car = this->create_service<sc_interfaces::srv::CommunicationActivationRequest>(
            "sc_service_communication_activation_request", std::bind(&SafetyLogic::callback_car, this, _1, _2));
    }

private:
    rclcpp::Subscription<sc_interfaces::msg::IsVechicleMoving>::SharedPtr           subscription_vm;
    rclcpp::Subscription<sc_interfaces::msg::IgnitionSignal>::SharedPtr             subscription_is;
    rclcpp::Subscription<sc_interfaces::msg::NominalPowerSupplyStatus>::SharedPtr   subscription_npss;
    rclcpp::Subscription<sc_interfaces::msg::SystemQuality>::SharedPtr              subscription_sq;
    rclcpp::Subscription<sc_interfaces::msg::SafetyTrigger>::SharedPtr              subscription_st;
    rclcpp::Subscription<sc_interfaces::msg::SafetyEnvelopeTrigger>::SharedPtr      subscription_set;

    rclcpp::Service<sc_interfaces::srv::HMIActivationRequest>::SharedPtr            service_har;
    rclcpp::Service<sc_interfaces::srv::CommunicationActivationRequest>::SharedPtr  service_car;
    
    rclcpp::Publisher<sc_interfaces::msg::SafetyLogicState>::SharedPtr              publisher_sls;

    void callback_is(const sc_interfaces::msg::IgnitionSignal & msg) 
    {
        RCLCPP_INFO(this->get_logger(), "receive Ignition Request: '%d'", msg.data);

        if (msg.data) safeclai.vehicle.in.ignition_on ();
        else safeclai.vehicle.in.ignition_off ();

        saf_log_sta();
    }

    void callback_vm(const sc_interfaces::msg::IsVechicleMoving & msg) 
    {
        RCLCPP_INFO(this->get_logger(), "receive Ignition Request: '%d'", msg.data);
        
        if (msg.data) safeclai.vehicle.in.velocity_nonzero ();
        else safeclai.vehicle.in.velocity_zero ();

        saf_log_sta();
    }

    void callback_npss(const sc_interfaces::msg::NominalPowerSupplyStatus & msg) 
    {
        std::cout << std::endl;
        RCLCPP_INFO(this->get_logger(), "Receives NPSS: '%d'", msg.data);
        
        if (msg.data) safeclai.vehicle.in.power_ok ();
        else safeclai.vehicle.in.power_nok ();

        saf_log_sta();
    }
    
    void callback_sq(const sc_interfaces::msg::SystemQuality & msg) 
    {
        RCLCPP_INFO(this->get_logger(), "receive System Quality: '%d'", msg.quality);
        
        if (msg.quality) safeclai.safety.in.quality_hi ();
        else safeclai.safety.in.quality_lo ();

        saf_log_sta();
    }

    void callback_st(const sc_interfaces::msg::SafetyTrigger & msg) 
    {
        RCLCPP_INFO(this->get_logger(), "receive Safety Trigger: '%d'", msg.trigger);
        
        if (msg.trigger) safeclai.safety.in.trigger_hi ();
        else safeclai.safety.in.trigger_lo ();

        saf_log_sta();
    }

    void callback_set(const sc_interfaces::msg::SafetyEnvelopeTrigger & msg) 
    {
        RCLCPP_INFO(this->get_logger(), "receive Safety Envelope Trigger: '%d'", msg.trigger);
        
        if (msg.trigger) safeclai.safety.in.envelope_trigger_hi ();
        else safeclai.safety.in.envelope_trigger_lo ();

        saf_log_sta();
    }


    void saf_log_sta() const
    {
        auto message = sc_interfaces::msg::SafetyLogicState();
        switch(safeclai.logic.ss)
        {
        case ::cc::ilogic::SS::SS0:
            message.state_major = 0;
            message.state_minor = 0;
            break;
        case ::cc::ilogic::SS::SS1_1:
            message.state_major = 1;
            message.state_minor = 1;
            break;
        case ::cc::ilogic::SS::SS1_2:
            message.state_major = 1;
            message.state_minor = 2;
            break;
        case ::cc::ilogic::SS::SS2_1:
            message.state_major = 2;
            message.state_minor = 1;
            break;
        case ::cc::ilogic::SS::SS2_2:
            message.state_major = 2;
            message.state_minor = 2;
            break;
        case ::cc::ilogic::SS::SS3_1:
            message.state_major = 3;
            message.state_minor = 1;
            break;
        case ::cc::ilogic::SS::SS3_2:
            message.state_major = 3;
            message.state_minor = 2;
            break;
        default:
          throw std::runtime_error ("unknown state: " + std::to_string(static_cast<size_t>(safeclai.logic.ss)));
        }
        RCLCPP_INFO(this->get_logger(), "send Safely Logic State: '%d,%d'", message.state_major, message.state_minor);
        publisher_sls->publish(message);
    }
    
    void callback_har (const std::shared_ptr<sc_interfaces::srv::HMIActivationRequest::Request>  request,
                       std::shared_ptr<sc_interfaces::srv::HMIActivationRequest::Response>)
    {
       RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "receive HMI Activation Request: %d", request->data);

       if (request->data) safeclai.hmi.in.activate ();
       else safeclai.hmi.in.deactivate ();

       saf_log_sta();
    }

    void callback_car (const std::shared_ptr<sc_interfaces::srv::CommunicationActivationRequest::Request>  request,
                       std::shared_ptr<sc_interfaces::srv::CommunicationActivationRequest::Response>)
    {
        RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "receive Communication Activation Request: %d", request->data);

        // 2 different sources, the same behavior?!!
        if (request->data) safeclai.hmi.in.activate ();
        else safeclai.hmi.in.deactivate ();

        saf_log_sta();
    }
};

int main(int argc, char * argv[])
{
    std::cout << "Logic" << std::endl;
    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<SafetyLogic>());
    rclcpp::shutdown();
    return 0;
}
