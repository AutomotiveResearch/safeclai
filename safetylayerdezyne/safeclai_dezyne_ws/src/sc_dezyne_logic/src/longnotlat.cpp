// Copyright 2016 Open Source Robotics Foundation, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <functional>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"
#include "sc_interfaces/msg/safety_logic_state.hpp"
#include "sc_interfaces/msg/brake_override.hpp"
#include "sc_interfaces/msg/safety_deceleration_request.hpp"
#include "sc_interfaces/msg/safety_steering_request.hpp"

#include <iostream>
#include <string>

#include <dzn/runtime.hh>
#include <dzn/locator.hh>
#include "longnotlat.hh"

using std::placeholders::_1;

class Longnotlatmanager : public rclcpp::Node
{
    dzn::locator locator;
    dzn::runtime runtime;
    cc::longnotlat lnl;

public:
    Longnotlatmanager() 
        : Node("longnotlat")
        , locator ()
        , runtime ()
        , lnl (locator.set (runtime))
    {
        lnl.ext.in.notify_set                      = [] { std::cout << "*** notify-set\n"; };
        lnl.ext.in.notify_remove                   = [] { std::cout << "*** notify-remove\n"; };
        lnl.ext.in.safety_deceleration_request     = [] { std::cout << "*** safety_deceleration_request\n"; };
        lnl.ext.in.safety_deceleration_request_cancel = [] { std::cout << "*** safety_deceleration_request-cancel\n"; };
        lnl.ext.in.safety_steering_request         = [] { std::cout << "*** safety_steering_request\n"; };
        lnl.ext.in.safety_steering_request_cancel  = [] { std::cout << "*** safety_steering_request-cancel\n"; };

        subscription_sls = this->create_subscription<sc_interfaces::msg::SafetyLogicState>(
          "sc_topic_safety_logic_state", 10, std::bind(&Longnotlatmanager::topic_callback_sls, this, _1));
        subscription_bo = this->create_subscription<sc_interfaces::msg::BrakeOverride>(
          "sc_topic_brake_override", 10, std::bind(&Longnotlatmanager::topic_callback_bo, this, _1));
      
        publisher_sdr = this->create_publisher<sc_interfaces::msg::SafetyDecelerationRequest>("sc_topic_safety_deceleration_request", 10);
        publisher_ssr = this->create_publisher<sc_interfaces::msg::SafetySteeringRequest>("sc_topic_safety_steering_request", 10);
    
        curr_state_major = -1;
        curr_state_minor = -1;
        curr_brake_override = false;
        
        
        // JG, 231211: to run stand alone; temporary
        //lnl.bo.in.brake_override_on();
    }

private:
    uint16_t	curr_state_major;
    uint16_t	curr_state_minor;
    bool		curr_brake_override;
  
    rclcpp::Subscription<sc_interfaces::msg::SafetyLogicState>::SharedPtr		subscription_sls;
    rclcpp::Subscription<sc_interfaces::msg::BrakeOverride>::SharedPtr		    subscription_bo;
  
    rclcpp::Publisher<sc_interfaces::msg::SafetyDecelerationRequest>::SharedPtr publisher_sdr;
    rclcpp::Publisher<sc_interfaces::msg::SafetySteeringRequest>::SharedPtr 	publisher_ssr;

    void saf_dec_req(float request) const
    {
        auto message = sc_interfaces::msg::SafetyDecelerationRequest();
        message.request = request;
        RCLCPP_INFO(this->get_logger(), "> sdr: '%f'", message.request);
        publisher_sdr->publish(message);
    }
  
    void saf_ste_req(bool frozen) const
    {
        auto message = sc_interfaces::msg::SafetySteeringRequest();
        message.frozen = frozen;
        RCLCPP_INFO(this->get_logger(), "> ssr: '%d'", message.frozen);
        publisher_ssr->publish(message);
    }
  
    void not_to_ext(uint16_t new_state_major, uint16_t new_state_minor) const
    {
        if ((new_state_major != curr_state_major) || (new_state_minor != curr_state_minor))
        {
            // only for new states
            
            if ((new_state_major == 1 && new_state_minor == 1) ||
                (new_state_major == 2 && new_state_minor == 1) ||
                (new_state_major == 2 && new_state_minor == 2))
            {
                RCLCPP_INFO(this->get_logger(), "send NotificationToExternal (to be implemented): '%d-%d'", 
                        new_state_major, new_state_minor);
            }
            if (curr_state_major == 2 && new_state_major != 2)
            {
	            RCLCPP_INFO(this->get_logger(), "remove NotificationToExternal (to be implemented): '%d-%d'", 
	                    new_state_major, new_state_minor);
            }
        }
    }
  
    void break_logic (uint16_t state_major, uint16_t state_minor, bool break_override)
    {
        if ((break_override) || (state_major == 2))
        {
            saf_dec_req (100);
        }
        else
        {
            saf_dec_req (0);
        }
        
        // suppress 'unused parameter'
        if (state_minor) {}
    }

    void steering_logic (uint16_t state_major, uint16_t state_minor)
    {
        if (state_major == 2 && state_minor == 2)
        {
            saf_ste_req (true);
        }
        else
        {
            saf_ste_req (false);
        }
    }

    void topic_callback_sls(const sc_interfaces::msg::SafetyLogicState & msg) 
    {
        std::cout << std::endl;
        RCLCPP_INFO(this->get_logger(), "< sls: '%d-%d'", msg.state_major, msg.state_minor);
        
        not_to_ext(msg.state_major, msg.state_minor);
        
        curr_state_major = msg.state_major;
        curr_state_minor = msg.state_minor;
        
        break_logic (curr_state_major, curr_state_minor, curr_brake_override);  
        steering_logic (curr_state_major, curr_state_minor);
    }
  
    void topic_callback_bo(const sc_interfaces::msg::BrakeOverride & msg) 
    {
        std::cout << std::endl;
        RCLCPP_INFO(this->get_logger(), "< bo: '%d'", msg.data);
        
        //if (msg.data)
        //{
        //    lnl.bo.in.brake_override_on();
        //}
        //else
        //{
        //    lnl.bo.in.brake_override_off();
        //}
        
        curr_brake_override = msg.data;
        break_logic (curr_state_major, curr_state_minor, curr_brake_override);  
    }

};

int main(int argc, char * argv[])
{
    std::cout << "Long-Not-Lat" << std::endl;
    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<Longnotlatmanager>());
    rclcpp::shutdown();
    return 0;
}

