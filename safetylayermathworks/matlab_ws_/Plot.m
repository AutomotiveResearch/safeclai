%Plot some data from simulations
Vxy = [5 -5];
out = sim("SafeCLAI.slx");
SM = out.SafetyMargin.Data(:,:,1);
SM_other = out.SafetyMarginOther.Data(:,:,1);
SE = out.SafetyEnvelope.Data(:,:,1);

%close all
%figure()
plot(SM(1,:),SM(2,:));
axis('equal');

hold on
plot(SM_other(1,:),SM_other(2,:));
axis('equal')

plot(SE(1,:),SE(2,:));
axis('equal')
legend('ego margin', 'other margin', 'envelope')
grid on