/*
 * SafeCLAI.h
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "SafeCLAI".
 *
 * Model version              : 4.394
 * Simulink Coder version : 23.2 (R2023b) 01-Aug-2023
 * C++ source code generated on : Fri Apr 12 12:25:48 2024
 *
 * Target selection: ert.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_SafeCLAI_h_
#define RTW_HEADER_SafeCLAI_h_
#include "rtwtypes.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#include "slros2_initialize.h"
#include "SafeCLAI_types.h"

extern "C"
{

#include "rt_nonfinite.h"

}

extern "C"
{

#include "rtGetInf.h"

}

extern "C"
{

#include "rtGetNaN.h"

}

#include <stddef.h>

/* Macros for accessing real-time model data structure */
#ifndef rtmGetErrorStatus
#define rtmGetErrorStatus(rtm)         ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
#define rtmSetErrorStatus(rtm, val)    ((rtm)->errorStatus = (val))
#endif

/* Block signals for system '<S1>/Header Assignment' */
struct B_HeaderAssignment_SafeCLAI_T {
  SL_Bus_geometry_msgs_PolygonStamped HeaderAssign;/* '<S11>/HeaderAssign' */
  char_T cv[513];
  char_T rtb_ASCIItoString_m[256];
  char_T Switch1[256];                 /* '<S11>/Switch1' */
};

/* Block states (default storage) for system '<S1>/Header Assignment' */
struct DW_HeaderAssignment_SafeCLAI_T {
  ros_slros2_internal_block_Cur_T obj; /* '<S11>/Current Time' */
  boolean_T objisempty;                /* '<S11>/Current Time' */
};

/* Block signals (default storage) */
struct B_SafeCLAI_T {
  SL_Bus_geometry_msgs_PolygonStamped msg_out_h;/* '<S1>/MATLAB Function1' */
  real_T output[3100];                 /* '<S2>/CalcOtherSafetyMargin' */
  SL_Bus_sc_interfaces_ObservedObjects b_varargout_2;
  real_T DataTypeConversion[100];      /* '<S9>/Data Type Conversion' */
  real_T DataTypeConversion1[100];     /* '<S9>/Data Type Conversion1' */
  real_T out[100];                     /* '<S8>/MATLAB Function2' */
  SL_Bus_sc_interfaces_SystemState b_varargout_2_m;
  SL_Bus_sc_interfaces_SafetyTrigger BusAssignment;/* '<S2>/Bus Assignment' */
  SL_Bus_sc_interfaces_SafetyEnvelopeTrigger BusAssignment1;/* '<S3>/Bus Assignment1' */
  real_T SafetyMargin_xy[62];          /* '<S1>/MATLAB Function' */
  real_T OtherSafetyMargin_xy[62];
  real_T OtherSafetyMargin[62];
  real_T out_x[50];
  real_T out_y[50];
  real_T x_l[12];
  real_T y_l[12];
  real_T b_x[12];
  real_T x_r[12];
  real_T y_r[12];
  real_T c_x[12];
  real_T x_f[7];
  real_T y_f[7];
  char_T b_zeroDelimTopic[34];
  char_T b_zeroDelimTopic_c[32];
  char_T b_zeroDelimTopic_k[29];
  real_T DataTypeConversion2;          /* '<S9>/Data Type Conversion2' */
  real_T SMoffset;
  real_T tmp;
  real_T test_y;
  real_T angle_l;
  real_T Rf;
  real_T angle_f;
  real_T x_f_tmp;
  real32_T s;
  real32_T Rl;
  B_HeaderAssignment_SafeCLAI_T HeaderAssignment1;/* '<S2>/Header Assignment1' */
  B_HeaderAssignment_SafeCLAI_T HeaderAssignment;/* '<S1>/Header Assignment' */
};

/* Block states (default storage) for system '<Root>' */
struct DW_SafeCLAI_T {
  ros_slros2_internal_block_Pub_T obj; /* '<S23>/SinkBlock' */
  ros_slros2_internal_block_Pub_T obj_h;/* '<S20>/SinkBlock' */
  ros_slros2_internal_block_Pub_T obj_p;/* '<S19>/SinkBlock' */
  ros_slros2_internal_block_Pub_T obj_n;/* '<S14>/SinkBlock' */
  ros_slros2_internal_block_Sub_T obj_i;/* '<S6>/SourceBlock' */
  ros_slros2_internal_block_Sub_T obj_hg;/* '<S5>/SourceBlock' */
  ros_slros2_internal_block_Sub_T obj_n1;/* '<S4>/SourceBlock' */
  boolean_T objisempty;                /* '<S6>/SourceBlock' */
  boolean_T objisempty_f;              /* '<S5>/SourceBlock' */
  boolean_T objisempty_n;              /* '<S4>/SourceBlock' */
  boolean_T objisempty_d;              /* '<S23>/SinkBlock' */
  boolean_T objisempty_h;              /* '<S20>/SinkBlock' */
  boolean_T objisempty_dd;             /* '<S19>/SinkBlock' */
  boolean_T objisempty_m;              /* '<S14>/SinkBlock' */
  DW_HeaderAssignment_SafeCLAI_T HeaderAssignment1;/* '<S2>/Header Assignment1' */
  DW_HeaderAssignment_SafeCLAI_T HeaderAssignment;/* '<S1>/Header Assignment' */
};

/* External outputs (root outports fed by signals with default storage) */
struct ExtY_SafeCLAI_T {
  boolean_T EnvelopeOut;               /* '<Root>/EnvelopeOut' */
  boolean_T SafetyOutput;              /* '<Root>/SafetyOutput' */
};

/* Parameters for system: '<S1>/Header Assignment' */
struct P_HeaderAssignment_SafeCLAI_T_ {
  real_T Constant1_Value;              /* Expression: SetFrameID
                                        * Referenced by: '<S11>/Constant1'
                                        */
  real_T Constant_Value;               /* Expression: InsertTimeStamp
                                        * Referenced by: '<S11>/Constant'
                                        */
  char_T StringConstant1_String[256];  /* Expression: FrameID
                                        * Referenced by: '<S11>/String Constant1'
                                        */
};

/* Parameters (default storage) */
struct P_SafeCLAI_T_ {
  SL_Bus_geometry_msgs_PolygonStamped Constant_Value;/* Computed Parameter: Constant_Value
                                                      * Referenced by: '<S10>/Constant'
                                                      */
  SL_Bus_geometry_msgs_PolygonStamped Constant_Value_p;/* Computed Parameter: Constant_Value_p
                                                        * Referenced by: '<S16>/Constant'
                                                        */
  SL_Bus_geometry_msgs_PolygonStamped Out1_Y0;/* Computed Parameter: Out1_Y0
                                               * Referenced by: '<S26>/Out1'
                                               */
  SL_Bus_geometry_msgs_PolygonStamped Constant_Value_f;/* Computed Parameter: Constant_Value_f
                                                        * Referenced by: '<S5>/Constant'
                                                        */
  SL_Bus_sc_interfaces_ObservedObjects Out1_Y0_i;/* Computed Parameter: Out1_Y0_i
                                                  * Referenced by: '<S27>/Out1'
                                                  */
  SL_Bus_sc_interfaces_ObservedObjects Constant_Value_b;/* Computed Parameter: Constant_Value_b
                                                         * Referenced by: '<S6>/Constant'
                                                         */
  SL_Bus_sc_interfaces_SystemState Out1_Y0_m;/* Computed Parameter: Out1_Y0_m
                                              * Referenced by: '<S25>/Out1'
                                              */
  SL_Bus_sc_interfaces_SystemState Constant_Value_m;/* Computed Parameter: Constant_Value_m
                                                     * Referenced by: '<S4>/Constant'
                                                     */
  SL_Bus_sc_interfaces_SafetyEnvelopeTrigger Constant_Value_k;/* Computed Parameter: Constant_Value_k
                                                               * Referenced by: '<S22>/Constant'
                                                               */
  SL_Bus_sc_interfaces_SafetyTrigger Constant_Value_j;/* Computed Parameter: Constant_Value_j
                                                       * Referenced by: '<S15>/Constant'
                                                       */
  real_T SafetyMargin_Y0;              /* Computed Parameter: SafetyMargin_Y0
                                        * Referenced by: '<S1>/SafetyMargin'
                                        */
  real_T Out1_Y0_p;                    /* Computed Parameter: Out1_Y0_p
                                        * Referenced by: '<S8>/Out1'
                                        */
  real_T Speed_Y0;                     /* Computed Parameter: Speed_Y0
                                        * Referenced by: '<S9>/Speed'
                                        */
  real_T Pos_Y0;                       /* Computed Parameter: Pos_Y0
                                        * Referenced by: '<S9>/Pos'
                                        */
  real_T length_Y0;                    /* Computed Parameter: length_Y0
                                        * Referenced by: '<S9>/length'
                                        */
  real32_T Speed_Y0_m;                 /* Computed Parameter: Speed_Y0_m
                                        * Referenced by: '<S7>/Speed'
                                        */
  real32_T steer_Y0;                   /* Computed Parameter: steer_Y0
                                        * Referenced by: '<S7>/steer'
                                        */
  boolean_T SafetyTrigger_Y0;          /* Computed Parameter: SafetyTrigger_Y0
                                        * Referenced by: '<S2>/SafetyTrigger'
                                        */
  boolean_T SafetyEnvelopeTrigger_Y0;
                                 /* Computed Parameter: SafetyEnvelopeTrigger_Y0
                                  * Referenced by: '<S3>/Safety Envelope Trigger'
                                  */
  P_HeaderAssignment_SafeCLAI_T HeaderAssignment1;/* '<S2>/Header Assignment1' */
  P_HeaderAssignment_SafeCLAI_T HeaderAssignment;/* '<S1>/Header Assignment' */
};

/* Real-time Model Data Structure */
struct tag_RTM_SafeCLAI_T {
  const char_T *errorStatus;
};

/* Class declaration for model SafeCLAI */
class SafeCLAI
{
  /* public data and function members */
 public:
  /* Real-Time Model get method */
  RT_MODEL_SafeCLAI_T * getRTM();

  /* Root outports get method */
  const ExtY_SafeCLAI_T &getExternalOutputs() const
  {
    return SafeCLAI_Y;
  }

  /* model start function */
  void start();

  /* Initial conditions function */
  void initialize();

  /* model step function */
  void step();

  /* model terminate function */
  void terminate();

  /* Constructor */
  SafeCLAI();

  /* Destructor */
  ~SafeCLAI();

  /* private data and function members */
 private:
  /* External outputs */
  ExtY_SafeCLAI_T SafeCLAI_Y;

  /* Block signals */
  B_SafeCLAI_T SafeCLAI_B;

  /* Block states */
  DW_SafeCLAI_T SafeCLAI_DW;

  /* Tunable parameters */
  static P_SafeCLAI_T SafeCLAI_P;

  /* private member function(s) for subsystem '<S1>/Header Assignment'*/
  static void SafeCLAI_HeaderAssignment_Start(DW_HeaderAssignment_SafeCLAI_T
    *localDW);
  void SafeCLAI_HeaderAssignment(const SL_Bus_geometry_msgs_PolygonStamped
    *rtu_Input, B_HeaderAssignment_SafeCLAI_T *localB,
    P_HeaderAssignment_SafeCLAI_T *localP);
  static void SafeCLAI_HeaderAssignment_Term(DW_HeaderAssignment_SafeCLAI_T
    *localDW);

  /* private member function(s) for subsystem '<Root>'*/
  void SafeCLAI_SystemCore_setup_poanc(ros_slros2_internal_block_Sub_T *obj);
  void SafeCLA_SystemCore_setup_poancb(ros_slros2_internal_block_Sub_T *obj);
  void SafeCLAI_SystemCore_setup_poan(ros_slros2_internal_block_Sub_T *obj);
  void SafeCLAI_SystemCore_setup(ros_slros2_internal_block_Pub_T *obj);
  void SafeCLAI_SystemCore_setup_poa(ros_slros2_internal_block_Pub_T *obj);
  void SafeCLAI_SystemCore_setup_po(ros_slros2_internal_block_Pub_T *obj);
  void SafeCLAI_SystemCore_setup_p(ros_slros2_internal_block_Pub_T *obj);
  real_T SafeCLAI_mod_e(real_T x);
  real_T SafeCLAI_mod(real_T x);

  /* Real-Time Model */
  RT_MODEL_SafeCLAI_T SafeCLAI_M;
};

extern volatile boolean_T stopRequested;
extern volatile boolean_T runModel;

/*-
 * These blocks were eliminated from the model due to optimizations:
 *
 * Block '<Root>/Display' : Unused code path elimination
 * Block '<Root>/Display1' : Unused code path elimination
 * Block '<Root>/Display2' : Unused code path elimination
 * Block '<Root>/Display3' : Unused code path elimination
 */

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'SafeCLAI'
 * '<S1>'   : 'SafeCLAI/Ego Safety Margin'
 * '<S2>'   : 'SafeCLAI/Other Safety Margin1'
 * '<S3>'   : 'SafeCLAI/Safety envelope check'
 * '<S4>'   : 'SafeCLAI/Subscribe1'
 * '<S5>'   : 'SafeCLAI/Subscribe2'
 * '<S6>'   : 'SafeCLAI/Subscribe3'
 * '<S7>'   : 'SafeCLAI/Subsystem'
 * '<S8>'   : 'SafeCLAI/Subsystem1'
 * '<S9>'   : 'SafeCLAI/Subsystem3'
 * '<S10>'  : 'SafeCLAI/Ego Safety Margin/Blank Message3'
 * '<S11>'  : 'SafeCLAI/Ego Safety Margin/Header Assignment'
 * '<S12>'  : 'SafeCLAI/Ego Safety Margin/MATLAB Function'
 * '<S13>'  : 'SafeCLAI/Ego Safety Margin/MATLAB Function1'
 * '<S14>'  : 'SafeCLAI/Ego Safety Margin/Publish3'
 * '<S15>'  : 'SafeCLAI/Other Safety Margin1/Blank Message'
 * '<S16>'  : 'SafeCLAI/Other Safety Margin1/Blank Message2'
 * '<S17>'  : 'SafeCLAI/Other Safety Margin1/CalcOtherSafetyMargin'
 * '<S18>'  : 'SafeCLAI/Other Safety Margin1/Header Assignment1'
 * '<S19>'  : 'SafeCLAI/Other Safety Margin1/Publish'
 * '<S20>'  : 'SafeCLAI/Other Safety Margin1/Publish2'
 * '<S21>'  : 'SafeCLAI/Other Safety Margin1/show other safety margin'
 * '<S22>'  : 'SafeCLAI/Safety envelope check/Blank Message1'
 * '<S23>'  : 'SafeCLAI/Safety envelope check/Publish1'
 * '<S24>'  : 'SafeCLAI/Safety envelope check/SafetyEnvelopeCheck'
 * '<S25>'  : 'SafeCLAI/Subscribe1/Enabled Subsystem'
 * '<S26>'  : 'SafeCLAI/Subscribe2/Enabled Subsystem'
 * '<S27>'  : 'SafeCLAI/Subscribe3/Enabled Subsystem'
 * '<S28>'  : 'SafeCLAI/Subsystem1/MATLAB Function2'
 * '<S29>'  : 'SafeCLAI/Subsystem3/MATLAB Function2'
 */
#endif                                 /* RTW_HEADER_SafeCLAI_h_ */
