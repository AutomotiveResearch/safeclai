#ifndef _SLROS_BUSMSG_CONVERSION_H_
#define _SLROS_BUSMSG_CONVERSION_H_

#include "rclcpp/rclcpp.hpp"
#include <builtin_interfaces/msg/time.hpp>
#include <geometry_msgs/msg/point32.hpp>
#include <geometry_msgs/msg/polygon.hpp>
#include <geometry_msgs/msg/polygon_stamped.hpp>
#include <sc_interfaces/msg/object.hpp>
#include <sc_interfaces/msg/observed_objects.hpp>
#include <sc_interfaces/msg/safety_envelope_trigger.hpp>
#include <sc_interfaces/msg/safety_trigger.hpp>
#include <sc_interfaces/msg/system_state.hpp>
#include <std_msgs/msg/header.hpp>
#include "SafeCLAI_types.h"
#include "slros_msgconvert_utils.h"


void convertFromBus(builtin_interfaces::msg::Time& msgPtr, SL_Bus_builtin_interfaces_Time const* busPtr);
void convertToBus(SL_Bus_builtin_interfaces_Time* busPtr, const builtin_interfaces::msg::Time& msgPtr);

void convertFromBus(geometry_msgs::msg::Point32& msgPtr, SL_Bus_geometry_msgs_Point32 const* busPtr);
void convertToBus(SL_Bus_geometry_msgs_Point32* busPtr, const geometry_msgs::msg::Point32& msgPtr);

void convertFromBus(geometry_msgs::msg::Polygon& msgPtr, SL_Bus_geometry_msgs_Polygon const* busPtr);
void convertToBus(SL_Bus_geometry_msgs_Polygon* busPtr, const geometry_msgs::msg::Polygon& msgPtr);

void convertFromBus(geometry_msgs::msg::PolygonStamped& msgPtr, SL_Bus_geometry_msgs_PolygonStamped const* busPtr);
void convertToBus(SL_Bus_geometry_msgs_PolygonStamped* busPtr, const geometry_msgs::msg::PolygonStamped& msgPtr);

void convertFromBus(sc_interfaces::msg::Object& msgPtr, SL_Bus_sc_interfaces_Object const* busPtr);
void convertToBus(SL_Bus_sc_interfaces_Object* busPtr, const sc_interfaces::msg::Object& msgPtr);

void convertFromBus(sc_interfaces::msg::ObservedObjects& msgPtr, SL_Bus_sc_interfaces_ObservedObjects const* busPtr);
void convertToBus(SL_Bus_sc_interfaces_ObservedObjects* busPtr, const sc_interfaces::msg::ObservedObjects& msgPtr);

void convertFromBus(sc_interfaces::msg::SafetyEnvelopeTrigger& msgPtr, SL_Bus_sc_interfaces_SafetyEnvelopeTrigger const* busPtr);
void convertToBus(SL_Bus_sc_interfaces_SafetyEnvelopeTrigger* busPtr, const sc_interfaces::msg::SafetyEnvelopeTrigger& msgPtr);

void convertFromBus(sc_interfaces::msg::SafetyTrigger& msgPtr, SL_Bus_sc_interfaces_SafetyTrigger const* busPtr);
void convertToBus(SL_Bus_sc_interfaces_SafetyTrigger* busPtr, const sc_interfaces::msg::SafetyTrigger& msgPtr);

void convertFromBus(sc_interfaces::msg::SystemState& msgPtr, SL_Bus_sc_interfaces_SystemState const* busPtr);
void convertToBus(SL_Bus_sc_interfaces_SystemState* busPtr, const sc_interfaces::msg::SystemState& msgPtr);

void convertFromBus(std_msgs::msg::Header& msgPtr, SL_Bus_std_msgs_Header const* busPtr);
void convertToBus(SL_Bus_std_msgs_Header* busPtr, const std_msgs::msg::Header& msgPtr);


#endif
