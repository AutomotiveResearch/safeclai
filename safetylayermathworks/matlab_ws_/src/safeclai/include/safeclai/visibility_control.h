#ifndef SAFECLAI__VISIBILITY_CONTROL_H_
#define SAFECLAI__VISIBILITY_CONTROL_H_
#if defined _WIN32 || defined __CYGWIN__
  #ifdef __GNUC__
    #define SAFECLAI_EXPORT __attribute__ ((dllexport))
    #define SAFECLAI_IMPORT __attribute__ ((dllimport))
  #else
    #define SAFECLAI_EXPORT __declspec(dllexport)
    #define SAFECLAI_IMPORT __declspec(dllimport)
  #endif
  #ifdef SAFECLAI_BUILDING_LIBRARY
    #define SAFECLAI_PUBLIC SAFECLAI_EXPORT
  #else
    #define SAFECLAI_PUBLIC SAFECLAI_IMPORT
  #endif
  #define SAFECLAI_PUBLIC_TYPE SAFECLAI_PUBLIC
  #define SAFECLAI_LOCAL
#else
  #define SAFECLAI_EXPORT __attribute__ ((visibility("default")))
  #define SAFECLAI_IMPORT
  #if __GNUC__ >= 4
    #define SAFECLAI_PUBLIC __attribute__ ((visibility("default")))
    #define SAFECLAI_LOCAL  __attribute__ ((visibility("hidden")))
  #else
    #define SAFECLAI_PUBLIC
    #define SAFECLAI_LOCAL
  #endif
  #define SAFECLAI_PUBLIC_TYPE
#endif
#endif  // SAFECLAI__VISIBILITY_CONTROL_H_
// Generated 12-Apr-2024 12:25:56
 