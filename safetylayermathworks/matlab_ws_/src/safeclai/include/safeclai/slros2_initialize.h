// Copyright 2022-2023 The MathWorks, Inc.
// Generated 12-Apr-2024 12:25:53
#ifndef _SLROS2_INITIALIZE_H_
#define _SLROS2_INITIALIZE_H_
#include "SafeCLAI_types.h"
// Generic pub-sub header
#include "slros2_generic_pubsub.h"
// Generic service header
#include "slros2_generic_service.h"
#include "slros2_time.h"
extern rclcpp::Node::SharedPtr SLROSNodePtr;
#ifndef SET_QOS_VALUES
#define SET_QOS_VALUES(qosStruct, hist, dep, dur, rel)  \
    {                                                   \
        qosStruct.history = hist;                       \
        qosStruct.depth = dep;                          \
        qosStruct.durability = dur;                     \
        qosStruct.reliability = rel;                    \
    }
#endif
inline rclcpp::QoS getQOSSettingsFromRMW(const rmw_qos_profile_t& qosProfile) {
    rclcpp::QoS qos(rclcpp::QoSInitialization::from_rmw(qosProfile));
    if (RMW_QOS_POLICY_DURABILITY_TRANSIENT_LOCAL == qosProfile.durability) {
        qos.transient_local();
    } else {
        qos.durability_volatile();
    }
    if (RMW_QOS_POLICY_RELIABILITY_RELIABLE == qosProfile.reliability) {
        qos.reliable();
    } else {
        qos.best_effort();
    }
    return qos;
}
// SafeCLAI/Ego Safety Margin/Publish3
extern SimulinkPublisher<geometry_msgs::msg::PolygonStamped,SL_Bus_geometry_msgs_PolygonStamped> Pub_SafeCLAI_183;
// SafeCLAI/Other Safety Margin1/Publish
extern SimulinkPublisher<sc_interfaces::msg::SafetyTrigger,SL_Bus_sc_interfaces_SafetyTrigger> Pub_SafeCLAI_139;
// SafeCLAI/Other Safety Margin1/Publish2
extern SimulinkPublisher<geometry_msgs::msg::PolygonStamped,SL_Bus_geometry_msgs_PolygonStamped> Pub_SafeCLAI_284;
// SafeCLAI/Safety envelope check/Publish1
extern SimulinkPublisher<sc_interfaces::msg::SafetyEnvelopeTrigger,SL_Bus_sc_interfaces_SafetyEnvelopeTrigger> Pub_SafeCLAI_143;
// SafeCLAI/Subscribe1
extern SimulinkSubscriber<sc_interfaces::msg::SystemState,SL_Bus_sc_interfaces_SystemState> Sub_SafeCLAI_132;
// SafeCLAI/Subscribe2
extern SimulinkSubscriber<geometry_msgs::msg::PolygonStamped,SL_Bus_geometry_msgs_PolygonStamped> Sub_SafeCLAI_197;
// SafeCLAI/Subscribe3
extern SimulinkSubscriber<sc_interfaces::msg::ObservedObjects,SL_Bus_sc_interfaces_ObservedObjects> Sub_SafeCLAI_230;
#endif
