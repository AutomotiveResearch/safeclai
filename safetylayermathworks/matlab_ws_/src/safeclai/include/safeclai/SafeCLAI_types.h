/*
 * SafeCLAI_types.h
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "SafeCLAI".
 *
 * Model version              : 4.394
 * Simulink Coder version : 23.2 (R2023b) 01-Aug-2023
 * C++ source code generated on : Fri Apr 12 12:25:48 2024
 *
 * Target selection: ert.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_SafeCLAI_types_h_
#define RTW_HEADER_SafeCLAI_types_h_
#include "rtwtypes.h"
#ifndef DEFINED_TYPEDEF_FOR_SL_Bus_builtin_interfaces_Time_
#define DEFINED_TYPEDEF_FOR_SL_Bus_builtin_interfaces_Time_

struct SL_Bus_builtin_interfaces_Time
{
  int32_T sec;
  uint32_T nanosec;
};

#endif

#ifndef DEFINED_TYPEDEF_FOR_SL_Bus_ROSVariableLengthArrayInfo_
#define DEFINED_TYPEDEF_FOR_SL_Bus_ROSVariableLengthArrayInfo_

struct SL_Bus_ROSVariableLengthArrayInfo
{
  uint32_T CurrentLength;
  uint32_T ReceivedLength;
};

#endif

#ifndef DEFINED_TYPEDEF_FOR_SL_Bus_std_msgs_Header_
#define DEFINED_TYPEDEF_FOR_SL_Bus_std_msgs_Header_

struct SL_Bus_std_msgs_Header
{
  SL_Bus_builtin_interfaces_Time stamp;
  uint8_T frame_id[513];
  SL_Bus_ROSVariableLengthArrayInfo frame_id_SL_Info;
};

#endif

#ifndef DEFINED_TYPEDEF_FOR_SL_Bus_geometry_msgs_Point32_
#define DEFINED_TYPEDEF_FOR_SL_Bus_geometry_msgs_Point32_

struct SL_Bus_geometry_msgs_Point32
{
  real32_T x;
  real32_T y;
  real32_T z;
};

#endif

#ifndef DEFINED_TYPEDEF_FOR_SL_Bus_geometry_msgs_Polygon_
#define DEFINED_TYPEDEF_FOR_SL_Bus_geometry_msgs_Polygon_

struct SL_Bus_geometry_msgs_Polygon
{
  SL_Bus_geometry_msgs_Point32 points[2048];
  SL_Bus_ROSVariableLengthArrayInfo points_SL_Info;
};

#endif

#ifndef DEFINED_TYPEDEF_FOR_SL_Bus_geometry_msgs_PolygonStamped_
#define DEFINED_TYPEDEF_FOR_SL_Bus_geometry_msgs_PolygonStamped_

struct SL_Bus_geometry_msgs_PolygonStamped
{
  SL_Bus_std_msgs_Header header;
  SL_Bus_geometry_msgs_Polygon polygon;
};

#endif

#ifndef DEFINED_TYPEDEF_FOR_SL_Bus_sc_interfaces_SafetyTrigger_
#define DEFINED_TYPEDEF_FOR_SL_Bus_sc_interfaces_SafetyTrigger_

struct SL_Bus_sc_interfaces_SafetyTrigger
{
  SL_Bus_std_msgs_Header header;
  boolean_T trigger;
};

#endif

#ifndef DEFINED_TYPEDEF_FOR_SL_Bus_sc_interfaces_SafetyEnvelopeTrigger_
#define DEFINED_TYPEDEF_FOR_SL_Bus_sc_interfaces_SafetyEnvelopeTrigger_

struct SL_Bus_sc_interfaces_SafetyEnvelopeTrigger
{
  SL_Bus_std_msgs_Header header;
  boolean_T trigger;
};

#endif

#ifndef DEFINED_TYPEDEF_FOR_SL_Bus_sc_interfaces_SystemState_
#define DEFINED_TYPEDEF_FOR_SL_Bus_sc_interfaces_SystemState_

struct SL_Bus_sc_interfaces_SystemState
{
  SL_Bus_std_msgs_Header header;
  real32_T velocity;
  real32_T heading;
  real32_T steering_angle;
  boolean_T safety_sensor_performance;
  boolean_T safety_sensor_quality;
  boolean_T actuator_performance;
  boolean_T actuator_quality;
};

#endif

#ifndef DEFINED_TYPEDEF_FOR_SL_Bus_sc_interfaces_Object_
#define DEFINED_TYPEDEF_FOR_SL_Bus_sc_interfaces_Object_

struct SL_Bus_sc_interfaces_Object
{
  real32_T pos_x;
  real32_T pos_y;
  real32_T pos_z;
  real32_T v_x;
  real32_T v_y;
  real32_T v_z;
  real32_T hitbox_x;
  real32_T hitbox_y;
};

#endif

#ifndef DEFINED_TYPEDEF_FOR_SL_Bus_sc_interfaces_ObservedObjects_
#define DEFINED_TYPEDEF_FOR_SL_Bus_sc_interfaces_ObservedObjects_

struct SL_Bus_sc_interfaces_ObservedObjects
{
  SL_Bus_std_msgs_Header header;
  SL_Bus_sc_interfaces_Object obejcts_position[100];
  int16_T len;
};

#endif

#ifndef struct_f_robotics_slcore_internal_bl_T
#define struct_f_robotics_slcore_internal_bl_T

struct f_robotics_slcore_internal_bl_T
{
  int32_T __dummy;
};

#endif                              /* struct_f_robotics_slcore_internal_bl_T */

#ifndef struct_ros_slros2_internal_block_Cur_T
#define struct_ros_slros2_internal_block_Cur_T

struct ros_slros2_internal_block_Cur_T
{
  boolean_T matlabCodegenIsDeleted;
  int32_T isInitialized;
  boolean_T isSetupComplete;
  f_robotics_slcore_internal_bl_T SampleTimeHandler;
};

#endif                              /* struct_ros_slros2_internal_block_Cur_T */

/* Custom Type definition for MATLABSystem: '<S6>/SourceBlock' */
#include "rmw/qos_profiles.h"
#include "rmw/types.h"
#include "rmw/types.h"
#include "rmw/types.h"
#ifndef struct_ros_slros2_internal_block_Pub_T
#define struct_ros_slros2_internal_block_Pub_T

struct ros_slros2_internal_block_Pub_T
{
  boolean_T matlabCodegenIsDeleted;
  int32_T isInitialized;
  boolean_T isSetupComplete;
};

#endif                              /* struct_ros_slros2_internal_block_Pub_T */

#ifndef struct_ros_slros2_internal_block_Sub_T
#define struct_ros_slros2_internal_block_Sub_T

struct ros_slros2_internal_block_Sub_T
{
  boolean_T matlabCodegenIsDeleted;
  int32_T isInitialized;
  boolean_T isSetupComplete;
};

#endif                              /* struct_ros_slros2_internal_block_Sub_T */

/* Parameters for system: '<S1>/Header Assignment' */
typedef struct P_HeaderAssignment_SafeCLAI_T_ P_HeaderAssignment_SafeCLAI_T;

/* Parameters (default storage) */
typedef struct P_SafeCLAI_T_ P_SafeCLAI_T;

/* Forward declaration for rtModel */
typedef struct tag_RTM_SafeCLAI_T RT_MODEL_SafeCLAI_T;

#endif                                 /* RTW_HEADER_SafeCLAI_types_h_ */
