#include "slros_busmsg_conversion.h"


// Conversions between SL_Bus_builtin_interfaces_Time and builtin_interfaces::msg::Time

void convertFromBus(builtin_interfaces::msg::Time& msgPtr, SL_Bus_builtin_interfaces_Time const* busPtr)
{
  const std::string rosMessageType("builtin_interfaces/Time");

  msgPtr.nanosec =  busPtr->nanosec;
  msgPtr.sec =  busPtr->sec;
}

void convertToBus(SL_Bus_builtin_interfaces_Time* busPtr, const builtin_interfaces::msg::Time& msgPtr)
{
  const std::string rosMessageType("builtin_interfaces/Time");

  busPtr->nanosec =  msgPtr.nanosec;
  busPtr->sec =  msgPtr.sec;
}


// Conversions between SL_Bus_geometry_msgs_Point32 and geometry_msgs::msg::Point32

void convertFromBus(geometry_msgs::msg::Point32& msgPtr, SL_Bus_geometry_msgs_Point32 const* busPtr)
{
  const std::string rosMessageType("geometry_msgs/Point32");

  msgPtr.x =  busPtr->x;
  msgPtr.y =  busPtr->y;
  msgPtr.z =  busPtr->z;
}

void convertToBus(SL_Bus_geometry_msgs_Point32* busPtr, const geometry_msgs::msg::Point32& msgPtr)
{
  const std::string rosMessageType("geometry_msgs/Point32");

  busPtr->x =  msgPtr.x;
  busPtr->y =  msgPtr.y;
  busPtr->z =  msgPtr.z;
}


// Conversions between SL_Bus_geometry_msgs_Polygon and geometry_msgs::msg::Polygon

void convertFromBus(geometry_msgs::msg::Polygon& msgPtr, SL_Bus_geometry_msgs_Polygon const* busPtr)
{
  const std::string rosMessageType("geometry_msgs/Polygon");

  convertFromBusVariableNestedArray(msgPtr.points, busPtr->points, busPtr->points_SL_Info);
}

void convertToBus(SL_Bus_geometry_msgs_Polygon* busPtr, const geometry_msgs::msg::Polygon& msgPtr)
{
  const std::string rosMessageType("geometry_msgs/Polygon");

  convertToBusVariableNestedArray(busPtr->points, busPtr->points_SL_Info, msgPtr.points, slros::EnabledWarning(rosMessageType, "points"));
}


// Conversions between SL_Bus_geometry_msgs_PolygonStamped and geometry_msgs::msg::PolygonStamped

void convertFromBus(geometry_msgs::msg::PolygonStamped& msgPtr, SL_Bus_geometry_msgs_PolygonStamped const* busPtr)
{
  const std::string rosMessageType("geometry_msgs/PolygonStamped");

  convertFromBus(msgPtr.header, &busPtr->header);
  convertFromBus(msgPtr.polygon, &busPtr->polygon);
}

void convertToBus(SL_Bus_geometry_msgs_PolygonStamped* busPtr, const geometry_msgs::msg::PolygonStamped& msgPtr)
{
  const std::string rosMessageType("geometry_msgs/PolygonStamped");

  convertToBus(&busPtr->header, msgPtr.header);
  convertToBus(&busPtr->polygon, msgPtr.polygon);
}


// Conversions between SL_Bus_sc_interfaces_Object and sc_interfaces::msg::Object

void convertFromBus(sc_interfaces::msg::Object& msgPtr, SL_Bus_sc_interfaces_Object const* busPtr)
{
  const std::string rosMessageType("sc_interfaces/Object");

  msgPtr.hitbox_x =  busPtr->hitbox_x;
  msgPtr.hitbox_y =  busPtr->hitbox_y;
  msgPtr.pos_x =  busPtr->pos_x;
  msgPtr.pos_y =  busPtr->pos_y;
  msgPtr.pos_z =  busPtr->pos_z;
  msgPtr.v_x =  busPtr->v_x;
  msgPtr.v_y =  busPtr->v_y;
  msgPtr.v_z =  busPtr->v_z;
}

void convertToBus(SL_Bus_sc_interfaces_Object* busPtr, const sc_interfaces::msg::Object& msgPtr)
{
  const std::string rosMessageType("sc_interfaces/Object");

  busPtr->hitbox_x =  msgPtr.hitbox_x;
  busPtr->hitbox_y =  msgPtr.hitbox_y;
  busPtr->pos_x =  msgPtr.pos_x;
  busPtr->pos_y =  msgPtr.pos_y;
  busPtr->pos_z =  msgPtr.pos_z;
  busPtr->v_x =  msgPtr.v_x;
  busPtr->v_y =  msgPtr.v_y;
  busPtr->v_z =  msgPtr.v_z;
}


// Conversions between SL_Bus_sc_interfaces_ObservedObjects and sc_interfaces::msg::ObservedObjects

void convertFromBus(sc_interfaces::msg::ObservedObjects& msgPtr, SL_Bus_sc_interfaces_ObservedObjects const* busPtr)
{
  const std::string rosMessageType("sc_interfaces/ObservedObjects");

  convertFromBus(msgPtr.header, &busPtr->header);
  msgPtr.len =  busPtr->len;
  convertFromBusFixedNestedArray(msgPtr.obejcts_position, busPtr->obejcts_position);
}

void convertToBus(SL_Bus_sc_interfaces_ObservedObjects* busPtr, const sc_interfaces::msg::ObservedObjects& msgPtr)
{
  const std::string rosMessageType("sc_interfaces/ObservedObjects");

  convertToBus(&busPtr->header, msgPtr.header);
  busPtr->len =  msgPtr.len;
  convertToBusFixedNestedArray(busPtr->obejcts_position, msgPtr.obejcts_position, slros::NoopWarning());
}


// Conversions between SL_Bus_sc_interfaces_SafetyEnvelopeTrigger and sc_interfaces::msg::SafetyEnvelopeTrigger

void convertFromBus(sc_interfaces::msg::SafetyEnvelopeTrigger& msgPtr, SL_Bus_sc_interfaces_SafetyEnvelopeTrigger const* busPtr)
{
  const std::string rosMessageType("sc_interfaces/SafetyEnvelopeTrigger");

  convertFromBus(msgPtr.header, &busPtr->header);
  msgPtr.trigger =  busPtr->trigger;
}

void convertToBus(SL_Bus_sc_interfaces_SafetyEnvelopeTrigger* busPtr, const sc_interfaces::msg::SafetyEnvelopeTrigger& msgPtr)
{
  const std::string rosMessageType("sc_interfaces/SafetyEnvelopeTrigger");

  convertToBus(&busPtr->header, msgPtr.header);
  busPtr->trigger =  msgPtr.trigger;
}


// Conversions between SL_Bus_sc_interfaces_SafetyTrigger and sc_interfaces::msg::SafetyTrigger

void convertFromBus(sc_interfaces::msg::SafetyTrigger& msgPtr, SL_Bus_sc_interfaces_SafetyTrigger const* busPtr)
{
  const std::string rosMessageType("sc_interfaces/SafetyTrigger");

  convertFromBus(msgPtr.header, &busPtr->header);
  msgPtr.trigger =  busPtr->trigger;
}

void convertToBus(SL_Bus_sc_interfaces_SafetyTrigger* busPtr, const sc_interfaces::msg::SafetyTrigger& msgPtr)
{
  const std::string rosMessageType("sc_interfaces/SafetyTrigger");

  convertToBus(&busPtr->header, msgPtr.header);
  busPtr->trigger =  msgPtr.trigger;
}


// Conversions between SL_Bus_sc_interfaces_SystemState and sc_interfaces::msg::SystemState

void convertFromBus(sc_interfaces::msg::SystemState& msgPtr, SL_Bus_sc_interfaces_SystemState const* busPtr)
{
  const std::string rosMessageType("sc_interfaces/SystemState");

  msgPtr.actuator_performance =  busPtr->actuator_performance;
  msgPtr.actuator_quality =  busPtr->actuator_quality;
  convertFromBus(msgPtr.header, &busPtr->header);
  msgPtr.heading =  busPtr->heading;
  msgPtr.safety_sensor_performance =  busPtr->safety_sensor_performance;
  msgPtr.safety_sensor_quality =  busPtr->safety_sensor_quality;
  msgPtr.steering_angle =  busPtr->steering_angle;
  msgPtr.velocity =  busPtr->velocity;
}

void convertToBus(SL_Bus_sc_interfaces_SystemState* busPtr, const sc_interfaces::msg::SystemState& msgPtr)
{
  const std::string rosMessageType("sc_interfaces/SystemState");

  busPtr->actuator_performance =  msgPtr.actuator_performance;
  busPtr->actuator_quality =  msgPtr.actuator_quality;
  convertToBus(&busPtr->header, msgPtr.header);
  busPtr->heading =  msgPtr.heading;
  busPtr->safety_sensor_performance =  msgPtr.safety_sensor_performance;
  busPtr->safety_sensor_quality =  msgPtr.safety_sensor_quality;
  busPtr->steering_angle =  msgPtr.steering_angle;
  busPtr->velocity =  msgPtr.velocity;
}


// Conversions between SL_Bus_std_msgs_Header and std_msgs::msg::Header

void convertFromBus(std_msgs::msg::Header& msgPtr, SL_Bus_std_msgs_Header const* busPtr)
{
  const std::string rosMessageType("std_msgs/Header");

  convertFromBusVariablePrimitiveArray(msgPtr.frame_id, busPtr->frame_id, busPtr->frame_id_SL_Info);
  convertFromBus(msgPtr.stamp, &busPtr->stamp);
}

void convertToBus(SL_Bus_std_msgs_Header* busPtr, const std_msgs::msg::Header& msgPtr)
{
  const std::string rosMessageType("std_msgs/Header");

  convertToBusVariablePrimitiveArray(busPtr->frame_id, busPtr->frame_id_SL_Info, msgPtr.frame_id, slros::EnabledWarning(rosMessageType, "frame_id"));
  convertToBus(&busPtr->stamp, msgPtr.stamp);
}

