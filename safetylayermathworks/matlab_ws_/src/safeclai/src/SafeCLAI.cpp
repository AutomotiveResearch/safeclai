/*
 * SafeCLAI.cpp
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "SafeCLAI".
 *
 * Model version              : 4.394
 * Simulink Coder version : 23.2 (R2023b) 01-Aug-2023
 * C++ source code generated on : Fri Apr 12 12:25:48 2024
 *
 * Target selection: ert.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "SafeCLAI.h"
#include "SafeCLAI_types.h"
#include "rtwtypes.h"
#include <string.h>
#include <math.h>
#include "SafeCLAI_private.h"

extern "C"
{

#include "rt_nonfinite.h"

}

#include "rmw/qos_profiles.h"
#include "rmw/types.h"
#include <stddef.h>
#include "rt_defines.h"

/*
 * Start for atomic system:
 *    '<S1>/Header Assignment'
 *    '<S2>/Header Assignment1'
 */
void SafeCLAI::SafeCLAI_HeaderAssignment_Start(DW_HeaderAssignment_SafeCLAI_T
  *localDW)
{
  /* Start for MATLABSystem: '<S11>/Current Time' */
  localDW->obj.matlabCodegenIsDeleted = false;
  localDW->objisempty = true;
  localDW->obj.isInitialized = 1;
  localDW->obj.isSetupComplete = true;
}

/*
 * Output and update for atomic system:
 *    '<S1>/Header Assignment'
 *    '<S2>/Header Assignment1'
 */
void SafeCLAI::SafeCLAI_HeaderAssignment(const
  SL_Bus_geometry_msgs_PolygonStamped *rtu_Input, B_HeaderAssignment_SafeCLAI_T *
  localB, P_HeaderAssignment_SafeCLAI_T *localP)
{
  SL_Bus_builtin_interfaces_Time rtb_CurrentTime_0;

  /* ASCIIToString: '<S11>/ASCII to String' */
  for (int32_T i = 0; i < 255; i++) {
    localB->rtb_ASCIItoString_m[i] = static_cast<int8_T>
      (rtu_Input->header.frame_id[i]);
  }

  localB->rtb_ASCIItoString_m[255] = '\x00';

  /* Switch: '<S11>/Switch1' incorporates:
   *  ASCIIToString: '<S11>/ASCII to String'
   *  Constant: '<S11>/Constant1'
   *  StringConstant: '<S11>/String Constant1'
   */
  if (localP->Constant1_Value != 0.0) {
    strncpy(&localB->Switch1[0], &localP->StringConstant1_String[0], 255U);
    localB->Switch1[255] = '\x00';
  } else {
    strncpy(&localB->Switch1[0], &localB->rtb_ASCIItoString_m[0], 255U);
    localB->Switch1[255] = '\x00';
  }

  /* End of Switch: '<S11>/Switch1' */

  /* MATLABSystem: '<S11>/Current Time' */
  currentROS2TimeBus(&rtb_CurrentTime_0);

  /* Switch: '<S11>/Switch' incorporates:
   *  Constant: '<S11>/Constant'
   */
  if (!(localP->Constant_Value != 0.0)) {
    rtb_CurrentTime_0 = rtu_Input->header.stamp;
  }

  /* End of Switch: '<S11>/Switch' */

  /* BusAssignment: '<S11>/HeaderAssign' */
  localB->HeaderAssign = *rtu_Input;

  /* StringToASCII: '<S11>/String To ASCII' */
  strncpy(&localB->cv[0], &localB->Switch1[0], 513U);

  /* BusAssignment: '<S11>/HeaderAssign' incorporates:
   *  StringLength: '<S11>/String Length'
   *  StringToASCII: '<S11>/String To ASCII'
   */
  for (int32_T i = 0; i < 513; i++) {
    localB->HeaderAssign.header.frame_id[i] = static_cast<uint8_T>(localB->cv[i]);
  }

  localB->HeaderAssign.header.frame_id_SL_Info.CurrentLength = strlen
    (&localB->Switch1[0]);
  localB->HeaderAssign.header.stamp = rtb_CurrentTime_0;
}

/*
 * Termination for atomic system:
 *    '<S1>/Header Assignment'
 *    '<S2>/Header Assignment1'
 */
void SafeCLAI::SafeCLAI_HeaderAssignment_Term(DW_HeaderAssignment_SafeCLAI_T
  *localDW)
{
  /* Terminate for MATLABSystem: '<S11>/Current Time' */
  if (!localDW->obj.matlabCodegenIsDeleted) {
    localDW->obj.matlabCodegenIsDeleted = true;
  }

  /* End of Terminate for MATLABSystem: '<S11>/Current Time' */
}

void SafeCLAI::SafeCLAI_SystemCore_setup_poanc(ros_slros2_internal_block_Sub_T
  *obj)
{
  rmw_qos_durability_policy_t durability;
  rmw_qos_history_policy_t history;
  rmw_qos_profile_t qos_profile;
  rmw_qos_reliability_policy_t reliability;
  char_T b_zeroDelimTopic[16];
  static const char_T tmp[15] = { '/', 's', 'a', 'f', 't', 'y', '_', 'e', 'n',
    'v', 'e', 'l', 'o', 'p', 'e' };

  obj->isInitialized = 1;
  qos_profile = rmw_qos_profile_default;
  history = RMW_QOS_POLICY_HISTORY_KEEP_LAST;
  reliability = RMW_QOS_POLICY_RELIABILITY_BEST_EFFORT;
  durability = RMW_QOS_POLICY_DURABILITY_VOLATILE;
  SET_QOS_VALUES(qos_profile, history, (size_t)1.0, durability, reliability);
  for (int32_T i = 0; i < 15; i++) {
    b_zeroDelimTopic[i] = tmp[i];
  }

  b_zeroDelimTopic[15] = '\x00';
  Sub_SafeCLAI_197.createSubscriber(&b_zeroDelimTopic[0], qos_profile);
  obj->isSetupComplete = true;
}

void SafeCLAI::SafeCLA_SystemCore_setup_poancb(ros_slros2_internal_block_Sub_T
  *obj)
{
  rmw_qos_durability_policy_t durability;
  rmw_qos_history_policy_t history;
  rmw_qos_profile_t qos_profile;
  rmw_qos_reliability_policy_t reliability;
  static const char_T tmp[28] = { '/', 's', 'c', '_', 't', 'o', 'p', 'i', 'c',
    '_', 'p', 'e', 'r', 'c', 'e', 'p', 't', 'i', 'o', 'n', '_', 'o', 'b', 'j',
    'e', 'c', 't', 's' };

  obj->isInitialized = 1;
  qos_profile = rmw_qos_profile_default;
  history = RMW_QOS_POLICY_HISTORY_KEEP_LAST;
  reliability = RMW_QOS_POLICY_RELIABILITY_BEST_EFFORT;
  durability = RMW_QOS_POLICY_DURABILITY_VOLATILE;
  SET_QOS_VALUES(qos_profile, history, (size_t)1.0, durability, reliability);
  for (int32_T i = 0; i < 28; i++) {
    SafeCLAI_B.b_zeroDelimTopic_k[i] = tmp[i];
  }

  SafeCLAI_B.b_zeroDelimTopic_k[28] = '\x00';
  Sub_SafeCLAI_230.createSubscriber(&SafeCLAI_B.b_zeroDelimTopic_k[0],
    qos_profile);
  obj->isSetupComplete = true;
}

void SafeCLAI::SafeCLAI_SystemCore_setup_poan(ros_slros2_internal_block_Sub_T
  *obj)
{
  rmw_qos_durability_policy_t durability;
  rmw_qos_history_policy_t history;
  rmw_qos_profile_t qos_profile;
  rmw_qos_reliability_policy_t reliability;
  char_T b_zeroDelimTopic[17];
  static const char_T tmp[16] = { '/', 's', 'c', '/', 's', 'y', 's', 't', 'e',
    'm', '_', 's', 't', 'a', 't', 'e' };

  obj->isInitialized = 1;
  qos_profile = rmw_qos_profile_default;
  history = RMW_QOS_POLICY_HISTORY_KEEP_LAST;
  reliability = RMW_QOS_POLICY_RELIABILITY_BEST_EFFORT;
  durability = RMW_QOS_POLICY_DURABILITY_VOLATILE;
  SET_QOS_VALUES(qos_profile, history, (size_t)1.0, durability, reliability);
  for (int32_T i = 0; i < 16; i++) {
    b_zeroDelimTopic[i] = tmp[i];
  }

  b_zeroDelimTopic[16] = '\x00';
  Sub_SafeCLAI_132.createSubscriber(&b_zeroDelimTopic[0], qos_profile);
  obj->isSetupComplete = true;
}

void SafeCLAI::SafeCLAI_SystemCore_setup(ros_slros2_internal_block_Pub_T *obj)
{
  rmw_qos_durability_policy_t durability;
  rmw_qos_history_policy_t history;
  rmw_qos_profile_t qos_profile;
  rmw_qos_reliability_policy_t reliability;
  char_T b_zeroDelimTopic[24];
  static const char_T tmp[23] = { '/', 's', 'c', '_', 't', 'o', 'p', 'i', 'c',
    '_', 's', 'a', 'f', 'e', 't', 'y', '_', 'm', 'a', 'r', 'g', 'i', 'n' };

  obj->isInitialized = 1;
  qos_profile = rmw_qos_profile_default;
  history = RMW_QOS_POLICY_HISTORY_KEEP_LAST;
  reliability = RMW_QOS_POLICY_RELIABILITY_RELIABLE;
  durability = RMW_QOS_POLICY_DURABILITY_VOLATILE;
  SET_QOS_VALUES(qos_profile, history, (size_t)1.0, durability, reliability);
  for (int32_T i = 0; i < 23; i++) {
    b_zeroDelimTopic[i] = tmp[i];
  }

  b_zeroDelimTopic[23] = '\x00';
  Pub_SafeCLAI_183.createPublisher(&b_zeroDelimTopic[0], qos_profile);
  obj->isSetupComplete = true;
}

void SafeCLAI::SafeCLAI_SystemCore_setup_poa(ros_slros2_internal_block_Pub_T
  *obj)
{
  rmw_qos_durability_policy_t durability;
  rmw_qos_history_policy_t history;
  rmw_qos_profile_t qos_profile;
  rmw_qos_reliability_policy_t reliability;
  static const char_T tmp[33] = { '/', 's', 'c', '_', 't', 'o', 'p', 'i', 'c',
    '_', 's', 'a', 'f', 'e', 't', 'y', '_', 'e', 'n', 'v', 'e', 'l', 'o', 'p',
    'e', '_', 't', 'r', 'i', 'g', 'g', 'e', 'r' };

  obj->isInitialized = 1;
  qos_profile = rmw_qos_profile_default;
  history = RMW_QOS_POLICY_HISTORY_KEEP_LAST;
  reliability = RMW_QOS_POLICY_RELIABILITY_RELIABLE;
  durability = RMW_QOS_POLICY_DURABILITY_VOLATILE;
  SET_QOS_VALUES(qos_profile, history, (size_t)1.0, durability, reliability);
  for (int32_T i = 0; i < 33; i++) {
    SafeCLAI_B.b_zeroDelimTopic[i] = tmp[i];
  }

  SafeCLAI_B.b_zeroDelimTopic[33] = '\x00';
  Pub_SafeCLAI_143.createPublisher(&SafeCLAI_B.b_zeroDelimTopic[0], qos_profile);
  obj->isSetupComplete = true;
}

void SafeCLAI::SafeCLAI_SystemCore_setup_po(ros_slros2_internal_block_Pub_T *obj)
{
  rmw_qos_durability_policy_t durability;
  rmw_qos_history_policy_t history;
  rmw_qos_profile_t qos_profile;
  rmw_qos_reliability_policy_t reliability;
  static const char_T tmp[31] = { '/', 's', 'c', '_', 't', 'o', 'p', 'i', 'c',
    '_', 'o', 't', 'h', 'e', 'r', '_', 's', 'a', 'f', 'e', 't', 'y', '_', 'm',
    'a', 'r', 'g', 'i', 'n', '_', '0' };

  obj->isInitialized = 1;
  qos_profile = rmw_qos_profile_default;
  history = RMW_QOS_POLICY_HISTORY_KEEP_LAST;
  reliability = RMW_QOS_POLICY_RELIABILITY_RELIABLE;
  durability = RMW_QOS_POLICY_DURABILITY_VOLATILE;
  SET_QOS_VALUES(qos_profile, history, (size_t)1.0, durability, reliability);
  for (int32_T i = 0; i < 31; i++) {
    SafeCLAI_B.b_zeroDelimTopic_c[i] = tmp[i];
  }

  SafeCLAI_B.b_zeroDelimTopic_c[31] = '\x00';
  Pub_SafeCLAI_284.createPublisher(&SafeCLAI_B.b_zeroDelimTopic_c[0],
    qos_profile);
  obj->isSetupComplete = true;
}

void SafeCLAI::SafeCLAI_SystemCore_setup_p(ros_slros2_internal_block_Pub_T *obj)
{
  rmw_qos_durability_policy_t durability;
  rmw_qos_history_policy_t history;
  rmw_qos_profile_t qos_profile;
  rmw_qos_reliability_policy_t reliability;
  char_T b_zeroDelimTopic[25];
  static const char_T tmp[24] = { '/', 's', 'c', '_', 't', 'o', 'p', 'i', 'c',
    '_', 's', 'a', 'f', 'e', 't', 'y', '_', 't', 'r', 'i', 'g', 'g', 'e', 'r' };

  obj->isInitialized = 1;
  qos_profile = rmw_qos_profile_default;
  history = RMW_QOS_POLICY_HISTORY_KEEP_LAST;
  reliability = RMW_QOS_POLICY_RELIABILITY_RELIABLE;
  durability = RMW_QOS_POLICY_DURABILITY_VOLATILE;
  SET_QOS_VALUES(qos_profile, history, (size_t)1.0, durability, reliability);
  for (int32_T i = 0; i < 24; i++) {
    b_zeroDelimTopic[i] = tmp[i];
  }

  b_zeroDelimTopic[24] = '\x00';
  Pub_SafeCLAI_139.createPublisher(&b_zeroDelimTopic[0], qos_profile);
  obj->isSetupComplete = true;
}

/* Function for MATLAB Function: '<S3>/SafetyEnvelopeCheck' */
real_T SafeCLAI::SafeCLAI_mod_e(real_T x)
{
  real_T r;
  if (rtIsNaN(x) || rtIsInf(x)) {
    r = (rtNaN);
  } else if (x == 0.0) {
    r = 0.0;
  } else {
    r = fmod(x, 50.0);
    if (r == 0.0) {
      r = 0.0;
    } else if (x < 0.0) {
      r += 50.0;
    }
  }

  return r;
}

real_T rt_atan2d_snf(real_T u0, real_T u1)
{
  real_T y;
  if (rtIsNaN(u0) || rtIsNaN(u1)) {
    y = (rtNaN);
  } else if (rtIsInf(u0) && rtIsInf(u1)) {
    int32_T tmp;
    int32_T tmp_0;
    if (u0 > 0.0) {
      tmp = 1;
    } else {
      tmp = -1;
    }

    if (u1 > 0.0) {
      tmp_0 = 1;
    } else {
      tmp_0 = -1;
    }

    y = atan2(static_cast<real_T>(tmp), static_cast<real_T>(tmp_0));
  } else if (u1 == 0.0) {
    if (u0 > 0.0) {
      y = RT_PI / 2.0;
    } else if (u0 < 0.0) {
      y = -(RT_PI / 2.0);
    } else {
      y = 0.0;
    }
  } else {
    y = atan2(u0, u1);
  }

  return y;
}

/* Function for MATLAB Function: '<S2>/CalcOtherSafetyMargin' */
real_T SafeCLAI::SafeCLAI_mod(real_T x)
{
  real_T r;
  if (rtIsNaN(x) || rtIsInf(x)) {
    r = (rtNaN);
  } else if (x == 0.0) {
    r = 0.0;
  } else {
    r = fmod(x, 31.0);
    if (r == 0.0) {
      r = 0.0;
    } else if (x < 0.0) {
      r += 31.0;
    }
  }

  return r;
}

/* Model step function */
void SafeCLAI::step()
{
  int32_T c_i;
  int32_T c_tmp;
  int32_T i;
  int32_T id;
  int32_T idx;
  int32_T j;
  int32_T test_y_tmp;
  real32_T Rr;
  real32_T angle_l;
  real32_T x_f_tmp;
  boolean_T b_varargout_1;
  boolean_T b_varargout_1_0;
  boolean_T exitg1;

  /* MATLABSystem: '<S5>/SourceBlock' */
  b_varargout_1 = Sub_SafeCLAI_197.getLatestMessage(&SafeCLAI_B.msg_out_h);

  /* Outputs for Enabled SubSystem: '<Root>/Subsystem1' incorporates:
   *  EnablePort: '<S8>/Enable'
   */
  /* Outputs for Enabled SubSystem: '<S5>/Enabled Subsystem' incorporates:
   *  EnablePort: '<S26>/Enable'
   */
  if (b_varargout_1) {
    /* MATLAB Function: '<S8>/MATLAB Function2' */
    for (i = 0; i < 50; i++) {
      SafeCLAI_B.out_x[i] = SafeCLAI_B.msg_out_h.polygon.points[i].x;
      SafeCLAI_B.out_y[i] = SafeCLAI_B.msg_out_h.polygon.points[i].y;
    }

    SafeCLAI_B.out_x[49] = SafeCLAI_B.msg_out_h.polygon.points[0].x;
    SafeCLAI_B.out_y[49] = SafeCLAI_B.msg_out_h.polygon.points[0].y;
    for (c_i = 0; c_i < 50; c_i++) {
      idx = c_i << 1;
      SafeCLAI_B.out[idx] = SafeCLAI_B.out_x[c_i];
      SafeCLAI_B.out[idx + 1] = SafeCLAI_B.out_y[c_i];
    }

    /* End of MATLAB Function: '<S8>/MATLAB Function2' */
  }

  /* End of MATLABSystem: '<S5>/SourceBlock' */
  /* End of Outputs for SubSystem: '<S5>/Enabled Subsystem' */
  /* End of Outputs for SubSystem: '<Root>/Subsystem1' */

  /* MATLABSystem: '<S6>/SourceBlock' */
  b_varargout_1 = Sub_SafeCLAI_230.getLatestMessage(&SafeCLAI_B.b_varargout_2);

  /* Outputs for Enabled SubSystem: '<Root>/Subsystem3' incorporates:
   *  EnablePort: '<S9>/Enable'
   */
  /* Outputs for Enabled SubSystem: '<S6>/Enabled Subsystem' incorporates:
   *  EnablePort: '<S27>/Enable'
   */
  if (b_varargout_1) {
    for (i = 0; i < 100; i++) {
      /* DataTypeConversion: '<S9>/Data Type Conversion1' incorporates:
       *  MATLAB Function: '<S9>/MATLAB Function2'
       */
      SafeCLAI_B.DataTypeConversion1[i] = 1.0;

      /* DataTypeConversion: '<S9>/Data Type Conversion' incorporates:
       *  MATLAB Function: '<S9>/MATLAB Function2'
       */
      SafeCLAI_B.DataTypeConversion[i] = 1.0;
    }

    /* MATLAB Function: '<S9>/MATLAB Function2' incorporates:
     *  DataTypeConversion: '<S9>/Data Type Conversion'
     *  DataTypeConversion: '<S9>/Data Type Conversion1'
     */
    idx = SafeCLAI_B.b_varargout_2.len;
    for (i = 0; i < idx; i++) {
      SafeCLAI_B.DataTypeConversion1[i] =
        SafeCLAI_B.b_varargout_2.obejcts_position[i].pos_x;
      SafeCLAI_B.DataTypeConversion1[i + 50] =
        SafeCLAI_B.b_varargout_2.obejcts_position[i].pos_y;
      SafeCLAI_B.DataTypeConversion[i] =
        SafeCLAI_B.b_varargout_2.obejcts_position[i].v_x;
      SafeCLAI_B.DataTypeConversion[i + 50] =
        SafeCLAI_B.b_varargout_2.obejcts_position[i].v_y;
    }

    /* DataTypeConversion: '<S9>/Data Type Conversion2' incorporates:
     *  MATLAB Function: '<S9>/MATLAB Function2'
     */
    SafeCLAI_B.DataTypeConversion2 = SafeCLAI_B.b_varargout_2.len;
  }

  /* End of Outputs for SubSystem: '<S6>/Enabled Subsystem' */
  /* End of Outputs for SubSystem: '<Root>/Subsystem3' */

  /* MATLABSystem: '<S4>/SourceBlock' */
  b_varargout_1_0 = Sub_SafeCLAI_132.getLatestMessage
    (&SafeCLAI_B.b_varargout_2_m);

  /* Outputs for Enabled SubSystem: '<Root>/Safety envelope check' incorporates:
   *  EnablePort: '<S3>/Enable'
   */
  /* Outputs for Enabled SubSystem: '<Root>/Ego Safety Margin' incorporates:
   *  EnablePort: '<S1>/Enable'
   */
  /* Outputs for Enabled SubSystem: '<Root>/Subsystem' incorporates:
   *  EnablePort: '<S7>/Enable'
   */
  /* Outputs for Enabled SubSystem: '<S4>/Enabled Subsystem' incorporates:
   *  EnablePort: '<S25>/Enable'
   */
  if (b_varargout_1_0) {
    /* MATLAB Function: '<S1>/MATLAB Function' incorporates:
     *  SignalConversion generated from: '<S7>/Bus Selector'
     * */
    memset(&SafeCLAI_B.x_l[0], 0, 12U * sizeof(real_T));
    memset(&SafeCLAI_B.y_l[0], 0, 12U * sizeof(real_T));
    memset(&SafeCLAI_B.x_r[0], 0, 12U * sizeof(real_T));
    memset(&SafeCLAI_B.b_x[0], 0, 12U * sizeof(real_T));
    SafeCLAI_B.s = (((0.3F * SafeCLAI_B.b_varargout_2_m.velocity + 0.1F *
                      SafeCLAI_B.b_varargout_2_m.velocity) +
                     SafeCLAI_B.b_varargout_2_m.velocity *
                     SafeCLAI_B.b_varargout_2_m.velocity / 8.0F) +
                    SafeCLAI_B.b_varargout_2_m.velocity * 0.0F) + 1.8F;
    SafeCLAI_B.x_l[0] = -2.2359999999999998;
    SafeCLAI_B.y_l[0] = -0.725;
    SafeCLAI_B.x_l[1] = 0.0;
    SafeCLAI_B.y_l[1] = -0.725;
    SafeCLAI_B.Rl = 1.686F / static_cast<real32_T>(tan(static_cast<real_T>
      (SafeCLAI_B.b_varargout_2_m.steering_angle - 0.0139626339F)));
    SafeCLAI_B.x_r[0] = -2.2359999999999998;
    SafeCLAI_B.b_x[0] = 0.725;
    SafeCLAI_B.x_r[1] = 0.0;
    SafeCLAI_B.b_x[1] = 0.725;
    Rr = 1.686F / static_cast<real32_T>(tan(static_cast<real_T>
      (SafeCLAI_B.b_varargout_2_m.steering_angle + 0.0139626339F)));
    for (i = 0; i < 10; i++) {
      if (static_cast<real32_T>(fabs(static_cast<real_T>(SafeCLAI_B.Rl))) ==
          (rtInfF)) {
        SafeCLAI_B.x_l[i + 2] = (static_cast<real32_T>(i) + 1.0F) *
          (SafeCLAI_B.s / 10.0F);
        SafeCLAI_B.y_l[i + 2] = -0.725;
      } else {
        angle_l = (static_cast<real32_T>(i) + 1.0F) * (SafeCLAI_B.s / 10.0F) /
          SafeCLAI_B.Rl;
        SafeCLAI_B.x_l[i + 2] = (SafeCLAI_B.Rl + 0.725F) * static_cast<real32_T>
          (sin(static_cast<real_T>(angle_l)));
        SafeCLAI_B.y_l[i + 2] = SafeCLAI_B.Rl - (SafeCLAI_B.Rl + 0.725F) *
          static_cast<real32_T>(cos(static_cast<real_T>(angle_l)));
      }

      if (static_cast<real32_T>(fabs(static_cast<real_T>(Rr))) == (rtInfF)) {
        SafeCLAI_B.x_r[i + 2] = (static_cast<real32_T>(i) + 1.0F) *
          (SafeCLAI_B.s / 10.0F);
        SafeCLAI_B.b_x[i + 2] = 0.725;
      } else {
        angle_l = (static_cast<real32_T>(i) + 1.0F) * (SafeCLAI_B.s / 10.0F) /
          Rr;
        SafeCLAI_B.x_r[i + 2] = (Rr - 0.725F) * static_cast<real32_T>(sin(
          static_cast<real_T>(angle_l)));
        SafeCLAI_B.b_x[i + 2] = Rr - (Rr - 0.725F) * static_cast<real32_T>(cos(
          static_cast<real_T>(angle_l)));
      }
    }

    SafeCLAI_B.Rl = SafeCLAI_B.b_varargout_2_m.steering_angle - 0.0139626339F;
    SafeCLAI_B.SMoffset = 0.725;
    for (c_i = 0; c_i < 7; c_i++) {
      SafeCLAI_B.Rl += 0.00279252673F;
      SafeCLAI_B.SMoffset -= 0.145;
      Rr = 1.686F / static_cast<real32_T>(tan(static_cast<real_T>(SafeCLAI_B.Rl)));
      if (static_cast<real32_T>(fabs(static_cast<real_T>(Rr))) == (rtInfF)) {
        SafeCLAI_B.x_f[c_i] = SafeCLAI_B.s;
        SafeCLAI_B.y_f[c_i] = SafeCLAI_B.SMoffset;
      } else {
        angle_l = SafeCLAI_B.s / Rr;
        x_f_tmp = Rr + static_cast<real32_T>(SafeCLAI_B.SMoffset);
        SafeCLAI_B.x_f[c_i] = x_f_tmp * static_cast<real32_T>(sin(static_cast<
          real_T>(angle_l)));
        SafeCLAI_B.y_f[c_i] = Rr - x_f_tmp * static_cast<real32_T>(cos(
          static_cast<real_T>(angle_l)));
      }
    }

    for (c_i = 0; c_i < 6; c_i++) {
      SafeCLAI_B.tmp = SafeCLAI_B.x_r[c_i];
      SafeCLAI_B.x_r[c_i] = SafeCLAI_B.x_r[11 - c_i];
      SafeCLAI_B.x_r[11 - c_i] = SafeCLAI_B.tmp;
      SafeCLAI_B.tmp = SafeCLAI_B.b_x[c_i];
      SafeCLAI_B.b_x[c_i] = SafeCLAI_B.b_x[11 - c_i];
      SafeCLAI_B.b_x[11 - c_i] = SafeCLAI_B.tmp;
    }

    for (c_i = 0; c_i < 12; c_i++) {
      SafeCLAI_B.SafetyMargin_xy[c_i << 1] = SafeCLAI_B.x_l[c_i];
    }

    for (c_i = 0; c_i < 7; c_i++) {
      SafeCLAI_B.SafetyMargin_xy[(c_i + 12) << 1] = SafeCLAI_B.x_f[c_i];
    }

    for (c_i = 0; c_i < 12; c_i++) {
      SafeCLAI_B.SafetyMargin_xy[(c_i + 19) << 1] = SafeCLAI_B.x_r[c_i];
      SafeCLAI_B.SafetyMargin_xy[(c_i << 1) + 1] = SafeCLAI_B.y_l[c_i];
    }

    for (c_i = 0; c_i < 7; c_i++) {
      SafeCLAI_B.SafetyMargin_xy[((c_i + 12) << 1) + 1] = SafeCLAI_B.y_f[c_i];
    }

    for (c_i = 0; c_i < 12; c_i++) {
      SafeCLAI_B.SafetyMargin_xy[((c_i + 19) << 1) + 1] = SafeCLAI_B.b_x[c_i];
    }

    /* End of MATLAB Function: '<S1>/MATLAB Function' */

    /* MATLAB Function: '<S1>/MATLAB Function1' incorporates:
     *  Constant: '<S10>/Constant'
     */
    SafeCLAI_B.msg_out_h = SafeCLAI_P.Constant_Value;
    SafeCLAI_B.msg_out_h.polygon.points_SL_Info.CurrentLength = 31U;
    SafeCLAI_B.msg_out_h.polygon.points_SL_Info.ReceivedLength = 31U;
    for (i = 0; i < 31; i++) {
      c_i = i << 1;
      SafeCLAI_B.msg_out_h.polygon.points[i].x = static_cast<real32_T>
        (SafeCLAI_B.SafetyMargin_xy[c_i]);
      SafeCLAI_B.msg_out_h.polygon.points[i].y = static_cast<real32_T>
        (SafeCLAI_B.SafetyMargin_xy[c_i + 1]);
    }

    /* End of MATLAB Function: '<S1>/MATLAB Function1' */

    /* Outputs for Atomic SubSystem: '<S1>/Header Assignment' */
    SafeCLAI_HeaderAssignment(&SafeCLAI_B.msg_out_h,
      &SafeCLAI_B.HeaderAssignment, &SafeCLAI_P.HeaderAssignment);

    /* End of Outputs for SubSystem: '<S1>/Header Assignment' */

    /* MATLABSystem: '<S14>/SinkBlock' */
    Pub_SafeCLAI_183.publish(&SafeCLAI_B.HeaderAssignment.HeaderAssign);

    /* Outport: '<Root>/EnvelopeOut' incorporates:
     *  MATLAB Function: '<S3>/SafetyEnvelopeCheck'
     */
    SafeCLAI_Y.EnvelopeOut = false;

    /* MATLAB Function: '<S3>/SafetyEnvelopeCheck' */
    j = 0;
    exitg1 = false;
    while ((!exitg1) && (j < 31)) {
      test_y_tmp = j << 1;
      SafeCLAI_B.test_y = SafeCLAI_B.SafetyMargin_xy[test_y_tmp + 1];
      SafeCLAI_B.tmp = 0.0;
      for (i = 0; i < 50; i++) {
        idx = static_cast<int32_T>(SafeCLAI_mod_e(static_cast<real_T>(i) + 1.0)
          + 1.0) - 1;
        c_i = i << 1;
        SafeCLAI_B.SMoffset = SafeCLAI_B.out[c_i + 1];
        if (((SafeCLAI_B.SMoffset <= SafeCLAI_B.test_y) && (SafeCLAI_B.out[(idx <<
               1) + 1] > SafeCLAI_B.test_y)) || ((SafeCLAI_B.out[(idx << 1) + 1]
              <= SafeCLAI_B.test_y) && (SafeCLAI_B.SMoffset > SafeCLAI_B.test_y)))
        {
          c_tmp = idx << 1;
          if (SafeCLAI_B.SafetyMargin_xy[test_y_tmp] < (SafeCLAI_B.out[c_tmp] -
               SafeCLAI_B.out[c_i]) * (SafeCLAI_B.test_y - SafeCLAI_B.SMoffset) /
              (SafeCLAI_B.out[c_tmp + 1] - SafeCLAI_B.SMoffset) +
              SafeCLAI_B.out[c_i]) {
            SafeCLAI_B.tmp++;
          }
        }
      }

      if (rtIsInf(SafeCLAI_B.tmp)) {
        SafeCLAI_B.tmp = (rtNaN);
      } else if (SafeCLAI_B.tmp == 0.0) {
        SafeCLAI_B.tmp = 0.0;
      } else {
        SafeCLAI_B.tmp = fmod(SafeCLAI_B.tmp, 2.0);
      }

      if (SafeCLAI_B.tmp == 0.0) {
        SafeCLAI_Y.EnvelopeOut = true;
        exitg1 = true;
      } else {
        j++;
      }
    }

    /* BusAssignment: '<S3>/Bus Assignment1' incorporates:
     *  Constant: '<S22>/Constant'
     *  Outport: '<Root>/EnvelopeOut'
     */
    SafeCLAI_B.BusAssignment1 = SafeCLAI_P.Constant_Value_k;
    SafeCLAI_B.BusAssignment1.trigger = SafeCLAI_Y.EnvelopeOut;

    /* MATLABSystem: '<S23>/SinkBlock' */
    Pub_SafeCLAI_143.publish(&SafeCLAI_B.BusAssignment1);
  }

  /* End of MATLABSystem: '<S4>/SourceBlock' */
  /* End of Outputs for SubSystem: '<S4>/Enabled Subsystem' */
  /* End of Outputs for SubSystem: '<Root>/Subsystem' */
  /* End of Outputs for SubSystem: '<Root>/Ego Safety Margin' */
  /* End of Outputs for SubSystem: '<Root>/Safety envelope check' */

  /* Outputs for Enabled SubSystem: '<Root>/Other Safety Margin1' incorporates:
   *  EnablePort: '<S2>/Enable'
   */
  /* MATLABSystem: '<S6>/SourceBlock' */
  if (b_varargout_1) {
    /* MATLAB Function: '<S2>/CalcOtherSafetyMargin' */
    SafeCLAI_B.test_y = SafeCLAI_B.DataTypeConversion2;
    memset(&SafeCLAI_B.output[0], 0, 3100U * sizeof(real_T));

    /* Outport: '<Root>/SafetyOutput' incorporates:
     *  MATLAB Function: '<S2>/CalcOtherSafetyMargin'
     */
    SafeCLAI_Y.SafetyOutput = false;

    /* MATLAB Function: '<S2>/CalcOtherSafetyMargin' incorporates:
     *  DataTypeConversion: '<S9>/Data Type Conversion'
     *  DataTypeConversion: '<S9>/Data Type Conversion1'
     */
    memset(&SafeCLAI_B.x_l[0], 0, 12U * sizeof(real_T));
    memset(&SafeCLAI_B.y_l[0], 0, 12U * sizeof(real_T));
    memset(&SafeCLAI_B.x_r[0], 0, 12U * sizeof(real_T));
    memset(&SafeCLAI_B.y_r[0], 0, 12U * sizeof(real_T));
    if (SafeCLAI_B.DataTypeConversion2 > 50.0) {
      /* Outport: '<Root>/SafetyOutput' */
      SafeCLAI_Y.SafetyOutput = true;
      SafeCLAI_B.test_y = 1.0;
    }

    idx = static_cast<int32_T>(SafeCLAI_B.test_y);
    for (id = 0; id < idx; id++) {
      SafeCLAI_B.tmp = SafeCLAI_B.DataTypeConversion[id];
      SafeCLAI_B.test_y = SafeCLAI_B.DataTypeConversion[id + 50];
      SafeCLAI_B.tmp = sqrt(fabs(SafeCLAI_B.test_y * SafeCLAI_B.test_y +
        SafeCLAI_B.tmp * SafeCLAI_B.tmp));
      if (SafeCLAI_B.tmp < 0.1) {
        SafeCLAI_B.tmp = 0.0;
      }

      SafeCLAI_B.tmp = ((0.1 * SafeCLAI_B.tmp + 0.1 * SafeCLAI_B.tmp) +
                        SafeCLAI_B.tmp * SafeCLAI_B.tmp / 7.0) + 0.5;
      SafeCLAI_B.x_l[0] = -0.75;
      SafeCLAI_B.y_l[0] = -0.75;
      SafeCLAI_B.x_l[1] = 0.0;
      SafeCLAI_B.y_l[1] = -0.75;
      SafeCLAI_B.x_r[0] = -0.75;
      SafeCLAI_B.y_r[0] = 0.75;
      SafeCLAI_B.x_r[1] = 0.0;
      SafeCLAI_B.y_r[1] = 0.75;
      for (i = 0; i < 10; i++) {
        SafeCLAI_B.SMoffset = (static_cast<real_T>(i) + 1.0) * (SafeCLAI_B.tmp /
          10.0);
        SafeCLAI_B.angle_l = SafeCLAI_B.SMoffset / -2148.5916444741238;
        SafeCLAI_B.x_l[i + 2] = sin(SafeCLAI_B.angle_l) * -2147.8416444741238;
        SafeCLAI_B.y_l[i + 2] = -2148.5916444741238 - cos(SafeCLAI_B.angle_l) *
          -2147.8416444741238;
        SafeCLAI_B.SMoffset /= 2148.5916444741238;
        SafeCLAI_B.x_r[i + 2] = sin(SafeCLAI_B.SMoffset) * 2147.8416444741238;
        SafeCLAI_B.y_r[i + 2] = 2148.5916444741238 - cos(SafeCLAI_B.SMoffset) *
          2147.8416444741238;
      }

      SafeCLAI_B.angle_l = -0.00034906585039886593;
      SafeCLAI_B.SMoffset = 0.75;
      for (c_i = 0; c_i < 7; c_i++) {
        SafeCLAI_B.angle_l += 6.9813170079773186E-5;
        SafeCLAI_B.SMoffset -= 0.15;
        SafeCLAI_B.Rf = 0.75 / tan(SafeCLAI_B.angle_l);
        if (fabs(SafeCLAI_B.Rf) == (rtInf)) {
          SafeCLAI_B.x_f[c_i] = SafeCLAI_B.tmp;
          SafeCLAI_B.y_f[c_i] = SafeCLAI_B.SMoffset;
        } else {
          SafeCLAI_B.angle_f = SafeCLAI_B.tmp / SafeCLAI_B.Rf;
          SafeCLAI_B.x_f_tmp = SafeCLAI_B.Rf + SafeCLAI_B.SMoffset;
          SafeCLAI_B.x_f[c_i] = SafeCLAI_B.x_f_tmp * sin(SafeCLAI_B.angle_f);
          SafeCLAI_B.y_f[c_i] = SafeCLAI_B.Rf - SafeCLAI_B.x_f_tmp * cos
            (SafeCLAI_B.angle_f);
        }
      }

      memcpy(&SafeCLAI_B.b_x[0], &SafeCLAI_B.x_r[0], 12U * sizeof(real_T));
      for (c_i = 0; c_i < 6; c_i++) {
        SafeCLAI_B.tmp = SafeCLAI_B.b_x[c_i];
        SafeCLAI_B.b_x[c_i] = SafeCLAI_B.b_x[11 - c_i];
        SafeCLAI_B.b_x[11 - c_i] = SafeCLAI_B.tmp;
      }

      memcpy(&SafeCLAI_B.c_x[0], &SafeCLAI_B.y_r[0], 12U * sizeof(real_T));
      for (c_i = 0; c_i < 6; c_i++) {
        SafeCLAI_B.tmp = SafeCLAI_B.c_x[c_i];
        SafeCLAI_B.c_x[c_i] = SafeCLAI_B.c_x[11 - c_i];
        SafeCLAI_B.c_x[11 - c_i] = SafeCLAI_B.tmp;
      }

      for (c_i = 0; c_i < 12; c_i++) {
        SafeCLAI_B.OtherSafetyMargin_xy[c_i << 1] = SafeCLAI_B.x_l[c_i];
      }

      for (c_i = 0; c_i < 7; c_i++) {
        SafeCLAI_B.OtherSafetyMargin_xy[(c_i + 12) << 1] = SafeCLAI_B.x_f[c_i];
      }

      for (c_i = 0; c_i < 12; c_i++) {
        SafeCLAI_B.OtherSafetyMargin_xy[(c_i + 19) << 1] = SafeCLAI_B.b_x[c_i];
        SafeCLAI_B.OtherSafetyMargin_xy[(c_i << 1) + 1] = SafeCLAI_B.y_l[c_i];
      }

      for (c_i = 0; c_i < 7; c_i++) {
        SafeCLAI_B.OtherSafetyMargin_xy[((c_i + 12) << 1) + 1] =
          SafeCLAI_B.y_f[c_i];
      }

      for (c_i = 0; c_i < 12; c_i++) {
        SafeCLAI_B.OtherSafetyMargin_xy[((c_i + 19) << 1) + 1] =
          SafeCLAI_B.c_x[c_i];
      }

      SafeCLAI_B.tmp = rt_atan2d_snf(SafeCLAI_B.test_y,
        SafeCLAI_B.DataTypeConversion[id]);
      SafeCLAI_B.test_y = sin(SafeCLAI_B.tmp);
      SafeCLAI_B.SMoffset = cos(SafeCLAI_B.tmp);
      SafeCLAI_B.tmp = SafeCLAI_B.DataTypeConversion1[id];
      SafeCLAI_B.angle_l = SafeCLAI_B.DataTypeConversion1[id + 50];
      for (c_i = 0; c_i < 31; c_i++) {
        j = c_i << 1;
        SafeCLAI_B.angle_f = SafeCLAI_B.OtherSafetyMargin_xy[j + 1];
        SafeCLAI_B.x_f_tmp = SafeCLAI_B.OtherSafetyMargin_xy[j];
        SafeCLAI_B.Rf = SafeCLAI_B.angle_f * SafeCLAI_B.SMoffset +
          SafeCLAI_B.x_f_tmp * SafeCLAI_B.test_y;
        SafeCLAI_B.x_f_tmp = SafeCLAI_B.angle_f * -SafeCLAI_B.test_y +
          SafeCLAI_B.x_f_tmp * SafeCLAI_B.SMoffset;
        SafeCLAI_B.OtherSafetyMargin_xy[j] = SafeCLAI_B.x_f_tmp;
        SafeCLAI_B.OtherSafetyMargin_xy[j + 1] = SafeCLAI_B.Rf;
        SafeCLAI_B.angle_f = SafeCLAI_B.x_f_tmp + SafeCLAI_B.tmp;
        SafeCLAI_B.OtherSafetyMargin[j] = SafeCLAI_B.angle_f;
        SafeCLAI_B.Rf += SafeCLAI_B.angle_l;
        SafeCLAI_B.OtherSafetyMargin[j + 1] = SafeCLAI_B.Rf;
        j += 62 * id;
        SafeCLAI_B.output[j] = SafeCLAI_B.angle_f;
        SafeCLAI_B.output[j + 1] = SafeCLAI_B.Rf;
      }

      j = 0;
      exitg1 = false;
      while ((!exitg1) && (j < 31)) {
        test_y_tmp = j << 1;
        SafeCLAI_B.test_y = SafeCLAI_B.OtherSafetyMargin[test_y_tmp + 1];
        SafeCLAI_B.tmp = 0.0;
        for (i = 0; i < 31; i++) {
          c_tmp = static_cast<int32_T>(SafeCLAI_mod(static_cast<real_T>(i) + 1.0)
            + 1.0) - 1;
          c_i = i << 1;
          SafeCLAI_B.SMoffset = SafeCLAI_B.SafetyMargin_xy[c_i + 1];
          if (((SafeCLAI_B.SMoffset <= SafeCLAI_B.test_y) &&
               (SafeCLAI_B.SafetyMargin_xy[(c_tmp << 1) + 1] > SafeCLAI_B.test_y))
              || ((SafeCLAI_B.SafetyMargin_xy[(c_tmp << 1) + 1] <=
                   SafeCLAI_B.test_y) && (SafeCLAI_B.SMoffset >
                SafeCLAI_B.test_y))) {
            c_tmp <<= 1;
            if (SafeCLAI_B.OtherSafetyMargin[test_y_tmp] <
                (SafeCLAI_B.SafetyMargin_xy[c_tmp] -
                 SafeCLAI_B.SafetyMargin_xy[c_i]) * (SafeCLAI_B.test_y -
                 SafeCLAI_B.SMoffset) / (SafeCLAI_B.SafetyMargin_xy[c_tmp + 1] -
                 SafeCLAI_B.SMoffset) + SafeCLAI_B.SafetyMargin_xy[c_i]) {
              SafeCLAI_B.tmp++;
            }
          }
        }

        if (rtIsInf(SafeCLAI_B.tmp)) {
          SafeCLAI_B.tmp = (rtNaN);
        } else if (SafeCLAI_B.tmp == 0.0) {
          SafeCLAI_B.tmp = 0.0;
        } else {
          SafeCLAI_B.tmp = fmod(SafeCLAI_B.tmp, 2.0);
        }

        if (SafeCLAI_B.tmp == 1.0) {
          SafeCLAI_Y.SafetyOutput = true;
          exitg1 = true;
        } else {
          j++;
        }
      }
    }

    /* MATLAB Function: '<S2>/show other safety margin' incorporates:
     *  Constant: '<S16>/Constant'
     */
    SafeCLAI_B.msg_out_h = SafeCLAI_P.Constant_Value_p;
    SafeCLAI_B.msg_out_h.polygon.points_SL_Info.CurrentLength = 1550U;
    SafeCLAI_B.msg_out_h.polygon.points_SL_Info.ReceivedLength = 1550U;
    for (j = 0; j < 50; j++) {
      for (i = 0; i < 31; i++) {
        idx = j * 31 + i;
        c_i = (i << 1) + 62 * j;
        SafeCLAI_B.msg_out_h.polygon.points[idx].x = static_cast<real32_T>
          (SafeCLAI_B.output[c_i]);
        SafeCLAI_B.msg_out_h.polygon.points[idx].y = static_cast<real32_T>
          (SafeCLAI_B.output[c_i + 1]);
      }
    }

    /* End of MATLAB Function: '<S2>/show other safety margin' */

    /* Outputs for Atomic SubSystem: '<S2>/Header Assignment1' */
    SafeCLAI_HeaderAssignment(&SafeCLAI_B.msg_out_h,
      &SafeCLAI_B.HeaderAssignment1, &SafeCLAI_P.HeaderAssignment1);

    /* End of Outputs for SubSystem: '<S2>/Header Assignment1' */

    /* MATLABSystem: '<S20>/SinkBlock' */
    Pub_SafeCLAI_284.publish(&SafeCLAI_B.HeaderAssignment1.HeaderAssign);

    /* BusAssignment: '<S2>/Bus Assignment' incorporates:
     *  Constant: '<S15>/Constant'
     *  Outport: '<Root>/SafetyOutput'
     */
    SafeCLAI_B.BusAssignment = SafeCLAI_P.Constant_Value_j;
    SafeCLAI_B.BusAssignment.trigger = SafeCLAI_Y.SafetyOutput;

    /* MATLABSystem: '<S19>/SinkBlock' */
    Pub_SafeCLAI_139.publish(&SafeCLAI_B.BusAssignment);
  }

  /* End of Outputs for SubSystem: '<Root>/Other Safety Margin1' */
}

/* Model initialize function */
void SafeCLAI::initialize()
{
  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* Start for MATLABSystem: '<S5>/SourceBlock' */
  SafeCLAI_DW.objisempty_f = true;
  SafeCLAI_SystemCore_setup_poanc(&SafeCLAI_DW.obj_hg);

  /* Start for MATLABSystem: '<S6>/SourceBlock' */
  SafeCLAI_DW.objisempty = true;
  SafeCLA_SystemCore_setup_poancb(&SafeCLAI_DW.obj_i);

  /* Start for MATLABSystem: '<S4>/SourceBlock' */
  SafeCLAI_DW.objisempty_n = true;
  SafeCLAI_SystemCore_setup_poan(&SafeCLAI_DW.obj_n1);

  /* Start for Enabled SubSystem: '<Root>/Ego Safety Margin' */
  /* Start for Atomic SubSystem: '<S1>/Header Assignment' */
  SafeCLAI_HeaderAssignment_Start(&SafeCLAI_DW.HeaderAssignment);

  /* End of Start for SubSystem: '<S1>/Header Assignment' */

  /* Start for MATLABSystem: '<S14>/SinkBlock' */
  SafeCLAI_DW.objisempty_m = true;
  SafeCLAI_SystemCore_setup(&SafeCLAI_DW.obj_n);

  /* End of Start for SubSystem: '<Root>/Ego Safety Margin' */

  /* Start for Enabled SubSystem: '<Root>/Safety envelope check' */
  /* Start for MATLABSystem: '<S23>/SinkBlock' */
  SafeCLAI_DW.objisempty_d = true;
  SafeCLAI_SystemCore_setup_poa(&SafeCLAI_DW.obj);

  /* End of Start for SubSystem: '<Root>/Safety envelope check' */

  /* Start for Enabled SubSystem: '<Root>/Other Safety Margin1' */
  /* Start for Atomic SubSystem: '<S2>/Header Assignment1' */
  SafeCLAI_HeaderAssignment_Start(&SafeCLAI_DW.HeaderAssignment1);

  /* End of Start for SubSystem: '<S2>/Header Assignment1' */

  /* Start for MATLABSystem: '<S20>/SinkBlock' */
  SafeCLAI_DW.objisempty_h = true;
  SafeCLAI_SystemCore_setup_po(&SafeCLAI_DW.obj_h);

  /* Start for MATLABSystem: '<S19>/SinkBlock' */
  SafeCLAI_DW.objisempty_dd = true;
  SafeCLAI_SystemCore_setup_p(&SafeCLAI_DW.obj_p);

  /* End of Start for SubSystem: '<Root>/Other Safety Margin1' */
  {
    int32_T i;

    /* SystemInitialize for Enabled SubSystem: '<Root>/Subsystem3' */
    /* SystemInitialize for Enabled SubSystem: '<Root>/Subsystem1' */
    for (i = 0; i < 100; i++) {
      /* SystemInitialize for Outport: '<S8>/Out1' */
      SafeCLAI_B.out[i] = SafeCLAI_P.Out1_Y0_p;

      /* SystemInitialize for DataTypeConversion: '<S9>/Data Type Conversion' incorporates:
       *  Outport: '<S9>/Speed'
       */
      SafeCLAI_B.DataTypeConversion[i] = SafeCLAI_P.Speed_Y0;

      /* SystemInitialize for DataTypeConversion: '<S9>/Data Type Conversion1' incorporates:
       *  Outport: '<S9>/Pos'
       */
      SafeCLAI_B.DataTypeConversion1[i] = SafeCLAI_P.Pos_Y0;
    }

    /* End of SystemInitialize for SubSystem: '<Root>/Subsystem1' */

    /* SystemInitialize for DataTypeConversion: '<S9>/Data Type Conversion2' incorporates:
     *  Outport: '<S9>/length'
     */
    SafeCLAI_B.DataTypeConversion2 = SafeCLAI_P.length_Y0;

    /* End of SystemInitialize for SubSystem: '<Root>/Subsystem3' */

    /* SystemInitialize for Enabled SubSystem: '<Root>/Ego Safety Margin' */
    /* SystemInitialize for Outport: '<S1>/SafetyMargin' */
    for (i = 0; i < 62; i++) {
      SafeCLAI_B.SafetyMargin_xy[i] = SafeCLAI_P.SafetyMargin_Y0;
    }

    /* End of SystemInitialize for Outport: '<S1>/SafetyMargin' */
    /* End of SystemInitialize for SubSystem: '<Root>/Ego Safety Margin' */

    /* SystemInitialize for Enabled SubSystem: '<Root>/Safety envelope check' */
    /* SystemInitialize for Outport: '<Root>/EnvelopeOut' incorporates:
     *  Outport: '<S3>/Safety Envelope Trigger'
     */
    SafeCLAI_Y.EnvelopeOut = SafeCLAI_P.SafetyEnvelopeTrigger_Y0;

    /* End of SystemInitialize for SubSystem: '<Root>/Safety envelope check' */

    /* SystemInitialize for Enabled SubSystem: '<Root>/Other Safety Margin1' */
    /* SystemInitialize for Outport: '<Root>/SafetyOutput' incorporates:
     *  Outport: '<S2>/SafetyTrigger'
     */
    SafeCLAI_Y.SafetyOutput = SafeCLAI_P.SafetyTrigger_Y0;

    /* End of SystemInitialize for SubSystem: '<Root>/Other Safety Margin1' */
  }
}

/* Model terminate function */
void SafeCLAI::terminate()
{
  /* Terminate for MATLABSystem: '<S5>/SourceBlock' */
  if (!SafeCLAI_DW.obj_hg.matlabCodegenIsDeleted) {
    SafeCLAI_DW.obj_hg.matlabCodegenIsDeleted = true;
  }

  /* End of Terminate for MATLABSystem: '<S5>/SourceBlock' */

  /* Terminate for MATLABSystem: '<S6>/SourceBlock' */
  if (!SafeCLAI_DW.obj_i.matlabCodegenIsDeleted) {
    SafeCLAI_DW.obj_i.matlabCodegenIsDeleted = true;
  }

  /* End of Terminate for MATLABSystem: '<S6>/SourceBlock' */

  /* Terminate for MATLABSystem: '<S4>/SourceBlock' */
  if (!SafeCLAI_DW.obj_n1.matlabCodegenIsDeleted) {
    SafeCLAI_DW.obj_n1.matlabCodegenIsDeleted = true;
  }

  /* End of Terminate for MATLABSystem: '<S4>/SourceBlock' */

  /* Terminate for Enabled SubSystem: '<Root>/Ego Safety Margin' */
  /* Terminate for Atomic SubSystem: '<S1>/Header Assignment' */
  SafeCLAI_HeaderAssignment_Term(&SafeCLAI_DW.HeaderAssignment);

  /* End of Terminate for SubSystem: '<S1>/Header Assignment' */

  /* Terminate for MATLABSystem: '<S14>/SinkBlock' */
  if (!SafeCLAI_DW.obj_n.matlabCodegenIsDeleted) {
    SafeCLAI_DW.obj_n.matlabCodegenIsDeleted = true;
  }

  /* End of Terminate for MATLABSystem: '<S14>/SinkBlock' */
  /* End of Terminate for SubSystem: '<Root>/Ego Safety Margin' */

  /* Terminate for Enabled SubSystem: '<Root>/Safety envelope check' */
  /* Terminate for MATLABSystem: '<S23>/SinkBlock' */
  if (!SafeCLAI_DW.obj.matlabCodegenIsDeleted) {
    SafeCLAI_DW.obj.matlabCodegenIsDeleted = true;
  }

  /* End of Terminate for MATLABSystem: '<S23>/SinkBlock' */
  /* End of Terminate for SubSystem: '<Root>/Safety envelope check' */

  /* Terminate for Enabled SubSystem: '<Root>/Other Safety Margin1' */
  /* Terminate for Atomic SubSystem: '<S2>/Header Assignment1' */
  SafeCLAI_HeaderAssignment_Term(&SafeCLAI_DW.HeaderAssignment1);

  /* End of Terminate for SubSystem: '<S2>/Header Assignment1' */

  /* Terminate for MATLABSystem: '<S20>/SinkBlock' */
  if (!SafeCLAI_DW.obj_h.matlabCodegenIsDeleted) {
    SafeCLAI_DW.obj_h.matlabCodegenIsDeleted = true;
  }

  /* End of Terminate for MATLABSystem: '<S20>/SinkBlock' */

  /* Terminate for MATLABSystem: '<S19>/SinkBlock' */
  if (!SafeCLAI_DW.obj_p.matlabCodegenIsDeleted) {
    SafeCLAI_DW.obj_p.matlabCodegenIsDeleted = true;
  }

  /* End of Terminate for MATLABSystem: '<S19>/SinkBlock' */
  /* End of Terminate for SubSystem: '<Root>/Other Safety Margin1' */
}

/* Constructor */
SafeCLAI::SafeCLAI() :
  SafeCLAI_Y(),
  SafeCLAI_B(),
  SafeCLAI_DW(),
  SafeCLAI_M()
{
  /* Currently there is no constructor body generated.*/
}

/* Destructor */
SafeCLAI::~SafeCLAI()
{
  /* Currently there is no destructor body generated.*/
}

/* Real-Time Model get method */
RT_MODEL_SafeCLAI_T * SafeCLAI::getRTM()
{
  return (&SafeCLAI_M);
}
