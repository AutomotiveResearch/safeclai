// Copyright 2022-2023 The MathWorks, Inc.
// Generated 12-Apr-2024 12:25:53
#include "slros2_initialize.h"
// SafeCLAI/Ego Safety Margin/Publish3
SimulinkPublisher<geometry_msgs::msg::PolygonStamped,SL_Bus_geometry_msgs_PolygonStamped> Pub_SafeCLAI_183;
// SafeCLAI/Other Safety Margin1/Publish
SimulinkPublisher<sc_interfaces::msg::SafetyTrigger,SL_Bus_sc_interfaces_SafetyTrigger> Pub_SafeCLAI_139;
// SafeCLAI/Other Safety Margin1/Publish2
SimulinkPublisher<geometry_msgs::msg::PolygonStamped,SL_Bus_geometry_msgs_PolygonStamped> Pub_SafeCLAI_284;
// SafeCLAI/Safety envelope check/Publish1
SimulinkPublisher<sc_interfaces::msg::SafetyEnvelopeTrigger,SL_Bus_sc_interfaces_SafetyEnvelopeTrigger> Pub_SafeCLAI_143;
// SafeCLAI/Subscribe1
SimulinkSubscriber<sc_interfaces::msg::SystemState,SL_Bus_sc_interfaces_SystemState> Sub_SafeCLAI_132;
// SafeCLAI/Subscribe2
SimulinkSubscriber<geometry_msgs::msg::PolygonStamped,SL_Bus_geometry_msgs_PolygonStamped> Sub_SafeCLAI_197;
// SafeCLAI/Subscribe3
SimulinkSubscriber<sc_interfaces::msg::ObservedObjects,SL_Bus_sc_interfaces_ObservedObjects> Sub_SafeCLAI_230;
