function create_busses_object() 
% CREATE_BUSSES_OBJECT initializes a set of bus objects in the MATLAB base workspace 

% Bus object: control_signal 
clear elems;
elems(1) = Simulink.BusElement;
elems(1).Name = 'type';
elems(1).Dimensions = 1;
elems(1).DimensionsMode = 'Fixed';
elems(1).DataType = 'int8';
elems(1).Complexity = 'real';
elems(1).Min = [];
elems(1).Max = [];
elems(1).DocUnits = '';
elems(1).Description = '';

elems(2) = Simulink.BusElement;
elems(2).Name = 'message_counter';
elems(2).Dimensions = 1;
elems(2).DimensionsMode = 'Fixed';
elems(2).DataType = 'uint8';
elems(2).Complexity = 'real';
elems(2).Min = [];
elems(2).Max = [];
elems(2).DocUnits = '';
elems(2).Description = '';

elems(3) = Simulink.BusElement;
elems(3).Name = 'steering_command';
elems(3).Dimensions = 1;
elems(3).DimensionsMode = 'Fixed';
elems(3).DataType = 'single';
elems(3).Complexity = 'real';
elems(3).Min = [];
elems(3).Max = [];
elems(3).DocUnits = '';
elems(3).Description = '';

elems(4) = Simulink.BusElement;
elems(4).Name = 'speed_command';
elems(4).Dimensions = 1;
elems(4).DimensionsMode = 'Fixed';
elems(4).DataType = 'single';
elems(4).Complexity = 'real';
elems(4).Min = [];
elems(4).Max = [];
elems(4).DocUnits = '';
elems(4).Description = '';

control_signal = Simulink.Bus;
control_signal.HeaderFile = '';
control_signal.Description = '';
control_signal.DataScope = 'Auto';
control_signal.Alignment = -1;
control_signal.PreserveElementDimensions = 0;
control_signal.Elements = elems;
clear elems;
assignin('base','control_signal', control_signal);

% Bus object: nominal_input 
clear elems;

elems(1) = Simulink.BusElement;
elems(1).Name = 'message_counter';
elems(1).Dimensions = 1;
elems(1).DimensionsMode = 'Fixed';
elems(1).DataType = 'uint8';
elems(1).Complexity = 'real';
elems(1).Min = [];
elems(1).Max = [];
elems(1).DocUnits = '';
elems(1).Description = '';

elems(2) = Simulink.BusElement;
elems(2).Name = 'steering_command';
elems(2).Dimensions = 1;
elems(2).DimensionsMode = 'Fixed';
elems(2).DataType = 'single';
elems(2).Complexity = 'real';
elems(2).Min = [];
elems(2).Max = [];
elems(2).DocUnits = '';
elems(2).Description = '';

elems(3) = Simulink.BusElement;
elems(3).Name = 'speed_command';
elems(3).Dimensions = 1;
elems(3).DimensionsMode = 'Fixed';
elems(3).DataType = 'single';
elems(3).Complexity = 'real';
elems(3).Min = [];
elems(3).Max = [];
elems(3).DocUnits = '';
elems(3).Description = '';

nominal_input = Simulink.Bus;
nominal_input.HeaderFile = '';
nominal_input.Description = '';
nominal_input.DataScope = 'Auto';
nominal_input.Alignment = -1;
nominal_input.PreserveElementDimensions = 0;
nominal_input.Elements = elems;
clear elems;
assignin('base','nominal_input', nominal_input);

% Bus object: vehicle_information 
clear elems;
elems(1) = Simulink.BusElement;
elems(1).Name = 'message_counter';
elems(1).Dimensions = 1;
elems(1).DimensionsMode = 'Fixed';
elems(1).DataType = 'uint8';
elems(1).Complexity = 'real';
elems(1).Min = [];
elems(1).Max = [];
elems(1).DocUnits = '';
elems(1).Description = '';

elems(2) = Simulink.BusElement;
elems(2).Name = 'map_pos_x';
elems(2).Dimensions = 1;
elems(2).DimensionsMode = 'Fixed';
elems(2).DataType = 'single';
elems(2).Complexity = 'real';
elems(2).Min = [];
elems(2).Max = [];
elems(2).DocUnits = '';
elems(2).Description = '';

elems(3) = Simulink.BusElement;
elems(3).Name = 'map_pos_y';
elems(3).Dimensions = 1;
elems(3).DimensionsMode = 'Fixed';
elems(3).DataType = 'single';
elems(3).Complexity = 'real';
elems(3).Min = [];
elems(3).Max = [];
elems(3).DocUnits = '';
elems(3).Description = '';

elems(4) = Simulink.BusElement;
elems(4).Name = 'map_heading_rad';
elems(4).Dimensions = 1;
elems(4).DimensionsMode = 'Fixed';
elems(4).DataType = 'single';
elems(4).Complexity = 'real';
elems(4).Min = [];
elems(4).Max = [];
elems(4).DocUnits = '';
elems(4).Description = '';

elems(5) = Simulink.BusElement;
elems(5).Name = 'ego_velocity_x';
elems(5).Dimensions = 1;
elems(5).DimensionsMode = 'Fixed';
elems(5).DataType = 'single';
elems(5).Complexity = 'real';
elems(5).Min = [];
elems(5).Max = [];
elems(5).DocUnits = '';
elems(5).Description = '';

elems(6) = Simulink.BusElement;
elems(6).Name = 'ego_velocity_y';
elems(6).Dimensions = 1;
elems(6).DimensionsMode = 'Fixed';
elems(6).DataType = 'single';
elems(6).Complexity = 'real';
elems(6).Min = [];
elems(6).Max = [];
elems(6).DocUnits = '';
elems(6).Description = '';

elems(7) = Simulink.BusElement;
elems(7).Name = 'ego_steering_angle_rad';
elems(7).Dimensions = 1;
elems(7).DimensionsMode = 'Fixed';
elems(7).DataType = 'single';
elems(7).Complexity = 'real';
elems(7).Min = [];
elems(7).Max = [];
elems(7).DocUnits = '';
elems(7).Description = '';

elems(8) = Simulink.BusElement;
elems(8).Name = 'ego_ignition';
elems(8).Dimensions = 1;
elems(8).DimensionsMode = 'Fixed';
elems(8).DataType = 'int8';
elems(8).Complexity = 'real';
elems(8).Min = [];
elems(8).Max = [];
elems(8).DocUnits = '';
elems(8).Description = '';

elems(9) = Simulink.BusElement;
elems(9).Name = 'ego_system_quality';
elems(9).Dimensions = 1;
elems(9).DimensionsMode = 'Fixed';
elems(9).DataType = 'int8';
elems(9).Complexity = 'real';
elems(9).Min = [];
elems(9).Max = [];
elems(9).DocUnits = '';
elems(9).Description = '';

vehicle_information = Simulink.Bus;
vehicle_information.HeaderFile = '';
vehicle_information.Description = '';
vehicle_information.DataScope = 'Auto';
vehicle_information.Alignment = -1;
vehicle_information.PreserveElementDimensions = 0;
vehicle_information.Elements = elems;
clear elems;
assignin('base','vehicle_information', vehicle_information);

% Bus object: object_information_list 
clear elems;
elems(1) = Simulink.BusElement;
elems(1).Name = 'message_counter';
elems(1).Dimensions = 1;
elems(1).DimensionsMode = 'Fixed';
elems(1).DataType = 'uint8';
elems(1).Complexity = 'real';
elems(1).Min = [];
elems(1).Max = [];
elems(1).DocUnits = '';
elems(1).Description = '';

elems(2) = Simulink.BusElement;
elems(2).Name = 'size';
elems(2).Dimensions = 1;
elems(2).DimensionsMode = 'Fixed';
elems(2).DataType = 'uint8';
elems(2).Complexity = 'real';
elems(2).Min = [];
elems(2).Max = [];
elems(2).DocUnits = '';
elems(2).Description = '';

% Create loop to add all 
maxObjects = 4+2;
for index = 3:1:maxObjects
    elems(index) = Simulink.BusElement;
    elems(index).Name = strcat('object',int2str(index-2));
    elems(index).Dimensions = 1;
    elems(index).DimensionsMode = 'Fixed';
    elems(index).DataType = 'Bus: object_information';
    elems(index).Complexity = 'real';
    elems(index).Min = [];
    elems(index).Max = [];
    elems(index).DocUnits = '';
    elems(index).Description = '';
end

object_information_list = Simulink.Bus;
object_information_list.HeaderFile = '';
object_information_list.Description = '';
object_information_list.DataScope = 'Auto';
object_information_list.Alignment = -1;
object_information_list.PreserveElementDimensions = 0;
object_information_list.Elements = elems;
clear elems;
assignin('base','object_information_list', object_information_list);

% Bus object: object_information 
clear elems;
elems(1) = Simulink.BusElement;
elems(1).Name = 'pos_x';
elems(1).Dimensions = 1;
elems(1).DimensionsMode = 'Fixed';
elems(1).DataType = 'single';
elems(1).Complexity = 'real';
elems(1).Min = [];
elems(1).Max = [];
elems(1).DocUnits = '';
elems(1).Description = '';

elems(2) = Simulink.BusElement;
elems(2).Name = 'pos_y';
elems(2).Dimensions = 1;
elems(2).DimensionsMode = 'Fixed';
elems(2).DataType = 'single';
elems(2).Complexity = 'real';
elems(2).Min = [];
elems(2).Max = [];
elems(2).DocUnits = '';
elems(2).Description = '';

elems(3) = Simulink.BusElement;
elems(3).Name = 'heading_rad';
elems(3).Dimensions = 1;
elems(3).DimensionsMode = 'Fixed';
elems(3).DataType = 'single';
elems(3).Complexity = 'real';
elems(3).Min = [];
elems(3).Max = [];
elems(3).DocUnits = '';
elems(3).Description = '';

elems(4) = Simulink.BusElement;
elems(4).Name = 'velocity_x';
elems(4).Dimensions = 1;
elems(4).DimensionsMode = 'Fixed';
elems(4).DataType = 'single';
elems(4).Complexity = 'real';
elems(4).Min = [];
elems(4).Max = [];
elems(4).DocUnits = '';
elems(4).Description = '';

elems(5) = Simulink.BusElement;
elems(5).Name = 'velocity_y';
elems(5).Dimensions = 1;
elems(5).DimensionsMode = 'Fixed';
elems(5).DataType = 'single';
elems(5).Complexity = 'real';
elems(5).Min = [];
elems(5).Max = [];
elems(5).DocUnits = '';
elems(5).Description = '';

elems(6) = Simulink.BusElement;
elems(6).Name = 'object_size_x';
elems(6).Dimensions = 1;
elems(6).DimensionsMode = 'Fixed';
elems(6).DataType = 'single';
elems(6).Complexity = 'real';
elems(6).Min = [];
elems(6).Max = [];
elems(6).DocUnits = '';
elems(6).Description = '';

elems(7) = Simulink.BusElement;
elems(7).Name = 'object_size_y';
elems(7).Dimensions = 1;
elems(7).DimensionsMode = 'Fixed';
elems(7).DataType = 'single';
elems(7).Complexity = 'real';
elems(7).Min = [];
elems(7).Max = [];
elems(7).DocUnits = '';
elems(7).Description = '';

object_information = Simulink.Bus;
object_information.HeaderFile = '';
object_information.Description = '';
object_information.DataScope = 'Auto';
object_information.Alignment = -1;
object_information.PreserveElementDimensions = 0;
object_information.Elements = elems;
clear elems;
assignin('base','object_information', object_information);


% Bus object: point2d 
clear elems;
elems(1) = Simulink.BusElement;
elems(1).Name = 'x_position';
elems(1).Dimensions = 1;
elems(1).DimensionsMode = 'Fixed';
elems(1).DataType = 'single';
elems(1).Complexity = 'real';
elems(1).Min = [];
elems(1).Max = [];
elems(1).DocUnits = '';
elems(1).Description = '';

elems(2) = Simulink.BusElement;
elems(2).Name = 'y_position';
elems(2).Dimensions = 1;
elems(2).DimensionsMode = 'Fixed';
elems(2).DataType = 'single';
elems(2).Complexity = 'real';
elems(2).Min = [];
elems(2).Max = [];
elems(2).DocUnits = '';
elems(2).Description = '';

point2d = Simulink.Bus;
point2d.HeaderFile = '';
point2d.Description = '';
point2d.DataScope = 'Auto';
point2d.Alignment = -1;
point2d.PreserveElementDimensions = 0;
point2d.Elements = elems;
clear elems;
assignin('base','point2d', point2d);

% Create map information
clear elems;
elems(1) = Simulink.BusElement;
elems(1).Name = 'message_counter';
elems(1).Dimensions = 1;
elems(1).DimensionsMode = 'Fixed';
elems(1).DataType = 'uint8';
elems(1).Complexity = 'real';
elems(1).Min = [];
elems(1).Max = [];
elems(1).DocUnits = '';
elems(1).Description = '';

elems(2) = Simulink.BusElement;
elems(2).Name = 'size';
elems(2).Dimensions = 1;
elems(2).DimensionsMode = 'Fixed';
elems(2).DataType = 'uint8';
elems(2).Complexity = 'real';
elems(2).Min = [];
elems(2).Max = [];
elems(2).DocUnits = '';
elems(2).Description = '';

% Create loop to add all 
maxPoints = 4+2;
for index = 3:1:maxPoints
    elems(index) = Simulink.BusElement;
    elems(index).Name = strcat('point2d_',int2str(index-3));
    elems(index).Dimensions = 1;
    elems(index).DimensionsMode = 'Fixed';
    elems(index).DataType = 'Bus: point2d';
    elems(index).Complexity = 'real';
    elems(index).Min = [];
    elems(index).Max = [];
    elems(index).DocUnits = '';
    elems(index).Description = '';
end

map_information = Simulink.Bus;
map_information.HeaderFile = '';
map_information.Description = '';
map_information.DataScope = 'Auto';
map_information.Alignment = -1;
map_information.PreserveElementDimensions = 0;
map_information.Elements = elems;
clear elems;
assignin('base','map_information', map_information);