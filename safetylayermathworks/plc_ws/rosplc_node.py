#!/usr/bin/env python3

# Copyright (c) 2024 HAN university of applied science

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import numpy as np
import rclpy
from rclpy.node import Node
from tf2_ros.static_transform_broadcaster import StaticTransformBroadcaster
from geometry_msgs.msg import TransformStamped, Vector3, PoseStamped
from tf2_ros import TransformBroadcaster
import math
from sensor_msgs.msg import NavSatFix ,LaserScan,PointCloud2,Imu
from rclpy.qos import QoSProfile, QoSReliabilityPolicy, QoSHistoryPolicy, QoSDurabilityPolicy
from sc_interfaces.msg import SafetyLogicState,SafetyEnvelopeTrigger
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Twist
from visualization_msgs.msg import MarkerArray,Marker
from rclpy.parameter import Parameter
from sc_interfaces.msg import ObservedObjects,Object,SystemState
from ros_to_plc_bridge import *
max_object_hardcoded = 4
class ros_plc_node(Node):
    def __init__(self):
        super().__init__('ros_plc_node')
        qos_profile = QoSProfile(
            reliability=QoSReliabilityPolicy.BEST_EFFORT,
            durability=QoSDurabilityPolicy.VOLATILE,
            history=QoSHistoryPolicy.KEEP_LAST,
            depth=1
        ) 
        simtime =Parameter("use_sim_time", Parameter.Type.BOOL, True)
        self.set_parameters([simtime])  
        self.create_subscription(
            ObservedObjects,"/sc_topic_perception_objects",self.sc_object_callback,qos_profile)

        self.create_subscription(SystemState,"/sc/system_state",self.sc_system_state_callback,qos_profile)
        
        self.create_timer(0.05, self.timer_callback)

        self.ego_info = vehicle_information()
        self.ego_info.message_counter = 0
        self.ego_info.encode_message(3)
    

        self.nominal_input_info = nominal_input()
        self.nominal_input_info.message_counter = 0
        self.nominal_input_info.encode_message(4)




        self.object_list_ = object_information_list(max_object_hardcoded)
        self.object_list_.message_counter = 0
        self.object_list_.encode_message(2,0)


        ## test the message encoder

        point2d_info = point2d()
        point2d_info.x = 0.0
        point2d_info.y = 0.0
    

        self.map_info = map_information(4)
        self.map_info.map[0] = point2d_info
        point2d_info.x = 10.0
        point2d_info.y = 0.0
        self.map_info.map[1] = point2d_info
        point2d_info.x = 10.0
        point2d_info.y = 5.0
        self.map_info.map[2] = point2d_info
        point2d_info.x = 0.0
        point2d_info.y = 5.0
        self.map_info.map[3] = point2d_info
        self.map_info.message_counter = 1
        self.size_of_the_map = 4

        self.map_info.encode_message(1,self.size_of_the_map)

    


        self.port_ego_info = plc_interface_client("192.168.82.248",8000,"UDP")
        self.port_nominal_input_info = plc_interface_client("192.168.82.248",8001,"UDP")
        self.port_object_list_ = plc_interface_client("192.168.82.248",8002,"UDP")
        self.port_map_info = plc_interface_client("192.168.82.248",8003,"UDP")


    def timer_callback(self):
        self.port_ego_info.send_message(self.ego_info.bytes_message)
        self.port_nominal_input_info.send_message(self.nominal_input_info.bytes_message)
        self.port_object_list_.send_message(self.object_list_.bytes_message)
        self.port_map_info.send_message(self.map_info.bytes_message)

        

    def sc_system_state_callback(self,message:SystemState):
        
        self.ego_info.ego_ignition = True
        self.ego_info.ego_system_quality = True
        self.ego_info.message_counter = self.ego_info.message_counter + 1
        self.ego_info.map_pos_x = 0
        self.ego_info.map_pos_y = 0
        self.ego_info.map_heading_rad = 0
        self.ego_info.ego_velocity_x = message.velocity 
        self.ego_info.ego_velocity_y = 0
        self.ego_info.ego_steering_angle_rad = message.steering_angle

        ## test the message encoder for the map
        self.map_info.message_counter += 1
        self.map_info.encode_message(1,self.size_of_the_map)


        self.ego_info.encode_message(3)

        self.nominal_input_info.message_counter = self.nominal_input_info.message_counter + 1
        self.nominal_input_info.encode_message(4)

    def sc_object_callback(self,message:ObservedObjects):
       
        actual_size = message.len
        if actual_size > max_object_hardcoded:
            actual_size = max_object_hardcoded
        
        for i in range(actual_size):
            object_info = object_information()
            tmp = Object()
            tmp = message.obejcts_position[i]
            object_info.pos_x = tmp.pos_x
            object_info.pos_y = tmp.pos_y
            object_info.heading_rad = 0.0
            object_info.velocity_x = tmp.v_x
            object_info.velocity_y = tmp.v_y
            object_info.object_size_x = tmp.hitbox_x
            object_info.object_size_y = tmp.hitbox_y
            self.object_list_.object_list[i] = object_info

        self.object_list_.message_counter = self.object_list_.message_counter + 1

        self.object_list_.encode_message(2,actual_size)

def main(args=None):
    rclpy.init(args=args)
    ros_plc_node_= ros_plc_node()
    rclpy.spin(ros_plc_node_)
    #optitrack_tf_state.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
