<?xml version="1.0"?>

<!--
// # Copyright (c) 2024 HAN university of applied science

// # Permission is hereby granted, free of charge, to any person obtaining
// # a copy of this software and associated documentation files (the
// # "Software"), to deal in the Software without restriction, including
// # without limitation the rights to use, copy, modify, merge, publish,
// # distribute, sublicense, and/or sell copies of the Software, and to
// # permit persons to whom the Software is furnished to do so, subject to
// # the following conditions:

// # The above copyright notice and this permission notice shall be
// # included in all copies or substantial portions of the Software.

// # THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// # EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// # MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// # NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// # LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// # OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// # WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

-->

<robot name="sd_twizy" xmlns:xacro="http://www.ros.org/wiki/xacro">

  <!-- ***************** -->
  <!-- Imported elements -->
  <!-- ***************** -->
  <xacro:include filename="$(find twizy_drone)/urdf/bases/sd_twizy_body.urdf.xacro" />
  <xacro:include filename="$(find twizy_drone)/urdf/wheels/sd_twizy_suspension.urdf.xacro" />
  <xacro:include filename="$(find twizy_drone)/urdf/sensors/VLP-16.urdf.xacro"/>
  <xacro:include filename="$(find twizy_drone)/urdf/sensors/cameras.urdf.xacro"/>
  <xacro:include filename="$(find twizy_drone)/urdf/sensors/imu.urdf.xacro"/>
  <xacro:include filename="$(find twizy_drone)/urdf/sensors/rtk_gps.urdf.xacro"/>
  <xacro:include filename="$(find twizy_drone)/urdf/sensors/front_link.urdf.xacro"/>
  <xacro:include filename="$(find twizy_drone)/urdf/sensors/radar.urdf.xacro"/>


  <!-- *************** -->
  <!-- Robots Elements -->
  <!-- *************** -->
  <xacro:sd_twizy_base name="sd_twizy"/>

  <xacro:RL_wheel/>
  <xacro:RR_wheel/>
  <xacro:FL_wheel/>
  <xacro:FR_wheel/>

  <!-- Sensors top 0.162 in x to base link -->
  <!-- These sensors are simulating the filtered output compared to reallife -->


  <xacro:VLP-16  parent="chassis" name="velodyne_top" topic="/sensing/lidar/top/outlier_filtered/pointcloud" hz="10" lasers="24" max_range="130" samples="512" plugin_name ="laser_plugin_1">
    <origin xyz="-0.681 0 1.455" rpy="0 -${1.5*M_PI/180.0} 0"/>
  </xacro:VLP-16>
  <!-- <xacro:VLP-16  parent="chassis" name="velodyne_1" topic="/sensing/lidar/left/outlier_filtered/pointcloud" hz="10" samples="512" plugin_name ="laser_plugin_2">
    <origin xyz="-0.681 0 1.455" rpy="0 -${1.5*M_PI/180.0} 0.006"/>
  </xacro:VLP-16> -->

  <!-- <xacro:VLP-16  parent="base_link" name="velodyne_left" topic="/sensing/lidar/left/outlier_filtered/pointcloud" hz="10" samples="128" plugin_name ="laser_plugin_2" min_angle='-${70*M_PI/180.0}' max_angle='${70*M_PI/180.0}'>
    <origin xyz="0.0 0.56362 1.30555" rpy="-0.02 ${15*M_PI/180.0} ${90*M_PI/180.0}"/>
  </xacro:VLP-16>

  <xacro:VLP-16  parent="base_link" name="velodyne_right" topic="/sensing/lidar/right/outlier_filtered/pointcloud" hz="10" samples="128" plugin_name ="laser_plugin_3" min_angle='-${70*M_PI/180.0}' max_angle='${70*M_PI/180.0}'>
    <origin xyz="-0.0 -0.56362 1.350555" rpy="-0.01 ${15*M_PI/180.0} -${90*M_PI/180.0}"/>
  </xacro:VLP-16> -->

  <xacro:VLP-16  parent="base_link" name="velodyne_front" topic="/sensing/lidar/front/outlier_filtered/pointcloud" hz="10" lasers="32" samples="128" max_range="256" plugin_name ="laser_plugin_4" min_angle='-${60*M_PI/180.0}' max_angle='${60*M_PI/180.0}' min_angle_h='-${10*M_PI/180.0}' max_angle_h='${10*M_PI/180.0}'>
    <origin xyz="1.8 -0 0.50555" rpy="-0.01 0.0 -${0*M_PI/180.0}"/>
  </xacro:VLP-16>

  <xacro:Radar parent="base_link" name="front_left_radar">
    <origin xyz="1.8 0.3 0.50555" rpy="0 0 ${10*M_PI/180.0}"/>
  </xacro:Radar>

  <xacro:Radar parent="base_link" name="front_right_radar">
    <origin xyz="1.8 -0.3 0.50555" rpy="0 0 -${10*M_PI/180.0}"/>
  </xacro:Radar>

  <xacro:imu prefix="">
    <origin xyz="0 0 0" rpy="0 0 0"/>
  </xacro:imu>
  
  <xacro:rtk_gps prefix="">
    <origin xyz="0 0 0" rpy="0 0 0"/>
  </xacro:rtk_gps>

  <xacro:front_link prefix="">
    <origin xyz="1.686 0 0" rpy="0 0 0"/>
  </xacro:front_link>
  <!-- <xacro:sekonix_camera frlr_prefix="front" lrc_prefix="left">
    <origin xyz="-0.04 0.12 1.3" rpy="0 0.087 0"/>
  </xacro:sekonix_camera>
  
  <xacro:sekonix_camera frlr_prefix="front" lrc_prefix="right">
    <origin xyz="-0.04 -0.12 1.3" rpy="0 0.087 0"/>
  </xacro:sekonix_camera>

  <xacro:sekonix_camera frlr_prefix="front" lrc_prefix="center">
    <origin xyz="-0.04 0 1.3" rpy="0 0.087 0"/>
  </xacro:sekonix_camera>

  <xacro:sekonix_camera frlr_prefix="rear" lrc_prefix="center">
    <origin xyz="-1.26 0 1.28" rpy="0 0.2618 3.14"/>
  </xacro:sekonix_camera>

  <xacro:sekonix_camera frlr_prefix="right" lrc_prefix="forward">
    <origin xyz="-0.95 -0.42 1.2" rpy="0 0.244 -1.12"/>
  </xacro:sekonix_camera>

  <xacro:sekonix_camera frlr_prefix="right" lrc_prefix="rearward">
    <origin xyz="-1.05 -0.42 1.2" rpy="0 0.244 -2.18"/>
  </xacro:sekonix_camera>

  <xacro:sekonix_camera frlr_prefix="left" lrc_prefix="forward">
    <origin xyz="-0.95 0.4 1.2" rpy="0 0.2967 1.12"/>
  </xacro:sekonix_camera>

  <xacro:sekonix_camera frlr_prefix="left" lrc_prefix="rearward">
    <origin xyz="-1.05 0.39 1.2" rpy="0 0.2967 2.18"/>
  </xacro:sekonix_camera> -->

  <gazebo>

  </gazebo>

  <!-- <gazebo>
    <plugin name="p3d" filename="libgazebo_ros_p3d.so">
      <body_name>base_link</body_name>
      <topic_name>base_pose_ground_truth</topic_name>
      <frame_name>map</frame_name>
      <update_rate>100.0</update_rate>
    </plugin>
  </gazebo> -->

  <gazebo>

        <plugin name='ackermann_drive' filename='libgazebo_ros_ackermann_drive.so'>

            <ros>

              <!-- <remapping>odom:=/localization/kinematic_state</remapping> -->
              <!-- <remapping>cmd_vel:=cmd_vel_raw</remapping> -->
         
            </ros>
            <wheel_base>1.686</wheel_base>
            <update_rate>100.0</update_rate>

            <!-- wheels -->
            <front_left_joint>front_left_wheel_joint</front_left_joint>
            <front_right_joint>front_right_wheel_joint</front_right_joint>
            <rear_left_joint>rear_left_wheel_joint</rear_left_joint>
            <rear_right_joint>rear_right_wheel_joint</rear_right_joint>
            <left_steering_joint>front_left_steer_joint</left_steering_joint>
            <right_steering_joint>front_right_steer_joint</right_steering_joint>

            <!-- Max absolute steer angle for tyre in radians-->
            <!-- Any cmd_vel angular z greater than this would be capped -->
            <max_steer>0.9</max_steer>

            <!-- Max absolute steering angle of steering wheel -->
            <max_steering_angle>25.85</max_steering_angle>

            <!-- Max absolute linear speed in m/s -->
            <max_speed>50</max_speed>

            <!-- PID tuning -->
            <left_steering_pid_gain>450 0.0 0.0</left_steering_pid_gain>
            <left_steering_i_range>0 0</left_steering_i_range>
            <right_steering_pid_gain>450 0.0 0.0</right_steering_pid_gain>
            <right_steering_i_range>0 0</right_steering_i_range>
            <linear_velocity_pid_gain>100 0 0.0</linear_velocity_pid_gain>
            <linear_velocity_i_range>0 0</linear_velocity_i_range>

            <!-- output -->
            <publish_odom>true</publish_odom>
            <publish_odom_tf>true</publish_odom_tf>
            <publish_wheel_tf>true</publish_wheel_tf>
            <publish_distance>false</publish_distance>

            <odometry_frame>map</odometry_frame>
            <robot_base_frame>base_link</robot_base_frame>

        </plugin>
  </gazebo>

  <!-- <gazebo>
    <plugin name="joint_state_publisher" filename="libgazebo_ros_joint_state_publisher.so">
      <robot_namespace>/sd_twizy</robot_namespace>
      <joint_name>front_left_wheel_joint</joint_name>
      <joint_name>front_right_wheel_joint</joint_name>
      <joint_name>front_left_steer_joint</joint_name>
      <joint_name> front_right_steer_joint</joint_name>
      <joint_name> rear_left_wheel_joint</joint_name>
      <joint_name>rear_right_wheel_joint</joint_name>
      <update_rate>30.0</update_rate>
    </plugin>
  </gazebo> -->

</robot>
