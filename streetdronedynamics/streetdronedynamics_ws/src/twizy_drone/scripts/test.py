#// # Copyright (c) 2024 HAN university of applied science

#// # Permission is hereby granted, free of charge, to any person obtaining
#// # a copy of this software and associated documentation files (the
#// # "Software"), to deal in the Software without restriction, including
#// # without limitation the rights to use, copy, modify, merge, publish,
#// # distribute, sublicense, and/or sell copies of the Software, and to
#// # permit persons to whom the Software is furnished to do so, subject to
#// # the following conditions:

#// # The above copyright notice and this permission notice shall be
#// # included in all copies or substantial portions of the Software.

#// # THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
#// # EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
#// # MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
#// # NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
#// # LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
#// # OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
#// # WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import math

q = [ 0.0,0.0,-0.517660948532364,0.8555858474545808]
    #   x: -21.325956344604492
    #   y: 26.769323348999023
    #   z: 0.0
    # orientation:
    #   x: 0.0
    #   y: 0.0
    #   z: -0.517660948532364
    #   w: 0.8555858474545808
def euler_to_quaternion(yaw, pitch, roll):
        
        qx = math.sin(roll/2) * math.cos(pitch/2) * math.cos(yaw/2) - math.cos(roll/2) * math.sin(pitch/2) * math.sin(yaw/2)
        qy = math.cos(roll/2) * math.sin(pitch/2) * math.cos(yaw/2) + math.sin(roll/2) * math.cos(pitch/2) * math.sin(yaw/2)
        qz = math.cos(roll/2) * math.cos(pitch/2) * math.sin(yaw/2) - math.sin(roll/2) * math.sin(pitch/2) * math.cos(yaw/2)
        qw = math.cos(roll/2) * math.cos(pitch/2) * math.cos(yaw/2) + math.sin(roll/2) * math.sin(pitch/2) * math.sin(yaw/2)
        
        return [qx, qy, qz, qw] 
def quaternion_to_euler(x, y, z, w):
    
    t0 = +2.0 * (w * x + y * z)
    t1 = +1.0 - 2.0 * (x * x + y * y)
    roll = math.atan2(t0, t1)
    t2 = +2.0 * (w * y - z * x)
    t2 = +1.0 if t2 > +1.0 else t2
    t2 = -1.0 if t2 < -1.0 else t2
    pitch = math.asin(t2)
    t3 = +2.0 * (w * z + x * y)
    t4 = +1.0 - 2.0 * (y * y + z * z)
    yaw = math.atan2(t3, t4)
    return yaw, pitch, roll

yaw,p,r = quaternion_to_euler(q[0],q[1],q[2],q[3])
print(euler_to_quaternion(3.1415926535897932/2,0.0,0.0))
